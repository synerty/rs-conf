/*
 * File.cpp
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#include "DbocFile.h"

#include <iostream>
#include <fstream>

#include <boost/format.hpp>
#include <boost/filesystem.hpp>

#include <QtCore/qglobal.h>

namespace dboc
{
  File::File(Logger& logger, std::string path, std::string file) :
    mLogger(logger), //
        mPath(path), mFile(file), mFileName(path + "\\" + file)
  {
    namespace bf = boost::filesystem;

    bf::path filePath(mFileName);
    bf::path dirPath(filePath.remove_filename());

    if (!bf::exists(dirPath))
      assert(bf::create_directories(dirPath));

    assert(bf::exists(dirPath));
  }

  File::~File()
  {
  }

  void
  File::save(std::string data)
  {
    if (isDataSame(data))
    {
      // mLogger("Skipping file " + mFileName);
      return;
    }

    mLogger("opening file " + mFileName);
    std::ofstream file;
    file.open(mFileName.c_str());
    assert(file.is_open());
    file << data;
    file.close();
  }

  void
  File::saveData(std::string data)
  {
  }

  bool
  File::isDataSame(std::string data)
  {
    std::ifstream file;
    file.open(mFileName.c_str());
    if (!file.is_open())
    {
      file.close();
      return false;
    }

    std::istreambuf_iterator< char > itr(file.rdbuf());
    std::istreambuf_iterator< char > itrEnd;

    std::string currentData(itr, itrEnd);

    return currentData == data;
  }
}
