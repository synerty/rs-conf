/*
 * DbOcFormat.h
 *
 *  Created on: Feb 14, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _DBOC_FORMAT_H_
#define _DBOC_FORMAT_H_

#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>

class SmSchema;
class SmTable;
class SmColumn;

/*! dboc::Format Brief Description
 * Long comment about the class
 *
 */
namespace dboc
{

  void
  generateRelatesTo(const boost::shared_ptr< SmTable > table,
      std::vector< boost::shared_ptr< SmTable > >& relatesTo);

  void
  generateRelatesFrom(const boost::shared_ptr< SmTable > table,
      std::vector< boost::shared_ptr< SmTable > >& relatedFrom);

  bool
  isExportEnabled(const boost::shared_ptr< SmTable > table);

  bool
  isColumnInherited(const boost::shared_ptr< SmColumn > column);

  bool
  isColumnConcreteInherited(const boost::shared_ptr< SmColumn > column);

  bool
  isExportEnabled(const boost::shared_ptr< SmColumn > column);

  boost::shared_ptr< SmColumn >
  columnFromTable(const boost::shared_ptr< SmTable > table,
      std::string columnName);

  boost::shared_ptr< SmColumn >
  primaryKeyColumn(const boost::shared_ptr< SmTable > table);

  std::string
  enterObjectNamespace(std::string modelName,
      const boost::shared_ptr< SmTable > table);

  std::string
  exitObjectNamespace();

  std::string
  dllMacro(const std::string modelName);

  std::string
  dllAttributeMacro(const std::string modelName);

  std::string
  enterModelNamespace(std::string modelName);

  std::string
  exitModelNamespace();

  std::string
  objectFileName(const boost::shared_ptr< SmTable > table);

  std::string
  objectFilePath(const boost::shared_ptr< SmTable > table);

  std::string
  modelFilename(std::string modelClass);

  std::string
  objectDeclarationFilename(std::string modelName);

  std::string
  objectTypeFilename(std::string modelName);

  std::string
  valueTypeFilename(std::string modelName);

  std::string
  dispalyName(const boost::shared_ptr< SmTable > table);

  std::string
  headerObjectDefine(const boost::shared_ptr< SmTable > table);

  std::string
  headerObjectDeclataionDefine(std::string modelName);

  std::string
  headerModelDefine(std::string modelName);

  std::string
  headerObjectTypeDefine(std::string modelName);

  std::string
  headerValueTypeDefine(std::string modelName);

  std::string
  niceName(const boost::shared_ptr< SmSchema > schema);

  std::string
  niceName(const boost::shared_ptr< SmTable > table);

  std::string
  niceName(const boost::shared_ptr< SmColumn > column);

  std::string
  className(const boost::shared_ptr< SmTable > table);

  std::string
  pkStructName(const boost::shared_ptr< SmTable > table);

  std::string
  pkStructNameWithNamespace(std::string modelName,
      const boost::shared_ptr< SmTable > table);

  std::string
  classNameWithNamespace(std::string modelName,
      const boost::shared_ptr< SmTable > table);

  std::string
  modelClassNameWithNamespace(std::string modelName, std::string modelClass);

  std::string
  modelVariableName(const boost::shared_ptr< SmTable > table);

  std::string
  objectProperyGetterName(const boost::shared_ptr< SmColumn > column);

  std::string
  formatCleanupString(std::string str);

  std::string
  formatCamelCase(const std::string& name,
      std::string separator = "",
      bool firstLower = false,
      std::string tokenSeparator = "_");

  std::string
  formatUpperCase(const std::string& name);

  std::string
  formatLowerCase(const std::string& name, std::string separator = "_");

  std::string
  include(std::string name);

  std::string
  reservedWordCheck(std::string word);

}

#endif /* _DBOC_FORMAT_H_ */
