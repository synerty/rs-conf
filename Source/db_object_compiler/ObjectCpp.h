/*
 * CppObject.h
 *
 *  Created on: Feb 13, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _DBOC_CPP_OBJECT_H_
#define _DBOC_CPP_OBJECT_H_

#include <string>
#include <vector>
#include <set>
#include <stdint.h>

#include <boost/shared_ptr.hpp>

#include "Logger.h"

class SmModel;
class SmTable;
class SmColumn;

/*! dboc::CppObject Brief Description
 * Long comment about the class
 *
 */
namespace dboc
{

  class ObjectCpp
  {
    public:
      ObjectCpp(Logger& logger,
          const std::string outputPath,
          const std::string defaultSchema,
          const std::string modelName,
          const std::string modelClass,
          boost::shared_ptr< SmModel > model);

      virtual
      ~ObjectCpp();

      void
      Run();

    private:
      std::string
      generate(const boost::shared_ptr< SmTable > table);

      std::string
      addIncludes(const boost::shared_ptr< SmTable > table);

      std::string
      addProperties(const boost::shared_ptr< SmTable > table);

      std::string
      addProperty(const boost::shared_ptr< SmColumn > column);

      std::string
      addPropertyBackReference(const boost::shared_ptr< SmColumn > column);

      std::string
      addConstructor(const boost::shared_ptr< SmTable > table);

      std::string
      addConstructorProps(const boost::shared_ptr< SmTable > table);

      std::string
      addStaticSetup(const boost::shared_ptr< SmTable > table);

      std::string
      addStaticSetupSqlOpts(const boost::shared_ptr< SmTable > table);

      std::string
      addStaticSetupFieldMetas(const boost::shared_ptr< SmTable > table);

      std::string
      addStaticSetupFieldMeta(const boost::shared_ptr< SmColumn > column);

      void
          objectTypeIdsToExclude(const std::vector< boost::shared_ptr< SmTable > >& tables,
              std::set< std::string >& ids);

      void
          valueTypeIdsToExclude(const std::vector< boost::shared_ptr< SmTable > >& tables,
              std::set< std::string >& ids);

    private:
      Logger& mLogger;
      const std::string mOutputPath;

      const std::string mDefaultSchema;
      const std::string mModelName;
      const std::string mModelClass;

      boost::shared_ptr< SmModel > mModel;

      const std::string mDivider;

  };

}

#endif /* _DBOC_CPP_OBJECT_H_ */
