/*
 * HppObject.h
 *
 *  Created on: Feb 13, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _DBOC_HPP_OBJECT_H_
#define _DBOC_HPP_OBJECT_H_

#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>

#include "Logger.h"

class SmModel;
class SmTable;
class SmColumn;

/*! dboc::HppObject Brief Description
 * Long comment about the class
 *
 */
namespace dboc
{

  class ObjectHpp
  {
    public:
      ObjectHpp(Logger& logger,
          const std::string outputPath,
          const std::string modelName,
          const std::string modelClass,
          boost::shared_ptr< SmModel > model);

      virtual
      ~ObjectHpp();

      void
      Run();

    private:
      std::string
      generate(const boost::shared_ptr< SmTable > table);

      std::string
      includeOrmProperties(const boost::shared_ptr< SmTable > table);

      std::string
      addForwardDeclarations(const boost::shared_ptr< SmTable > table,
          bool declarePkStruct);

      std::string
      addProperties(const boost::shared_ptr< SmTable > table);

      std::string
      addProperty(const boost::shared_ptr< SmColumn > column);

      std::string
      addPropertyBackReference(const boost::shared_ptr< SmColumn > column);

    private:
      Logger& mLogger;
      const std::string mOutputPath;

      const std::string mModelName;
      const std::string mModelClass;

      boost::shared_ptr< SmModel > mModel;

  };

}

#endif /* _DBOC_HPP_OBJECT_H_ */
