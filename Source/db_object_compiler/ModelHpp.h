/*
 * HppModel.h
 *
 *  Created on: Feb 13, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _DBOC_HPP_MODEL_H_
#define _DBOC_HPP_MODEL_H_

#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>

#include "Logger.h"

class SmModel;
class SmTable;
class SmColumn;

/*! dboc::HppModel Brief Description
 * Long comment about the class
 *
 */
namespace dboc
{

  class ModelHpp
  {
    public:
      ModelHpp(Logger& logger, const std::string outputPath,
          const std::string modelName,
          const std::string modelClass,
          boost::shared_ptr< SmModel > model);

      virtual
      ~ModelHpp();

      void
      Run();

    private:
      std::string
      generate();

      std::string
      addObjects();

      std::string
      addObject(const boost::shared_ptr< SmTable > table);

    private:
      Logger& mLogger;
      const std::string mOutputPath;

      const std::string mModelName;
      const std::string mModelClass;

      boost::shared_ptr< SmModel > mModel;

  };

}

#endif /* _DBOC_HPP_MODEL_H_ */
