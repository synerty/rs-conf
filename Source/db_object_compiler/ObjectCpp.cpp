/*
 * CppObject.cpp
 *
 *  Created on: Feb 13, 2011
 *      Author: Jarrod Chesney
 */

#include "ObjectCpp.h"

#include <QtCore/qglobal.h>
#include <boost/format.hpp>
#include <boost/algorithm/string/join.hpp>

#include "SmOrmModel.h"
#include "SmSchema.h"
#include "SmTable.h"
#include "SmObjectType.h"
#include "SmValueType.h"
#include "SmColumn.h"

#include "DbOcFormat.h"
#include "DbocFile.h"
#include "ObjectPk.h"

namespace dboc
{

  ObjectCpp::ObjectCpp(Logger& logger,
      const std::string outputPath,
      const std::string defaultSchema,
      const std::string modelName,
      const std::string modelClass,
      boost::shared_ptr< SmModel > model) :
        mLogger(logger), //
        mOutputPath(outputPath), //
        mDefaultSchema(defaultSchema), //
        mModelName(modelName), //
        mModelClass(modelClass),//
        mModel(model), //
        mDivider("// ----------------------------------------------------------------------------\n")
  {
  }
  /// -------------------------------------------------------------------------

  ObjectCpp::~ObjectCpp()
  {
  }
  /// -------------------------------------------------------------------------

  void
  ObjectCpp::Run()
  {
    for (SmModel::TableListT::const_iterator itr = mModel->Tables().begin(); //
    itr != mModel->Tables().end(); ++itr)
    {
      if (!isExportEnabled(*itr))
        continue;

      File file(mLogger, mOutputPath, objectFilePath(*itr) + ".cpp");
      file.save(generate(*itr));
    }
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectCpp::generate(const boost::shared_ptr< SmTable > table)
  {
    ObjectPk pkObject(mModelName, table);

    boost::format fmt("%s" // Include the header
      "\n"
      "#include <orm/v1/StorableWrapper.ini>\n"
      "#include <orm/v1/StorageWrapper.ini>\n"
      "\n"
      "%s" // dependent includes
      "\n"
      "%s" // PK struct output stream function
      "\n"
      // Macro - storable, key type, key accessor
      "ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(%s, %s, %s) \n"
      // Explicit orm::v1::StorageWrapper< ModelT, StorableT, KeyT >
      "template class orm::v1::StorageWrapper<%s, %s, %s>;\n"
      // Explicit orm::v1::StorableWrapper< ModelT, StorableT > instatiation
      "template class orm::v1::StorableWrapper<%s, %s>;\n"
      "%s" // Divider
      "\n"
      "%s" // Define properties
      "%s" // Divider
      "\n"
      "%s" // Namespace enter
      "\n"
      "ORM_V1_CALL_STORABLE_SETUP(%s)\n"
      "%s" // Divider
      "\n"
      "%s" // pk struct definition
      "%s" // Divider
      "\n"
      "%s" // Constructor
      "%s" // Divider
      "\n"
      "%s::~%s()\n" // Destructor
      "{\n"
      "}\n"
      "%s" // Divider
      "\n"
      "%s" // staticSetup method
      "%s" // Divider
      "\n"
      "%s" // primary key function definition
      "%s" // devider
      "\n"
      "%s" // Namespace exit
        );

    fmt % include(objectFileName(table));// Include the header
    fmt % addIncludes(table);// dependent includes

    fmt % pkObject.structOperatorDefinition(); // PK struct output stream function

    // Macro - model, storable, key type, key accessor
    fmt % classNameWithNamespace(mModelName, table); // storable
    fmt % pkObject.keyDataType(); // key type
    fmt % pkObject.keyAcessorFunctionName(); // key accessor

    // Explicit orm::v1::StorageWrapper< ModelT, StorableT, KeyT >
    fmt % modelClassNameWithNamespace(mModelName, mModelClass); // model
    fmt % classNameWithNamespace(mModelName, table); // storable
    fmt % pkObject.keyDataType(); // key type

    // Explicit orm::v1::StorableWrapper< ModelT, StorableT > instatiation
    fmt % modelClassNameWithNamespace(mModelName, mModelClass); // model
    fmt % classNameWithNamespace(mModelName, table); // storable

    fmt % mDivider;

    fmt % addProperties(table); //Define properties
    fmt % mDivider;

    fmt % enterObjectNamespace(mModelName, table);// Namespace enter
    fmt % className(table); // Macro
    fmt % mDivider;

    fmt % pkObject.structDefinition(); // pk struct definition
    fmt % mDivider;

    fmt % addConstructor(table); // Constructor
    fmt % mDivider;

    fmt % className(table) % className(table); //  Destructor
    fmt % mDivider;

    fmt % addStaticSetup(table); // staticSetup method
    fmt % mDivider;

    if (pkObject.usingStruct())
    {
      fmt % pkObject.keyFunctionDefinition(); // primary key function definition
      fmt % mDivider;
    }
    else
    {
      fmt % "";
      fmt % "";
    }

    fmt % exitObjectNamespace();// Namespace exit

    return fmt.str();
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectCpp::addIncludes(SmTable::PtrT table)
  {
    std::string result;

    SmTable::ListT relatesTo;
    generateRelatesTo(table, relatesTo);

    // Tables is inheritedBy
    //    if (!table->isInheritedByEmpty() || !relatesTo.empty())
    result += include("../" + mModelClass);// Include the model

    if (table->inherits().get() != NULL)
      result += include("orm/v1/SqlOpts");

    if (relatesTo.empty())
      return result;

    for (SmTable::ListT::const_iterator itr = relatesTo.begin(); //
    itr != relatesTo.end(); ++itr)
      result += include("../" + objectFilePath(*itr));

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectCpp::addProperties(const boost::shared_ptr< SmTable > table)
  {
    std::string result;

    SmTable::ColumnsListT columnLists;
    table->columns(columnLists);
    for (SmTable::ColumnsListT::const_iterator itr = columnLists.begin(); //
    itr != columnLists.end(); ++itr)
    {
      const SmColumn::PtrT& column = *itr;

      if (!isExportEnabled(column))
        continue;

      result += addProperty(*itr);
      result += addPropertyBackReference(*itr);
    }

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectCpp::addProperty(const boost::shared_ptr< SmColumn > column)
  {
    std::string result;

    if (column->relatesTo().get() == NULL)
    {
      bool inherits = isColumnInherited(column);

      // Property
      // ORM_V1_SIMPLE_PROPERTY(SmModel, id, Id, int32_t)
      boost::format fmt("ORM_V1_DEFINE_SIMPLE_PROPERTY%s(%s, %s, %s, %s)\n");

      fmt % (inherits ? "_INHERITED" : "");
      fmt % classNameWithNamespace(mModelName, column->table());
      fmt % objectProperyGetterName(column);
      fmt % formatCamelCase(niceName(column));
      fmt % column->cppDataType();

      result += fmt.str();
    }
    else
    {
      // Related Property
      // ORM_V1_RELATED_PROPERTY(SmModel, valueType, ValueType, int32_t, SmValueType)
      boost::format
          fmt("ORM_V1_DEFINE_RELATED_PROPERTY(%s, %s, %s, %s, %s, %s)\n");

      fmt % modelClassNameWithNamespace(mModelName, mModelClass);
      fmt % classNameWithNamespace(mModelName, column->table());
      fmt % formatCamelCase(niceName(column), "", true);
      fmt % formatCamelCase(niceName(column));
      fmt % column->cppDataType();
      fmt % classNameWithNamespace(mModelName, column->relatesTo()->table());

      result += fmt.str();
    }

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectCpp::addPropertyBackReference(const boost::shared_ptr< SmColumn > column)
  {
    std::string result;

    SmColumn::RelatedFromListT columnRelatedFrom;
    column->relatedFrom(columnRelatedFrom);
    for (SmColumn::RelatedFromListT::const_iterator
        itr = columnRelatedFrom.begin(); //
    itr != columnRelatedFrom.end(); ++itr)
    {
      const SmColumn::PtrT& otherColumn = *itr;

      qDebug("addProperty : %s is related from %s",
          column->fullName().c_str(),
          otherColumn->fullName().c_str());

      // Related To Many
      // ORM_V1_RELATED_TO_MANY_PROPERTY(SmModel, relatedFrom, RelatedFrom, SmColumn)
      boost::format
          fmt("ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(%s, %s, %s, %s)\n");

      fmt % classNameWithNamespace(mModelName, column->table());
      fmt % (formatCamelCase(niceName(otherColumn->table()), "", true)
          + formatCamelCase(niceName(otherColumn)) + "s");
      fmt % (formatCamelCase(niceName(otherColumn->table()))
          + formatCamelCase(niceName(otherColumn)) + "s");
      fmt % classNameWithNamespace(mModelName, otherColumn->table());

      result += fmt.str();
    }

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectCpp::addConstructor(const boost::shared_ptr< SmTable > table)
  {
    //    SmColumn::SmColumn()
    //    {
    //      mPropId.setup(this);
    //      ...
    //    }
    boost::format fmt("%s::%s()\n" // SmColumn SmColumn
      "{\n"
      "%s" // Properties
      "}\n");

    fmt % className(table) % className(table);
    fmt % addConstructorProps(table);

    return fmt.str();
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectCpp::addConstructorProps(const boost::shared_ptr< SmTable > table)
  {
    std::string result;
    std::string relatedBackRef = "\n"
      "  // Related back references.\n";

    SmTable::ColumnsListT columnLists;
    table->columns(columnLists);
    for (SmTable::ColumnsListT::const_iterator itr = columnLists.begin(); //
    itr != columnLists.end(); ++itr)
    {
      const SmColumn::PtrT& column = *itr;

      if (!isExportEnabled(column))
        continue;

      boost::format fmt("  mProp%s.setup(this);\n");
      fmt % formatCamelCase(niceName(*itr));
      result += fmt.str();

      // setup the back references
      SmColumn::RelatedFromListT columnRelatedFrom;
      column->relatedFrom(columnRelatedFrom);
      for (SmColumn::RelatedFromListT::const_iterator
          itr = columnRelatedFrom.begin(); //
      itr != columnRelatedFrom.end(); ++itr)
      {
        const SmColumn::PtrT& otherColumn = *itr;

        // Related To Many
        // mPropRelatedFrom.setup(this);
        boost::format fmt("  mProp%s.setup(this);\n");

        fmt % (formatCamelCase(niceName(otherColumn->table()))
            + formatCamelCase(niceName(otherColumn)) + "s");
        relatedBackRef += fmt.str();
      }

    }

    return result + relatedBackRef;
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectCpp::addStaticSetup(const boost::shared_ptr< SmTable > table)
  {
    //    bool
    //    SmColumn::setupStatic()
    //    {
    //      using namespace orm::v1;
    //      typedef StorableConfig< SmColumn > CfgT;
    //      ...
    //      return true;
    //    }

    // Begin string
    boost::format fmt("bool\n"
      "%s::setupStatic()\n" // SmColumn
      "{\n"
      "\n"
      "  sModelAddDerrivedFunction = &ModelT::addDerrived%s;\n" // Add mem func
      "  sModelRemoveFunction = &ModelT::remove%s;\n" // remove member function
      "\n"
      "  using namespace orm::v1;\n"
      "  typedef StorableConfig< %s > CfgT;\n" // SmColumn
      "\n"
      "%s" // SqlOpt stuff
      "\n"
      "%s" // addAttrMeta stuff
      "\n"
      "  return true;\n"
      "}\n");

    fmt % className(table);
    fmt % modelVariableName(table); // Add mem func
    fmt % modelVariableName(table); // remove member function
    fmt % classNameWithNamespace(mModelName, table);
    fmt % addStaticSetupSqlOpts(table);
    fmt % addStaticSetupFieldMetas(table);

    return fmt.str();
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectCpp::addStaticSetupSqlOpts(const boost::shared_ptr< SmTable > table)
  {
    std::string result;

    {
      //    CfgT::setSqlOpts()->setName("SmColumn");
      //    CfgT::setSqlOpts()->setTableName("sams_meta.tbl_COLUMN");

      boost::format fmt(//
      "  CfgT::setSqlOpts()->setName(\"%s\");\n" // Display name
        "  CfgT::setSqlOpts()->setTableName(\"%s\");\n" // sams_meta.tbl_COLUMN
        "\n");

      fmt % dispalyName(table);
      if (mDefaultSchema == table->schema()->name())
        fmt % table->name();
      else
        fmt % (table->schema()->name() + "." + table->name());

      result += fmt.str();
    }

    if (table->inherits().get() != NULL)
    {
      // CfgT::setSqlOpts()->setInheritanceType(SqlOpts::Joined);
      boost::format inherit(//
      "  CfgT::setSqlOpts()->setInheritanceType(%s);\n"
        "\n");
      inherit % table->cppInheritType();
      result += inherit.str();
    }

    // We don't need to restrict the queries for concrete inheritance
    // IE, no polymorphic on (as SQLA would say)
    if (table->inheritType() == orm::v1::SqlOpts::ConcreteInheritance)
      return result;

    // Get a list of object IDs that are represented by inherited tables
    // we want to exclude them from this table when we query
    SmTable::InheritedByListT inheritedByTables;
    table->inheritedBy(inheritedByTables);

    std::set< std::string > objectTypeIds;
    objectTypeIdsToExclude(inheritedByTables, objectTypeIds);
    std::string objectTypeConditions = boost::algorithm::join(objectTypeIds,
        ", ");

    std::set< std::string > valueTypeIds;
    valueTypeIdsToExclude(inheritedByTables, valueTypeIds);
    std::string
        valueTypeConditions = boost::algorithm::join(valueTypeIds, ", ");

    // If both are empty
    // FIXME
    // DAMN DODGY CODING - It cuts to do this but i'm strapped for time :-(
    if (table->name() == "tblSimObjects" || table->name() == "tblTelePts")
    {
      // Exlcude everything
      result += "  CfgT::setSqlOpts()->addCondition(\"1 = 0\");\n";
    }
    else if (objectTypeConditions.empty() && valueTypeConditions.empty())
    {
      // Don't exclude anything
      result += "  CfgT::setSqlOpts()->addCondition(\"1 = 1 OR 1 = 0\");\n";
    }
    // if value type is empty, object type mustn't be
    else if (valueTypeConditions.empty())
    {
      boost::format fmt("  CfgT::setSqlOpts()->addCondition(\""
        "(\\\"%s\\\" NOT IN (%s)\\n%s\\n) OR 1 = 0\");\n");
      if (QString(table->name().c_str()).startsWith("tbl_OBJECT_TYPE"))
      {
        fmt % "INHERITS";
        fmt % objectTypeConditions;
        fmt % "OR \\\"INHERITS\\\" IS NULL";
      }
      else
      {
        fmt % "OBJECT_TYPE";
        fmt % objectTypeConditions;
        fmt % "";
      }
      result += fmt.str();
    }
    // if object type is empty, value type mustn't be
    else if (objectTypeConditions.empty())
    {
      boost::format fmt("  CfgT::setSqlOpts()->addCondition(\""
        "(\\\"VALUE_TYPE\\\" NOT IN (%s)\\n) OR 1 = 0\");\n");
      fmt % valueTypeConditions;
      result += fmt.str();
    }
    else
    {
      assert(false);
    }

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectCpp::addStaticSetupFieldMetas(const boost::shared_ptr< SmTable > table)
  {
    std::string result;

    SmTable::ColumnsListT columnLists;
    table->columns(columnLists);
    for (SmTable::ColumnsListT::const_iterator itr = columnLists.begin(); //
    itr != columnLists.end(); ++itr)
    {
      const SmColumn::PtrT& column = *itr;
      if (!isExportEnabled(column))
        continue;

      result += addStaticSetupFieldMeta(column);
    }

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectCpp::addStaticSetupFieldMeta(const boost::shared_ptr< SmColumn > column)
  {
    if (column->relatesTo().get() == NULL)
    {
      /*
       * Property
       * CfgT::addFieldMeta(NamePropT::setupStatic("NAME"));
       */
      boost::format fmt("  CfgT::addFieldMeta(%sPropT::setupStatic(\n"
        "    \"%s\",\n" // "NAME"
        "    %s,\n" // is primary key
        "    %s,\n" // is valid if null
        "    boost::optional<%s>(%s)));\n" // default value
          );

      fmt % formatCamelCase(niceName(column));
      fmt % column->name();
      fmt % (column->isPrimaryKey() ? "true" : "false");
      fmt % (column->isNullable() ? "true" : "false");
      fmt % column->cppDataType();
      fmt % (column->isDefaultValueNull() ? "" : column->cppDefaultValue());

      return fmt.str();
    }

    /*
     * Related Property
     * CfgT::addFieldMeta(TablePropT::setupStatic("TABLE",
     *     &SmTable::Id,
     *     &SmModel::Table,
     *     &SmTable::Columns,
     *     false,
     *     false));
     */
    boost::format fmt("  CfgT::addFieldMeta(%sPropT::setupStatic(\n"
      "    \"%s\",\n" // "NAME"
      "    &%s::%s,\n" // related object id function
      "    &%s::%s,\n" // function in model to retrieve related object
      "    &%s::%s,\n" // related object collection of objects relating to it
      "    %s,\n" // is primary key
      "    %s,\n" // is valid if null
      "    boost::optional<%s>(%s)));\n" // default value
        );

    std::string objectColumn = formatCamelCase(column->name());
    std::string relatedObject = className(column->relatesTo()->table());
    std::string relatedObjectNs = classNameWithNamespace(mModelName,
        column->relatesTo()->table());
    std::string
        relatedObjectColumn = formatCamelCase(niceName(column->relatesTo()),
            "",
            true);

    fmt % formatCamelCase(niceName(column));
    fmt % column->name();
    fmt % relatedObjectNs % relatedObjectColumn;
    fmt % mModelClass % modelVariableName(column->relatesTo()->table());
    fmt % relatedObjectNs % (className(column->table()) + objectColumn + "s");
    fmt % (column->isPrimaryKey() ? "true" : "false");
    fmt % (column->isNullable() ? "true" : "false");
    fmt % column->cppDataType();
    fmt % (column->isDefaultValueNull() ? "" : column->cppDefaultValue());

    return fmt.str();

  }
  /// -------------------------------------------------------------------------

  void
  ObjectCpp::objectTypeIdsToExclude(const SmTable::ListT& tables,
      std::set< std::string >& ids)
  {
    // FIXME, This is confusing. Split up the object types conditions
    // for data tables and the object types for the object type tables

    // Iterate though all the tables inheriting this one and exclude their
    // object types
    for (SmTable::ListT::const_iterator itr = tables.begin();//
    itr != tables.end(); ++itr)
    {
      const SmTable::PtrT& table = *itr;
      assert(table.get());

      std::deque< SmObjectType::PtrT > inheritedByObjectTypes;

      // Get the object types for this table
      {
        SmTable::ObjectTypesListT objectTypes;
        table->objectTypes(objectTypes);

        std::copy(objectTypes.begin(),
            objectTypes.end(),
            std::back_inserter(inheritedByObjectTypes));
      }

      bool isObjectTypeTables = false;
      if (!inheritedByObjectTypes.empty())
        isObjectTypeTables = (inheritedByObjectTypes.front()->objectTypeTable()
            == table);

      while (!inheritedByObjectTypes.empty())
      {
        SmObjectType::PtrT objectType = inheritedByObjectTypes.front();
        inheritedByObjectTypes.pop_front();

        assert(objectType.get());
        ids.insert(boost::lexical_cast< std::string >(objectType->id()));

        if (isObjectTypeTables)
        {
          SmObjectType::ListT objectTypes;
          objectType->inheritedBy(objectTypes);

          std::copy(objectTypes.begin(),
              objectTypes.end(),
              std::back_inserter(inheritedByObjectTypes));
        }
      }

      SmTable::InheritedByListT inheritedByTables;
      table->inheritedBy(inheritedByTables);

      objectTypeIdsToExclude(inheritedByTables, ids);
    }

  }
  /// -------------------------------------------------------------------------

  void
  ObjectCpp::valueTypeIdsToExclude(const SmTable::ListT& tables,
      std::set< std::string >& ids)
  {
    for (SmTable::ListT::const_iterator itr = tables.begin();//
    itr != tables.end(); ++itr)
    {
      const SmTable::PtrT& table = *itr;
      assert(table.get());

      SmTable::ValueTypesListT valueTypes;
      table->valueTypes(valueTypes);

      for (SmValueType::ListT::iterator valTypItr = valueTypes.begin();//
      valTypItr != valueTypes.end(); ++valTypItr)
      {
        SmValueType::PtrT& valueType = *valTypItr;
        assert(valueType.get());
        ids.insert(boost::lexical_cast< std::string >(valueType->id()));
      }

      SmTable::InheritedByListT inheritedByTables;
      table->inheritedBy(inheritedByTables);

      valueTypeIdsToExclude(inheritedByTables, ids);
    }

  }
/// -------------------------------------------------------------------------


}
