/*
 * Logger.cpp
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#include "Logger.h"

#include <QtGui/qlistwidget.h>
#include <QtCore/qstring.h>
namespace dboc
{

  Logger::Logger(QListWidget* listWidget) :
    mListWidget(listWidget)
  {
  }

  Logger::~Logger()
  {
  }

  void
  Logger::operator()(const std::string message)
  {
    mListWidget->addItem(QString(message.c_str()));
  }

}
