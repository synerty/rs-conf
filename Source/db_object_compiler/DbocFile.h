/*
 * File.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef DBOCFILE_H_
#define DBOCFILE_H_

#include <string>
#include "Logger.h"

namespace dboc
{
  /*! File Brief Description
   * Long comment about the class
   *
   */
  class File
  {
    public:
      File(Logger& logger, std::string path, std::string file);
      virtual
      ~File();

      void
      save(std::string data);

    private:
      void
      saveData(std::string data);

      bool
      isDataSame(std::string data);

     Logger mLogger;
      const std::string mPath;
      const std::string mFile;
      const std::string mFileName;
  };
}
#endif /* DBOCFILE_H_ */
