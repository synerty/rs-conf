/*
 * DbOcFormat.cpp
 *
 *  Created on: Feb 14, 2011
 *      Author: Jarrod Chesney
 */

#include "DbOcFormat.h"

#include <boost/format.hpp>
#include <boost/algorithm/string/erase.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/tokenizer.hpp>

#include <QtCore/qglobal.h>

#include "SmSchema.h"
#include "SmColumn.h"
#include "SmTable.h"

#include "orm/v1/SqlOpts.h"

namespace dboc
{

  void
  generateRelatesTo(const boost::shared_ptr< SmTable > table,
      SmTable::ListT& relatesTo)
  {
    SmTable::ColumnsListT tableColumns;
    table->columns(tableColumns);
    for (SmTable::ColumnsListT::const_iterator itr = tableColumns.begin(); //
    itr != tableColumns.end(); ++itr)
    {
      const SmColumn::PtrT& column = *itr;

      if (column->relatesTo() == NULL)
        continue;

      //      qDebug("generateRelatesTo : %s relates to %s",
      //          column->fullName().c_str(),
      //          column->relatesTo()->fullName().c_str());

      relatesTo.push_back(column->relatesTo()->table());
    }
  }
  /// -------------------------------------------------------------------------

  void
  generateRelatesFrom(const boost::shared_ptr< SmTable > table,
      SmTable::ListT& relatedFrom)
  {
    SmTable::ColumnsListT tableColumns;
    table->columns(tableColumns);
    for (SmTable::ColumnsListT::const_iterator itr = tableColumns.begin(); //
    itr != tableColumns.end(); ++itr)
    {
      const SmColumn::PtrT& column = *itr;

      SmColumn::RelatedFromListT columnRelatedFroms;
      column->relatedFrom(columnRelatedFroms);
      for (SmColumn::RelatedFromListT::const_iterator otherColItr = columnRelatedFroms.begin(); //
      otherColItr != columnRelatedFroms.end(); ++otherColItr)
      {
        const SmColumn::PtrT& otherColumn = *otherColItr;

        //        qDebug("generateRelatesFrom : %s is related from %s",
        //            column->fullName().c_str(),
        //            otherColumn->fullName().c_str());

        relatedFrom.push_back(otherColumn->table());
      }

    }
  }
  /// -------------------------------------------------------------------------

  bool
  isExportEnabled(const boost::shared_ptr< SmTable > table)
  {
    return table->generateStorable() //
    && (primaryKeyColumn(table).get() != NULL);

  }
  /// -------------------------------------------------------------------------

  bool
  isColumnInherited(const boost::shared_ptr< SmColumn > column)
  {
    if (column->table()->inherits().get() != NULL)
    {
      SmColumn::ListT parentCols;
      column->table()->inherits()->columns(parentCols);
      for (SmTable::ColumnsListT::const_iterator itr = parentCols.begin(); //
      itr != parentCols.end(); ++itr)
      {
        if ((*itr)->name() == column->name())
          return true;
      }
    }

    return false;

  }
  /// -------------------------------------------------------------------------

  bool
  isColumnConcreteInherited(const boost::shared_ptr< SmColumn > column)
  {
    if (!isColumnInherited(column))
      return false;

    return column->table()->inheritType()
        == orm::v1::SqlOpts::ConcreteInheritance;
  }
  /// -------------------------------------------------------------------------

  bool
  isExportEnabled(const boost::shared_ptr< SmColumn > column)
  {
    return column->isPrimaryKey() //
        || (!isColumnConcreteInherited(column)
            && column->isGenerateStorableProperty());
  }
  /// -------------------------------------------------------------------------

  boost::shared_ptr< SmColumn >
  columnFromTable(const boost::shared_ptr< SmTable > table,
      std::string columnName)
  {
    SmTable::ColumnsListT cols;
    table->columns(cols);

    for (SmTable::ColumnsListT::const_iterator itr = cols.begin(); //
    itr != cols.end(); ++itr)
    {
      if ((*itr)->name() == columnName)
        return *itr;
    }
    return SmColumn::PtrT();
  }
  /// -------------------------------------------------------------------------

  boost::shared_ptr< SmColumn >
  primaryKeyColumn(const boost::shared_ptr< SmTable > table)
  {

    SmTable::ColumnsListT tableColumns;
    table->columns(tableColumns);
    for (SmTable::ColumnsListT::const_iterator itr = tableColumns.begin(); //
    itr != tableColumns.end(); ++itr)
    {
      SmColumn::PtrT column = *itr;

      if (column->isPrimaryKey())
        return column;
    }

    qCritical("Skipping table %s as it doesn't have a primary key",
        table->fullName().c_str());

    return SmColumn::PtrT();
  }
  /// -------------------------------------------------------------------------

  std::string
  enterObjectNamespace(std::string modelName,
      const boost::shared_ptr< SmTable > table)
  {
    std::string result;

    boost::format fmt("namespace %s {\n");
    fmt % modelName;
    result += fmt.str();

    fmt.clear_binds();
    fmt % formatLowerCase(niceName(table->schema()));
    result += fmt.str();

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  exitObjectNamespace()
  {
    return "}\n"
        "}\n";
  }
  /// -------------------------------------------------------------------------

  std::string
  dllMacro(const std::string modelName)
  {
    std::string tag = formatUpperCase(modelName);
    std::string decldir = dllAttributeMacro(modelName);

    boost::format fmt("#if defined(BUILD_%s_DLL)\n"
        "#define %s __declspec(dllexport)\n"
        "#elif defined(USE_%s_DLL)\n"
        "#define %s __declspec(dllimport)\n"
        "#else\n"
        "#define %s \n"
        "#endif\n");
    fmt % tag;
    fmt % decldir;
    fmt % tag;
    fmt % decldir;
    fmt % decldir;
    return fmt.str();
  }
  /// -------------------------------------------------------------------------

  std::string
  dllAttributeMacro(const std::string modelName)
  {
    return formatUpperCase("DECL_" + modelName + "_DIR");
  }
  /// -------------------------------------------------------------------------

  std::string
  enterModelNamespace(std::string modelName)
  {
    std::string result;

    boost::format fmt("namespace %s {\n");
    fmt % modelName;
    result += fmt.str();

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  exitModelNamespace()
  {
    return "}\n";
  }
  /// -------------------------------------------------------------------------

  std::string
  objectFileName(const boost::shared_ptr< SmTable > table)
  {
    return formatCamelCase(niceName(table->schema())) + className(table);
  }
  /// -------------------------------------------------------------------------

  std::string
  objectFilePath(const boost::shared_ptr< SmTable > table)
  {
    return formatLowerCase(niceName(table->schema()), "_") //
    + "\\" + objectFileName(table);
  }
  /// -------------------------------------------------------------------------

  std::string
  modelFilename(std::string modelClass)
  {
    return modelClass;
  }
  /// -------------------------------------------------------------------------
  std::string
  objectDeclarationFilename(std::string modelName)
  {
    return formatCamelCase(modelName) + "Declaration";
  }
  /// -------------------------------------------------------------------------

  std::string
  objectTypeFilename(std::string modelName)
  {
    return formatCamelCase(modelName) + "ObjectType";
  }
  /// -------------------------------------------------------------------------

  std::string
  valueTypeFilename(std::string modelName)
  {
    return formatCamelCase(modelName) + "ValueType";
  }
  /// -------------------------------------------------------------------------

  std::string
  dispalyName(const boost::shared_ptr< SmTable > table)
  {
    return formatCamelCase(niceName(table->schema()), " ") //
    + " - " + formatCamelCase(niceName(table), " ");
  }
  /// -------------------------------------------------------------------------

  std::string
  headerObjectDefine(const boost::shared_ptr< SmTable > table)
  {
    return "_" + formatUpperCase(niceName(table->schema())) //
    + "_" + formatUpperCase(niceName(table)) + "_";
  }
  /// -------------------------------------------------------------------------

  std::string
  headerObjectDeclataionDefine(std::string modelName)
  {
    return "_" + formatUpperCase(modelName) + "_OBJECT_DECLARATIONS_H_";
  }
  /// -------------------------------------------------------------------------

  std::string
  headerModelDefine(std::string modelName)
  {
    return "_" + formatUpperCase(modelName) + "_MODEL_H_";
  }
  /// -------------------------------------------------------------------------

  std::string
  headerObjectTypeDefine(std::string modelName)
  {
    return "_" + formatUpperCase(modelName) + "_OBJECT_TYPE_H_";
  }
  /// -------------------------------------------------------------------------

  std::string
  headerValueTypeDefine(std::string modelName)
  {
    return "_" + formatUpperCase(modelName) + "_VALUE_TYPE_H_";
  }
  /// -------------------------------------------------------------------------

  std::string
  niceName(const SmSchema::PtrT schema)
  {
    assert(schema.get());
    return schema->isNiceNameNull() ? schema->name() : schema->niceName();
  }
  /// -------------------------------------------------------------------------

  std::string
  niceName(const SmTable::PtrT table)
  {
    assert(table.get());
    return table->isNiceNameNull() ? table->name() : table->niceName();
  }
  /// -------------------------------------------------------------------------

  std::string
  niceName(const SmColumn::PtrT column)
  {
    assert(column.get());
    return column->isNiceNameNull() ? column->name() : column->niceName();
  }
  /// -------------------------------------------------------------------------

  std::string
  className(const boost::shared_ptr< SmTable > table)
  {
    std::string name = formatCamelCase(niceName(table));
    assert(!name.empty());
    if (name.substr(name.length() - 3) == "es")
      name.erase(name.length() - 1);
    return name;
  }
  /// -------------------------------------------------------------------------

  std::string
  classNameWithNamespace(std::string modelName,
      const boost::shared_ptr< SmTable > table)
  {
    return modelName + "::" + formatLowerCase(niceName(table->schema())) + "::"
        + className(table);
  }
  /// -------------------------------------------------------------------------

  std::string
  pkStructName(const boost::shared_ptr< SmTable > table)
  {
    return className(table) + "Pk";
  }
  /// -------------------------------------------------------------------------

  std::string
  pkStructNameWithNamespace(std::string modelName,
      const boost::shared_ptr< SmTable > table)
  {
    return classNameWithNamespace(modelName, table) + "Pk";
  }
  /// -------------------------------------------------------------------------

  std::string
  modelClassNameWithNamespace(std::string modelName, std::string modelClass)
  {
    return modelName + "::" + modelClass;
  }
  /// -------------------------------------------------------------------------

  std::string
  modelVariableName(const boost::shared_ptr< SmTable > table)
  {
    return formatCamelCase(niceName(table->schema())) + className(table);
  }
  /// -------------------------------------------------------------------------

  std::string
  objectProperyGetterName(const boost::shared_ptr< SmColumn > column)
  {
    return (column->cppDataType() == "bool" //
        ? "is" + formatCamelCase(niceName(column)) //
        : formatCamelCase(niceName(column), "", true));
  }
  /// -------------------------------------------------------------------------

  std::string
  formatCleanupString(std::string str)
  {
    if (boost::istarts_with(str, "tbl"))
      boost::erase_head(str, 3);

    if (boost::istarts_with(str, "is"))
      boost::erase_head(str, 2);

    if (boost::istarts_with(str, "_"))
      boost::erase_head(str, 1);

    boost::replace_all(str, ",", "_");
    boost::replace_all(str, "=", "_");
    boost::replace_all(str, ".", "_");
    boost::replace_all(str, "/", "_");
    boost::replace_all(str, "\\", "_");
    boost::replace_all(str, "-", "_");
    boost::replace_all(str, "+", "_");
    boost::replace_all(str, "(", "_");
    boost::replace_all(str, ")", "_");
    boost::replace_all(str, " ", "_");
    boost::replace_all(str, "__", "_");

    std::string out;
    bool lastLower = false;
    for (std::string::size_type i = 0; i < str.length(); i++)
    {
      char c = str[i];

      if (lastLower && std::isupper(c))
        out += '_';

      out += c;
      lastLower = std::islower(c);
    }

    return out;
  }
  /// -------------------------------------------------------------------------

  std::string
  formatCamelCase(const std::string& name,
      std::string separator,
      bool firstLower,
      std::string tokenSeparator)
  {
    std::string tokSrc = formatCleanupString(name);

    typedef boost::char_separator< char > SepT;
    typedef boost::tokenizer< SepT > TokT;

    std::string result;
    TokT tok(tokSrc, SepT("_"));
    for (TokT::iterator itr = tok.begin(); itr != tok.end(); ++itr)
    {
      std::string word = *itr;

      if (word.empty())
        continue;

      boost::to_lower(word);

      if (!(result.empty() && firstLower))
        word[0] = toupper(word[0]);

      if (!result.empty())
        result += separator;

      result += word;
    }

    return reservedWordCheck(result);
  }
  /// -------------------------------------------------------------------------

  std::string
  formatUpperCase(const std::string& name)
  {
    std::string tokSrc = formatCleanupString(name);

    typedef boost::char_separator< char > SepT;
    typedef boost::tokenizer< SepT > TokT;

    std::string result;
    TokT tok(tokSrc, SepT("_"));
    for (TokT::iterator itr = tok.begin(); itr != tok.end(); ++itr)
    {
      std::string word = *itr;

      if (word.empty())
        continue;

      boost::to_upper(word);

      if (!result.empty())
        result += "_";

      result += word;
    }

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  formatLowerCase(const std::string& name, std::string separator)
  {
    std::string tokSrc = formatCleanupString(name);

    typedef boost::char_separator< char > SepT;
    typedef boost::tokenizer< SepT > TokT;

    std::string result;
    TokT tok(tokSrc, SepT("_"));
    for (TokT::iterator itr = tok.begin(); itr != tok.end(); ++itr)
    {
      std::string word = *itr;

      if (word.empty())
        continue;

      boost::to_lower(word);

      if (!result.empty())
        result += separator;

      result += word;
    }

    return reservedWordCheck(result);
  }
  /// -------------------------------------------------------------------------

  std::string
  include(std::string name)
  {
    boost::replace_all(name, "\\", "/");

    boost::format fmt("#include \"%s.h\"\n");
    fmt % name;
    return fmt.str();
  }
  /// ---------------------------------------------------------------------------

  std::string
  reservedWordCheck(std::string word)
  {
    if (word == "public")
      return word + "_";

    if (word == "signed")
      return word + "_";

    if (word == "typeid")
      return word + "_";

    return word;
  }
/// ---------------------------------------------------------------------------

}
