/*
 * CppModel.cpp
 *
 *  Created on: Feb 13, 2011
 *      Author: Jarrod Chesney
 */

#include "ModelCpp.h"

#include <QtCore/qglobal.h>
#include <boost/format.hpp>

#include "SmOrmModel.h"
#include "SmSchema.h"
#include "SmTable.h"
#include "SmColumn.h"

#include "DbOcFormat.h"
#include "DbocFile.h"
#include "ObjectPk.h"

namespace dboc
{

  ModelCpp::ModelCpp(Logger& logger,
      const std::string outputPath,
      const std::string modelName,
      const std::string modelClass,
      boost::shared_ptr< SmModel > model) :
        mLogger(logger), //
        mOutputPath(outputPath), //
        mModelName(modelName), //
        mModelClass(modelClass),//
        mModel(model), //
        mDivider("// ----------------------------------------------------------------------------\n")
  {
  }
  /// -------------------------------------------------------------------------

  ModelCpp::~ModelCpp()
  {
  }
  /// -------------------------------------------------------------------------

  void
  ModelCpp::Run()
  {
    File file(mLogger, mOutputPath, modelFilename(mModelClass) + ".cpp");
    file.save(generate());
  }
  /// -------------------------------------------------------------------------

  std::string
  ModelCpp::generate()
  {
    boost::format fmt("%s" // Include the header
      "\n"
      "#include <orm/v1/ModelWrapper.ini>\n"
      "\n"
      "// Add explicit instantiations for the model wrapper\n"
      "template class orm::v1::Model< %s >;" // Explicit Model< ModelT > instantiation
      "\n"
      "%s" // Explicit ModelWrapper<ModelT,StorableT> instantiation
      "\n"
      "%s" // Namespace enter
      "\n"
      "%s::%s(const orm::v1::Conn& ormConn)\n" // Constructor
      "  : BaseModel(ormConn)\n"
      "{\n"
      "%s" // addStorage function calls
      "}\n"
      "%s" // Divider
      "\n"
      "%s::~%s()\n" // Destructor
      "{\n"
      "}\n"
      "%s" // Divider
      "\n"
      "%s" // object declarations
      "\n"
      "%s" // Namespace exit
        );

    fmt % include(modelFilename(mModelClass)); // Include the header

    // Explicit model<> instantiation
    fmt % modelClassNameWithNamespace(mModelName, mModelClass);

    // Explicit ModelWrapper<ModelT,StorableT> instantiation
    fmt % addWrapperInstantiations();

    fmt % enterModelNamespace(mModelName); // Namespace enter
    fmt % mModelClass % mModelClass; //  Constructor
    fmt % addObjects();
    fmt % mDivider;
    fmt % mModelClass % mModelClass; //  Destructor
    fmt % mDivider;
    fmt % addObjectDefinitions();
    fmt % exitModelNamespace();// Namespace exit

    return fmt.str();
  }
  /// -------------------------------------------------------------------------

  std::string
  ModelCpp::addWrapperInstantiations()
  {
    SmColumn::PtrT pkColumn;
    std::string result;

    for (SmTable::ListT::const_iterator itr = mModel->Tables().begin(); //
    itr != mModel->Tables().end(); ++itr)
      result += addWrapperInstantiation(*itr);

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  ModelCpp::addWrapperInstantiation(const boost::shared_ptr< SmTable > table)
  {
    if (!isExportEnabled(table))
      return "";

    /*
     * addStorage(mSchemas, mSchemasMap, &SmSchema::Id);
     */
    boost::format fmt("template class orm::v1::ModelWrapper<%s, %s>;\n");

    fmt % modelClassNameWithNamespace(mModelName, mModelClass);
    fmt % classNameWithNamespace(mModelName, table);

    return fmt.str();
  }
  /// -------------------------------------------------------------------------

  std::string
  ModelCpp::addObjects()
  {
    SmColumn::PtrT pkColumn;
    std::string result;

    for (SmTable::ListT::const_iterator itr = mModel->Tables().begin(); //
    itr != mModel->Tables().end(); ++itr)
      result += addObject(*itr);

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  ModelCpp::addObject(const boost::shared_ptr< SmTable > table)
  {
    if (!isExportEnabled(table))
      return "";

    /*
     * addStorage(mSchemas, mSchemasMap, &SmSchema::Id);
     */
    boost::format fmt("  addStorageWrapper(m%sStorageWrapper);\n");

    fmt % modelVariableName(table);

    return fmt.str();
  }
  /// -------------------------------------------------------------------------

  std::string
  ModelCpp::addObjectDefinitions()
  {
    std::string result;

    for (SmTable::ListT::const_iterator itr = mModel->Tables().begin(); //
    itr != mModel->Tables().end(); ++itr)
      result += addObjectDefinition(*itr);

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  ModelCpp::addObjectDefinition(const boost::shared_ptr< SmTable > table)
  {
    if (!isExportEnabled(table))
      return "";

    ObjectPk pkObject(mModelName, table);

    // ORM_V1_MODEL_DEFINE_STORABLE(Is, Schema, std::string, Name)

    // Property
    boost::format fmt("    ORM_V1_MODEL_DEFINE_STORABLE(%s, %s, %s, %s)\n");

    fmt % mModelClass;
    fmt % classNameWithNamespace(mModelName, table);
    fmt % modelVariableName(table);
    fmt % pkObject.keyDataType();

    return fmt.str();
  }
/// -------------------------------------------------------------------------

}
