/*
 * ObjectPk.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef OBJECTPK_H_
#define OBJECTPK_H_

#include <vector>
#include <string>

#include <boost/shared_ptr.hpp>

class SmTable;
class SmColumn;

/*! dboc::ObjectPk Brief Description
 * Long comment about the class
 *
 */
namespace dboc
{

  class ObjectPk
  {
    public:
      ObjectPk(const std::string modelName, boost::shared_ptr< SmTable > table);
      virtual
      ~ObjectPk();

    public:

      bool
      usingStruct();

      std::string
      keyAcessorFunctionName();

      std::string
      keyDataType();

      std::string
      structDeclaration();

      std::string
      structDefinition();

      std::string
      keyFunctionDeclaration();

      std::string
      keyFunctionDefinition();

      std::string
      structOperatorDefinition();

    private:

      std::string
      constructorArgs();

      std::string
      constructorInitialisers();

      std::string
      structMembers();

      std::string
      constructor();

      std::string
      lessThan();

      std::string
      lessThanComparisons();

      std::string
      lessThanComparison(boost::shared_ptr< SmColumn >, bool isLast);

      const std::string mModelName;
      boost::shared_ptr< SmTable > mTable;
      std::vector< boost::shared_ptr< SmColumn > > mPkColumns;

      std::string mStructName;
  };

}

#endif /* OBJECTPK_H_ */
