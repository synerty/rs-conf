/*
 * HppObject.h
 *
 *  Created on: Feb 13, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _DBOC_HPP_OBJECT_DECLARATION_H_
#define _DBOC_HPP_OBJECT_DECLARATION_H_

#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>

#include "Logger.h"

class SmModel;
class SmTable;
class SmColumn;

/*! dboc::HppObject Brief Description
 * Long comment about the class
 *
 */
namespace dboc
{

  class ObjectDeclarationHpp
  {
    public:
      ObjectDeclarationHpp(Logger& logger, const std::string outputPath,
          const std::string modelName,
          const std::string modelClass,
          boost::shared_ptr< SmModel > model);

      virtual
      ~ObjectDeclarationHpp();

      void
      Run();

    private:
      std::string
      generate();

      std::string
      addForwardDeclarations();

    private:
      Logger& mLogger;
      const std::string mOutputPath;

      const std::string mModelName;
      const std::string mModelClass;

      boost::shared_ptr< SmModel > mModel;

  };

}

#endif /* _DBOC_HPP_OBJECT_DECLARATION_H_ */
