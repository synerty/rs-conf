#ifndef DBOBJECTCOMPILER_H
#define DBOBJECTCOMPILER_H

#include <QtCore/qobject.h>
#include "ui_DbObjectCompiler.h"

class DbObjectCompiler : public QObject
{
  Q_OBJECT

  public:
    DbObjectCompiler(QWidget *parent);

    virtual
    ~DbObjectCompiler();

  public:
    void Reset();

  public slots:

    void
    DoCompile();

  private:
    Ui::DbObjectCompilerClass ui;
};

#endif // DBOBJECTCOMPILER_H
