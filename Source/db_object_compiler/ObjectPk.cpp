/*
 * ObjectPk.cpp
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#include "ObjectPk.h"

#include "SmTable.h"
#include "SmColumn.h"

#include "DbOcFormat.h"

namespace dboc
{

  ObjectPk::ObjectPk(const std::string modelName,
      boost::shared_ptr< SmTable > table) :
    mModelName(modelName), //
        mTable(table), //
        mStructName(pkStructName(mTable))
  {
    SmColumn::ListT cols;
    table->columns(cols);

    for (SmColumn::ListT::const_iterator itr = cols.begin(); //
    itr != cols.end(); ++itr)
    {
      SmColumn::PtrT col = *itr;
      if (col->isPrimaryKey())
        mPkColumns.push_back(col);
    }

    if (mPkColumns.empty())
      throw std::runtime_error("No primary key columns, ObjectPk::ObjectPk");

  }

  ObjectPk::~ObjectPk()
  {
  }

  bool
  ObjectPk::usingStruct()
  {
    return (mPkColumns.size() != 1);
  }

  std::string
  ObjectPk::keyAcessorFunctionName()
  {
    if (mPkColumns.size() == 1)
      return formatCamelCase(niceName(mPkColumns.front()), "", true);

    return "primaryKey_";
  }

  std::string
  ObjectPk::keyDataType()
  {
    if (mPkColumns.size() == 1)
      return mPkColumns.front()->cppDataType();;

    return pkStructNameWithNamespace(mModelName, mTable);
  }

  std::string
  ObjectPk::keyFunctionDeclaration()
  {
    if (mPkColumns.size() == 1)
      return "";

    boost::format fmt("%s primaryKey_() const;\n"); // data type
    fmt % mStructName;
    return fmt.str();
  }

  std::string
  ObjectPk::keyFunctionDefinition()
  {
    if (mPkColumns.size() == 1)
      return "";

    boost::format fmt("%s %s::primaryKey_() const\n" // data type, class name
      "{\n"
      "  return %s(%s);\n"
      "}\n");
    fmt % mStructName % className(mTable);
    fmt % mStructName;

    // the property accessors
    {
      std::string propAccessors;

      for (SmColumn::ListT::const_iterator itr = mPkColumns.begin(); //
      itr != mPkColumns.end(); ++itr)
      {
        SmColumn::PtrT col = *itr;

        if (!propAccessors.empty())
          propAccessors += "\n    , ";

        propAccessors += objectProperyGetterName(col) + "()";
      }

      fmt % propAccessors;
    }

    return fmt.str();
  }

  std::string
  ObjectPk::structOperatorDefinition()
  {
    if (mPkColumns.size() == 1)
      return "";

    boost::format
        fmt("std::ostream& operator<<(std::ostream& os, const %s& o)\n" // data type, class name
          "{\n"
          "  os %s;\n"
          "  return os;\n"
          "}\n");

    fmt % pkStructNameWithNamespace(mModelName, mTable);

    // the property accessors
    {
      std::string output;

      for (SmColumn::ListT::const_iterator itr = mPkColumns.begin(); //
      itr != mPkColumns.end(); ++itr)
      {
        SmColumn::PtrT col = *itr;
        boost::format assign("\n    << \" %s=|\" << o.%s <<'|'");
        assign % objectProperyGetterName(col);
        assign % objectProperyGetterName(col);
        output += assign.str();
      }

      fmt % output;
    }

    return fmt.str();
  }

  std::string
  ObjectPk::structDeclaration()
  {
    if (mPkColumns.size() <= 1)
      return "";

    boost::format fmt("struct %s %s \n" // dll attribute macro, stuct name
      "{\n"
      "  %s(%s);\n" // struct name, constructor args
      "\n"
      "  bool operator<(const %s& o) const;\n" // struct name
      "\n"
      "%s" // Members
      "};\n");

    fmt % dllAttributeMacro(mModelName); // dll attribute macro

    fmt % mStructName; // stuct name

    fmt % mStructName % constructorArgs(); // struct name, constructor args

    fmt % mStructName;// stuct name

    fmt % structMembers();

    return fmt.str();

  }

  std::string
  ObjectPk::structDefinition()
  {
    if (mPkColumns.size() <= 1)
      return "";

    boost::format fmt("%s" // constructor
      "\n"
      "%s"// Less than function
      "\n");

    fmt % constructor();
    fmt % lessThan();

    return fmt.str();
  }

  std::string
  ObjectPk::constructor()
  {
    boost::format fmt("%s::%s(%s) :\n" // struct name, struct name, args
      "  %s\n" // Constructor initilisers
      "{\n"
      "}\n");

    fmt % mStructName % mStructName % constructorArgs();
    fmt % constructorInitialisers();

    return fmt.str();
  }

  std::string
  ObjectPk::constructorArgs()
  {
    std::string args;

    for (SmColumn::ListT::const_iterator itr = mPkColumns.begin(); //
    itr != mPkColumns.end(); ++itr)
    {
      SmColumn::PtrT col = *itr;

      boost::format arg("const %s& %s_");

      arg % col->cppDataType();
      arg % objectProperyGetterName(col);

      if (!args.empty())
        args += "\n    , ";

      args += arg.str();
    }

    return args;
  }

  std::string
  ObjectPk::constructorInitialisers()
  {
    std::string init;

    for (SmColumn::ListT::const_iterator itr = mPkColumns.begin(); //
    itr != mPkColumns.end(); ++itr)
    {
      SmColumn::PtrT col = *itr;
      const std::string colAccessor = objectProperyGetterName(col);

      boost::format arg("%s(%s_)");

      arg % colAccessor;
      arg % colAccessor;

      if (!init.empty())
        init += "//\n  , ";

      init += arg.str();
    }

    return init;
  }

  std::string
  ObjectPk::structMembers()
  {
    std::string members;

    for (SmColumn::ListT::const_iterator itr = mPkColumns.begin(); //
    itr != mPkColumns.end(); ++itr)
    {
      SmColumn::PtrT col = *itr;

      boost::format member("  const %s %s;\n");

      member % col->cppDataType();
      member % objectProperyGetterName(col);

      members += member.str();
    }

    return members;
  }

  std::string
  ObjectPk::lessThan()
  {
    boost::format fmt("bool %s::operator<(const %s& o) const\n" // struct name :\n" // struct name, struct name, args
      "{\n"
      "%s"
      "}\n");

    fmt % mStructName % mStructName;
    fmt % lessThanComparisons();

    return fmt.str();
  }

  std::string
  ObjectPk::lessThanComparisons()
  {
    std::string result;
    for (SmColumn::ListT::const_iterator itr = mPkColumns.begin(); //
    itr != mPkColumns.end(); ++itr)
    {
      SmColumn::ListT::const_iterator nextItr = itr;
      nextItr++;

      result += lessThanComparison(*itr, nextItr == mPkColumns.end());
    }

    return result;
  }

  std::string
  ObjectPk::lessThanComparison(boost::shared_ptr< SmColumn > column,
      bool isLast)
  {
    const std::string colAccessor = objectProperyGetterName(column);
    std::string result;

    if (!isLast)
    {
      boost::format condition("  if (%s != o.%s)\n  "); // member name, member name
      condition % colAccessor % colAccessor;
      result += condition.str();
    }

    boost::format fmt("  return %s < o.%s;\n\n"); // member name, member name
    fmt % colAccessor % colAccessor;
    result += fmt.str();

    return result;
  }

}
