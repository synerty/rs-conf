/*
 * HppObject.cpp
 *
 *  Created on: Feb 13, 2011
 *      Author: Jarrod Chesney
 */

#include "ObjectHpp.h"

#include <map>

#include <QtCore/qglobal.h>

#include "SmOrmModel.h"
#include "SmSchema.h"
#include "SmTable.h"
#include "SmColumn.h"

#include "DbOcFormat.h"
#include "DbocFile.h"
#include "ObjectPk.h"

namespace dboc
{

  ObjectHpp::ObjectHpp(Logger& logger,
      const std::string outputPath,
      const std::string modelName,
      const std::string modelClass,
      boost::shared_ptr< SmModel > model) :
    mLogger(logger), //
        mOutputPath(outputPath), //
        mModelName(modelName), //
        mModelClass(modelClass),//
        mModel(model)
  {
  }
  /// -------------------------------------------------------------------------

  ObjectHpp::~ObjectHpp()
  {
  }
  /// -------------------------------------------------------------------------

  void
  ObjectHpp::Run()
  {
    for (SmModel::TableListT::const_iterator itr = mModel->Tables().begin(); //
    itr != mModel->Tables().end(); ++itr)
    {
      if (!isExportEnabled(*itr))
        continue;

      File file(mLogger, mOutputPath, objectFilePath(*itr) + ".h");
      file.save(generate(*itr));
    }
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectHpp::generate(const boost::shared_ptr< SmTable > table)
  {
    ObjectPk pkObject(mModelName, table);

    boost::format fmt("#ifndef %s\n" // ifndef
      "#define %s\n" // define
      "\n"
      "%s" // dll macro
      "\n"
      "#include <string>\n"
      "#include <stdint.h>\n"
      "\n"
      "#include \"orm/v1/Storable.h\"\n"
      "#include \"orm/v1/StorableWrapper.h\"\n"
      "%s" // Include properties
      "\n"
      "%s" // include inherited table
      "\n"
      "%s" // forward declarations
      "\n"
      // Macro - storable, key type
      "ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(%s, %s)\n"
      "\n"
      "%s" // enter namespace
      "\n"
      "%s" // primary key struct
      "\n"
      "class %s %s\n" // dll attribute,  class name
      "    : public orm::v1::Storable< %s, %s %s>\n" // model , class, inherits
      "{\n"
      "  public:\n"
      "    %s();\n" // constructor
      "    virtual ~%s();\n" // destructor
      "\n"
      "  public:\n"
      "    static bool setupStatic();\n"
      "\n"
      "  public:\n"
      "%s" // declare key accessor function
      "\n"
      "%s" // properties
      "};\n"
      "\n"
      "%s" // Exit namespace
      "\n"
      "#endif /* %s */\n" // endif
        );

    fmt % headerObjectDefine(table); // ifndef
    fmt % headerObjectDefine(table); // define

    fmt % dllMacro(mModelName); // Dll macro

    fmt % includeOrmProperties(table); // Include properties

    fmt % (table->inherits().get() == NULL ? "" : include("../"
        + objectFilePath(table->inherits()))); // , inherits

    fmt % addForwardDeclarations(table, pkObject.usingStruct()); // forward declarations

    // Macro - storable, key type
    fmt % classNameWithNamespace(mModelName, table); // storable
    fmt % pkObject.keyDataType(); // key type

    fmt % enterObjectNamespace(mModelName, table);// enter namespace

    fmt % pkObject.structDeclaration();// primary key struct

    fmt % dllAttributeMacro(mModelName);// dll attribute

    fmt % className(table); // class name

    // model , class, inherits
    fmt % modelClassNameWithNamespace(mModelName, mModelClass);
    fmt % classNameWithNamespace(mModelName, table);
    fmt % (table->inherits().get() == NULL ? "" : ", "
        + classNameWithNamespace(mModelName, table->inherits())); // , inherits

    fmt % className(table); // constructor
    fmt % className(table); // destructor

    fmt % pkObject.keyFunctionDeclaration();// declare key accessor function

    fmt % addProperties(table); // properties

    fmt % exitObjectNamespace(); // Exit namespace

    fmt % headerObjectDefine(table); // endif

    return fmt.str();
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectHpp::includeOrmProperties(const SmTable::PtrT table)
  {
    bool includeProperty = false;
    bool includeRelatedProperty = false;
    bool includeRelatedToManyProperty = false;

    SmTable::ColumnsListT columnLists;
    table->columns(columnLists);
    for (SmTable::ColumnsListT::const_iterator itr = columnLists.begin(); //
    itr != columnLists.end(); ++itr)
    {
      const SmColumn::PtrT& column = *itr;

      if (!column->isRelatedFromEmpty())
        includeRelatedToManyProperty = true;

      if (column->relatesTo().get() != NULL)
        includeRelatedProperty = true;
      else
        includeProperty = true;
    }

    std::string result;

    if (includeProperty)
      result += include("orm/v1/SimpleProperty");

    if (includeRelatedProperty)
      result += include("orm/v1/RelatedProperty");

    if (includeRelatedToManyProperty)
      result += include("orm/v1/RelatedToManyProperty");

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectHpp::addForwardDeclarations(const SmTable::PtrT table,
      bool declarePkStruct)
  {
    std::string result;
    result += enterModelNamespace(mModelName);

    boost::format fmt("  class %s;\n");

    // Forward declare the model
    fmt % mModelClass;
    result += fmt.str();
    result += "\n";

    typedef std::multimap< SmSchema::PtrT, SmTable::PtrT > MapT;
    MapT map;

    SmTable::ListT objectsToDeclare;
    generateRelatesFrom(table, objectsToDeclare);
    generateRelatesTo(table, objectsToDeclare);

    // We want a declaration to this object to.
    objectsToDeclare.push_back(table);

    while (!objectsToDeclare.empty())
    {
      map.insert(std::make_pair(objectsToDeclare.back()->schema(),
          objectsToDeclare.back()));
      objectsToDeclare.pop_back();
    }

    std::string openNamespace;
    for (MapT::iterator itr = map.begin(); itr != map.end(); ++itr)
    {
      SmSchema::PtrT schema = itr->first;
      SmTable::PtrT tbl = itr->second;

      if (openNamespace != formatLowerCase(niceName(schema)))
      {
        if (!openNamespace.empty())
          result += "}\n\n";

        openNamespace = formatLowerCase(niceName(schema));
        result += "namespace " + openNamespace + " {\n";
      }

      fmt.clear_binds();
      fmt % className(tbl);
      result += fmt.str();

      // Add in this pk struct when we're adding in this table
      if (tbl == table && declarePkStruct)
      {
        fmt.clear_binds();
        fmt % pkStructName(tbl);
        result += fmt.str();
      }
    }

    if (!openNamespace.empty())
      result += "}\n\n";

    result += exitModelNamespace();
    return result;

  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectHpp::addProperties(const boost::shared_ptr< SmTable > table)
  {
    std::string result;

    SmTable::ColumnsListT columnLists;
    table->columns(columnLists);
    for (SmTable::ColumnsListT::const_iterator itr = columnLists.begin(); //
    itr != columnLists.end(); ++itr)
    {
      const SmColumn::PtrT& column = *itr;

      if (!isExportEnabled(column))
        continue;

      result += addProperty(*itr);
      result += addPropertyBackReference(*itr);
    }

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectHpp::addProperty(const boost::shared_ptr< SmColumn > column)
  {
    std::string result;

    if (column->relatesTo().get() == NULL)
    {
      bool inherits = isColumnInherited(column);

      // Property
      // ORM_V1_SIMPLE_PROPERTY(SmModel, id, Id, int32_t)
      boost::format fmt("    ORM_V1_DECLARE_SIMPLE_PROPERTY%s(%s, %s, %s)\n");

      fmt % (inherits ? "_INHERITED" : "");
      fmt % objectProperyGetterName(column);
      fmt % formatCamelCase(niceName(column));
      fmt % column->cppDataType();

      result += fmt.str();
    }
    else
    {
      // Related Property
      // ORM_V1_RELATED_PROPERTY(SmModel, valueType, ValueType, int32_t, SmValueType)
      boost::format
          fmt("    ORM_V1_DECLARE_RELATED_PROPERTY(%s, %s, %s, %s)\n");

      fmt % formatCamelCase(niceName(column), "", true);
      fmt % formatCamelCase(niceName(column));
      fmt % column->cppDataType();
      fmt % classNameWithNamespace(mModelName, column->relatesTo()->table());

      result += fmt.str();
    }

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectHpp::addPropertyBackReference(const boost::shared_ptr< SmColumn > column)
  {
    std::string result;

    SmColumn::RelatedFromListT columnRelatedFrom;
    column->relatedFrom(columnRelatedFrom);
    for (SmColumn::RelatedFromListT::const_iterator
        itr = columnRelatedFrom.begin(); //
    itr != columnRelatedFrom.end(); ++itr)
    {
      const SmColumn::PtrT& otherColumn = *itr;

      qDebug("addProperty : %s is related from %s",
          column->fullName().c_str(),
          otherColumn->fullName().c_str());

      // Related To Many
      // ORM_V1_RELATED_TO_MANY_PROPERTY(SmModel, relatedFrom, RelatedFrom, SmColumn)
      boost::format
          fmt("    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(%s, %s, %s)\n");

      fmt % (formatCamelCase(niceName(otherColumn->table()), "", true)
          + formatCamelCase(niceName(otherColumn)) + "s");
      fmt % (formatCamelCase(niceName(otherColumn->table()))
          + formatCamelCase(niceName(otherColumn)) + "s");
      fmt % classNameWithNamespace(mModelName, otherColumn->table());

      result += fmt.str();
    }

    return result;
  }
/// -------------------------------------------------------------------------

}
