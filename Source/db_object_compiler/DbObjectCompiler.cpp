#include "DbObjectCompiler.h"

#include <QtGui/qmessagebox.h>

#include "Main.h"

#include "Logger.h"
#include "ObjectCpp.h"
#include "ObjectHpp.h"
#include "ModelCpp.h"
#include "ModelHpp.h"
#include "ObjectTypeHpp.h"
#include "ValueTypeHpp.h"
#include "ObjectDeclarationHpp.h"
#include "SmOrmModel.h"

#include "DbBackup.h"

DbObjectCompiler::DbObjectCompiler(QWidget *parent)
{
  ui.setupUi(parent);
  connect(ui.btnCompile, SIGNAL(clicked()), this, SLOT(DoCompile()));
}
/// ---------------------------------------------------------------------------

DbObjectCompiler::~DbObjectCompiler()
{

}
/// ---------------------------------------------------------------------------


void
DbObjectCompiler::Reset()
{
  ui.lstLog->clear();
}
/// ---------------------------------------------------------------------------


void
DbObjectCompiler::DoCompile()
{
  ui.lstLog->clear();

  ui.lstLog->addItem("Loading model");

  SmModel::PtrT
      model(new SmModel(Main::Inst().metaDataDatabaseConnectionSettings()));
  assert(model->load());

  Main::DbocSettings settings = Main::Inst().dbocSettings();
  Main::DbMetaSettings dbMetaSettings = Main::Inst().dbmetaSettings();

  dboc::Logger logger(ui.lstLog);

  ui.lstLog->addItem("Creating object cpp files");
  dboc::ObjectCpp objectCpp(logger,
      settings.path,
      dbMetaSettings.defaultSchema,
      settings.modelName,
      settings.modelClass,
      model);
  objectCpp.Run();

  ui.lstLog->addItem("Creating object hpp files");
  dboc::ObjectHpp objectHpp(logger,
      settings.path,
      settings.modelName,
      settings.modelClass,
      model);
  objectHpp.Run();

  ui.lstLog->addItem("Creating model cpp file");
  dboc::ModelCpp modelCpp(logger,
      settings.path,
      settings.modelName,
      settings.modelClass,
      model);
  modelCpp.Run();

  ui.lstLog->addItem("Creating model hpp file");
  dboc::ModelHpp modelHpp(logger,
      settings.path,
      settings.modelName,
      settings.modelClass,
      model);
  modelHpp.Run();

  ui.lstLog->addItem("Creating object types Hpp file");
  dboc::ObjectTypeHpp objectTypeHpp(logger,
      settings.path,
      settings.modelName,
      settings.modelClass,
      model);
  objectTypeHpp.Run();

  ui.lstLog->addItem("Creating value type Hpp file");
  dboc::ValueTypeHpp valueTypeHpp(logger,
      settings.path,
      settings.modelName,
      settings.modelClass,
      model);
  valueTypeHpp.Run();

  ui.lstLog->addItem("Creating model declarations Hpp file");
  dboc::ObjectDeclarationHpp declarationsHpp(logger,
      settings.path,
      settings.modelName,
      settings.modelClass,
      model);
  declarationsHpp.Run();

  ui.lstLog->addItem("Backing up metadata");
  DbBackup dbBackup;
  dbBackup.run();

  ui.lstLog->addItem(QString("Backed up to : ") + dbBackup.dumpFile().c_str());

  if (!dbBackup.output().empty())
    ui.lstLog->addItem(QString("Backup stdout : ") + dbBackup.output().c_str());

  if (!dbBackup.error().empty())
    ui.lstLog->addItem(QString("Backup stderr : ") + dbBackup.error().c_str());

  ui.lstLog->addItem("Compile complete");
  QMessageBox::information(ui.btnCompile,
      "Compile Complete",
      "Completed compiling the database objects into C++ code");
}
/// ---------------------------------------------------------------------------
