/*
 * HppModel.h
 *
 *  Created on: Feb 13, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _DBOC_OBJECT_TYPE_HPP_H_
#define _DBOC_OBJECT_TYPE_HPP_H_

#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>

#include "Logger.h"

class SmModel;
class SmObjectType;

/*! dboc::HppModel Brief Description
 * Long comment about the class
 *
 */
namespace dboc
{

  class ObjectTypeHpp
  {
    public:
      ObjectTypeHpp(Logger& logger, const std::string outputPath,
          const std::string modelName,
          const std::string modelClass,
          boost::shared_ptr< SmModel > model);

      virtual
      ~ObjectTypeHpp();

      void
      Run();

    private:
      std::string
      generate();

      std::string
      addRootObjectTypes();

      std::string
      addObjectType(const boost::shared_ptr< SmObjectType > objectType);

      std::string
      addSubObjectTypes(const boost::shared_ptr< SmObjectType > objectType);



    private:
      Logger& mLogger;
      const std::string mOutputPath;

      const std::string mModelName;
      const std::string mModelClass;

      boost::shared_ptr< SmModel > mModel;

  };

}

#endif /* _DBOC_OBJECT_TYPE_HPP_H_ */
