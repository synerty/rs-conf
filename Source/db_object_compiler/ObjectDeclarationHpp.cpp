/*
 * HppObject.cpp
 *
 *  Created on: Feb 13, 2011
 *      Author: Jarrod Chesney
 */

#include "ObjectDeclarationHpp.h"

#include <map>

#include <QtCore/qglobal.h>

#include <boost/format.hpp>

#include "SmOrmModel.h"
#include "SmSchema.h"
#include "SmTable.h"
#include "SmColumn.h"

#include "DbOcFormat.h"
#include "DbocFile.h"
#include "ObjectPk.h"


namespace dboc
{

  ObjectDeclarationHpp::ObjectDeclarationHpp(Logger& logger,
      const std::string outputPath,
      const std::string modelName,
      const std::string modelClass,
      boost::shared_ptr< SmModel > model) :
    mLogger(logger), //
        mOutputPath(outputPath), //
        mModelName(modelName), //
        mModelClass(modelClass),//
        mModel(model)
  {
  }
  /// -------------------------------------------------------------------------

  ObjectDeclarationHpp::~ObjectDeclarationHpp()
  {
  }
  /// -------------------------------------------------------------------------

  void
  ObjectDeclarationHpp::Run()
  {
    File file(mLogger,
        mOutputPath,
        objectDeclarationFilename(mModelName) + ".h");
    file.save(generate());
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectDeclarationHpp::generate()
  {
    boost::format fmt("#ifndef %s\n" // ifndef
      "#define %s\n" // define
      "\n"
      "#include <vector>\n"
      "#include <boost/shared_ptr.hpp>\n"
      "\n"
      "%s" // model namespace
      "\n"
      "%s" // forward declarations
      "\n"
      "%s" // exit model namespace
      "\n"
      "#endif /* %s */\n" // endif
        );

    fmt % headerObjectDeclataionDefine(mModelName); // ifndef
    fmt % headerObjectDeclataionDefine(mModelName); // define

    fmt % enterModelNamespace(mModelName); // model namespace

    fmt % addForwardDeclarations(); // forward declarations

    fmt % exitModelNamespace(); // exit model namespace

    fmt % headerObjectDeclataionDefine(mModelName); // endif

    return fmt.str();
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectDeclarationHpp::addForwardDeclarations()
  {
    std::string result;

    boost::format fmt("  class %s;\n"
      "  typedef boost::shared_ptr< %s > %sPtrT;\n"
      "  typedef std::vector< %sPtrT > %sListT;\n"
      "\n");

    // Forward declare the model
    fmt % mModelClass;
    fmt % mModelClass;
    fmt % mModelClass;
    fmt % mModelClass;
    fmt % mModelClass;
    result += fmt.str();
    result += "\n";

    typedef std::multimap< SmSchema::PtrT, SmTable::PtrT > MapT;
    MapT map;

    SmTable::ListT relates = mModel->Tables();

    while (!relates.empty())
    {
      if (isExportEnabled(relates.back()))
        map.insert(std::make_pair(relates.back()->schema(), relates.back()));
      relates.pop_back();
    }

    std::string openNamespace;
    for (MapT::iterator itr = map.begin(); itr != map.end(); ++itr)
    {
      SmSchema::PtrT schema = itr->first;
      SmTable::PtrT table = itr->second;

      if (openNamespace != formatLowerCase(niceName(schema)))
      {
        if (!openNamespace.empty())
          result += "}\n\n";

        openNamespace = formatLowerCase(niceName(schema));
        result += "namespace " + openNamespace + " {\n";
      }

      ObjectPk pkObject(mModelName, table);
      if (pkObject.usingStruct())
        result += "  struct " + pkStructName(table) + ";\n";

      fmt.clear_binds();
      fmt % className(table);
      fmt % className(table);
      fmt % className(table);
      fmt % className(table);
      fmt % className(table);
      result += fmt.str();
    }

    if (!openNamespace.empty())
      result += "}\n\n";

    return result;

  }
/// -------------------------------------------------------------------------

}
