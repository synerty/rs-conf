/*
 * Logger.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef LOGGER_H_
#define LOGGER_H_

#include <string>

class QListWidget;

/*! dboc::Logger Brief Description
 * Long comment about the class
 *
 */
namespace dboc
{

  class Logger
  {
    public:
      Logger(QListWidget* listWidget);
      virtual
      ~Logger();

    public:
      void
      operator()(const std::string message);

    public:
      QListWidget* mListWidget;
  };

}

#endif /* LOGGER_H_ */
