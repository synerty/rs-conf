/*
 * HppModel.h
 *
 *  Created on: Feb 13, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _DBOC_VALUE_TYPE_HPP_H_
#define _DBOC_VALUE_TYPE_HPP_H_

#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>

#include "Logger.h"

class SmModel;
class SmValueType;
class SmValue;

/*! dboc::HppModel Brief Description
 * Long comment about the class
 *
 */
namespace dboc
{

  class ValueTypeHpp
  {
    public:
      ValueTypeHpp(Logger& logger, const std::string outputPath,
          const std::string modelName,
          const std::string modelClass,
          boost::shared_ptr< SmModel > model);

      virtual
      ~ValueTypeHpp();

      void
      Run();

    private:
      std::string
      generate();

      std::string
      addValueTypes();

      std::string
      addValueType(const boost::shared_ptr< SmValueType > valueType);

      std::string
      addEnum(const boost::shared_ptr< SmValueType > valueType);

      std::string
      addValue(const boost::shared_ptr< SmValue > value);

    private:
      Logger& mLogger;
      const std::string mOutputPath;

      const std::string mModelName;
      const std::string mModelClass;

      boost::shared_ptr< SmModel > mModel;

  };

}

#endif /* _DBOC_VALUE_TYPE_HPP_H_ */
