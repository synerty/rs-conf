/*
 * HppModel.cpp
 *
 *  Created on: Feb 13, 2011
 *      Author: Jarrod Chesney
 */

#include "ValueTypeHpp.h"

#include <boost/filesystem.hpp>

#include <QtCore/qglobal.h>

#include "SmOrmModel.h"
#include "SmSchema.h"
#include "SmTable.h"
#include "SmColumn.h"
#include "SmValueType.h"
#include "SmValue.h"

#include "DbOcFormat.h"
#include "DbocFile.h"

namespace dboc
{

  ValueTypeHpp::ValueTypeHpp(Logger& logger, const std::string outputPath,
      const std::string modelName,
      const std::string modelClass,
      boost::shared_ptr< SmModel > model) :
      mLogger(logger), //
    mOutputPath(outputPath), //
        mModelName(modelName), //
        mModelClass(modelClass),//
        mModel(model)
  {
  }
  /// -------------------------------------------------------------------------

  ValueTypeHpp::~ValueTypeHpp()
  {
  }
  /// -------------------------------------------------------------------------

  void
  ValueTypeHpp::Run()
  {
    File file(mLogger, mOutputPath, valueTypeFilename(mModelName) + ".h");
    file.save(generate());
  }
  /// -------------------------------------------------------------------------

  std::string
  ValueTypeHpp::generate()
  {
    boost::format fmt("#ifndef %s\n" // ifndef
      "#define %s\n" // define
      "\n"
      "%s" // enter namespace
      "\n"
      "%s" // Value type enums
      "\n"
      "%s" // Exit namespace
      "\n"
      "#endif /* %s */\n" // endif
        );

    fmt % headerValueTypeDefine(mModelName); // ifndef
    fmt % headerValueTypeDefine(mModelName); // define

    fmt % enterModelNamespace(mModelName);

    fmt % addValueTypes(); // enum values

    fmt % exitModelNamespace(); // exit namespace

    fmt % headerValueTypeDefine(mModelName); // endif

    return fmt.str();
  }
  /// -------------------------------------------------------------------------

  std::string
  ValueTypeHpp::addValueTypes()
  {
    std::string result;

    for (SmValueType::ListT::const_iterator itr = mModel->ValueTypes().begin(); //
    itr != mModel->ValueTypes().end(); ++itr)
      result += addValueType(*itr);

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  ValueTypeHpp::addValueType(const boost::shared_ptr< SmValueType > valueType)
  {
    // ValueType = 123,
    boost::format fmt(""
      "namespace %s\n" // Value type name
      "{\n"
      "  static const int valueType = %d;\n"
      "\n"
      "%s" // enum
      "}\n"
      "\n");

    fmt % formatCamelCase(valueType->name(), "", false, " ");
    fmt % valueType->id();
    fmt % addEnum(valueType);

    return fmt.str();
  }
  /// -------------------------------------------------------------------------

  std::string
  ValueTypeHpp::addEnum(const boost::shared_ptr< SmValueType > valueType)
  {
    if (!valueType->generateEnum())
      return "";

    std::string enumValues;

    SmValueType::ValuesListT values;
    valueType->values(values);
    for (SmValueType::ValuesListT::const_iterator itr = values.begin(); //
    itr != values.end(); ++itr)
      enumValues += addValue(*itr);

    // ValueType = 123,
    boost::format fmt(""
      "  enum %sE\n"// Value type name
      "  {\n"
      "%s" // Values
      "  };\n"
      "\n");

    fmt % formatCamelCase(valueType->name(), "", false, " ");
    fmt % enumValues;

    return fmt.str();
  }
  /// -------------------------------------------------------------------------

  std::string
  ValueTypeHpp::addValue(const boost::shared_ptr< SmValue > value)
  {
    // ValueType = 123,
    boost::format fmt("    %sValue = %d,\n");

    std::string name = formatCamelCase(value->name(), "", true, " ");
    assert(!name.empty());

    if (std::isdigit(name[0]))
      name = "_" + name;

    fmt % name;
    fmt % value->id();

    return fmt.str();
  }
/// -------------------------------------------------------------------------

}
