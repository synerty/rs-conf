/*
 * HppModel.cpp
 *
 *  Created on: Feb 13, 2011
 *      Author: Jarrod Chesney
 */

#include "ModelHpp.h"

#include <QtCore/qglobal.h>
#include <boost/format.hpp>

#include "SmOrmModel.h"
#include "SmSchema.h"
#include "SmTable.h"
#include "SmColumn.h"

#include "DbOcFormat.h"
#include "DbocFile.h"
#include "ObjectPk.h"

namespace dboc
{

  ModelHpp::ModelHpp(Logger& logger,
      const std::string outputPath,
      const std::string modelName,
      const std::string modelClass,
      boost::shared_ptr< SmModel > model) :
    mLogger(logger), //
        mOutputPath(outputPath), //
        mModelName(modelName), //
        mModelClass(modelClass),//
        mModel(model)
  {
  }
  /// -------------------------------------------------------------------------

  ModelHpp::~ModelHpp()
  {
  }
  /// -------------------------------------------------------------------------

  void
  ModelHpp::Run()
  {
    File file(mLogger, mOutputPath, modelFilename(mModelClass) + ".h");
    file.save(generate());
  }
  /// -------------------------------------------------------------------------

  std::string
  ModelHpp::generate()
  {
    boost::format fmt("#ifndef %s\n" // ifndef
      "#define %s\n" // define
      "\n"
      "%s" // Dll macro
      "\n"
      "#include \"orm/v1/Model.h\"\n"
      "\n"
      "%s" // forward declaration
      "\n"
      "%s" // enter namespace
      "\n"
      "class %s %s\n" // dll attribute, class name
      "    : public orm::v1::Model< %s>\n" // Model template
      "{\n"
      "  public:\n"
      "    %s(const orm::v1::Conn& ormConn = orm::v1::Conn());\n" // constructor
      "    virtual ~%s();\n" // destructor

      "\n"
      "  public:\n"
      "%s" // objects
      "};\n"
      "\n"
      "%s" // Exit namespace
      "\n"
      "#endif /* %s */\n" // endif
        );

    fmt % headerModelDefine(mModelName); // ifndef
    fmt % headerModelDefine(mModelName); // define

    fmt % dllMacro(mModelName); // Dll macro

    fmt % include(objectDeclarationFilename(mModelName));

    fmt % enterModelNamespace(mModelName);

    fmt % dllAttributeMacro(mModelName);// dll attribute

    fmt % mModelClass; //  class name
    fmt % mModelClass; // model template

    fmt % mModelClass; // constructor
    fmt % mModelClass; // destructor

    fmt % addObjects(); // properties

    fmt % exitModelNamespace(); // exit namespace

    fmt % headerModelDefine(mModelName); // endif

    return fmt.str();
  }
  /// -------------------------------------------------------------------------

  std::string
  ModelHpp::addObjects()
  {
    std::string result;

    for (SmTable::ListT::const_iterator itr = mModel->Tables().begin(); //
    itr != mModel->Tables().end(); ++itr)
      result += addObject(*itr);

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  ModelHpp::addObject(const boost::shared_ptr< SmTable > table)
  {
    if (!isExportEnabled(table))
      return "";

    ObjectPk pkObject(mModelName, table);

    // ORM_V1_MODEL_DEFINE_STORABLE(Is, Schema, std::string)

    // Property
    boost::format fmt("    ORM_V1_MODEL_DECLARE_STORABLE(%s, %s, %s)\n");

    fmt % classNameWithNamespace(mModelName, table);
    fmt % modelVariableName(table);
    fmt % pkObject.keyDataType();

    return fmt.str();
  }
/// -------------------------------------------------------------------------

}
