/*
 * HppModel.cpp
 *
 *  Created on: Feb 13, 2011
 *      Author: Jarrod Chesney
 */

#include "ObjectTypeHpp.h"

#include <boost/filesystem.hpp>

#include <QtCore/qglobal.h>

#include "SmOrmModel.h"
#include "SmSchema.h"
#include "SmTable.h"
#include "SmColumn.h"
#include "SmObjectType.h"

#include "DbOcFormat.h"
#include "DbocFile.h"

namespace dboc
{

  ObjectTypeHpp::ObjectTypeHpp(Logger& logger, const std::string outputPath,
      const std::string modelName,
      const std::string modelClass,
      boost::shared_ptr< SmModel > model) :
      mLogger(logger), //
    mOutputPath(outputPath), //
        mModelName(modelName), //
        mModelClass(modelClass),//
        mModel(model)
  {
  }
  /// -------------------------------------------------------------------------

  ObjectTypeHpp::~ObjectTypeHpp()
  {
  }
  /// -------------------------------------------------------------------------

  void
  ObjectTypeHpp::Run()
  {
    File file(mLogger, mOutputPath, objectTypeFilename(mModelName) + ".h");
    file.save(generate());
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectTypeHpp::generate()
  {
    boost::format fmt("#ifndef %s\n" // ifndef
      "#define %s\n" // define
      "\n"
      "%s" // enter namespace
      "\n"
      "namespace obj_typ\n"
      "{\n"
      "\n"
      "%s" // Object type enums
      "\n"
      "}\n"
      "\n"
      "%s" // Exit namespace
      "\n"
      "#endif /* %s */\n" // endif
        );

    fmt % headerObjectTypeDefine(mModelName); // ifndef
    fmt % headerObjectTypeDefine(mModelName); // define

    fmt % enterModelNamespace(mModelName);

    fmt % addRootObjectTypes(); // enum objects

    fmt % exitModelNamespace(); // exit namespace

    fmt % headerObjectTypeDefine(mModelName); // endif

    return fmt.str();

  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectTypeHpp::addRootObjectTypes()
  {
    std::string result;

    for (SmObjectType::ListT::const_iterator
        itr = mModel->ObjectTypes().begin(); //
    itr != mModel->ObjectTypes().end(); ++itr)
    {
      if ((*itr)->inherits() == NULL)
        result += addObjectType(*itr);
    }

    return result;
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectTypeHpp::addObjectType(const boost::shared_ptr< SmObjectType > objectType)
  {
    // ObjectType = 123,
    boost::format fmt(""
      "namespace %s\n" // Object type name
      "{\n"
      "  static const int objTyp = %d;\n"
      "\n"
      "%s" // sub object Types
      "}\n"
      "\n");

    std::string name = formatCamelCase(objectType->name(), "", false, " ");
    assert(!name.empty());

    if (std::isdigit(name[0]))
      name = "_" + name;

    fmt % name;
    fmt % objectType->id();

    fmt % addSubObjectTypes(objectType);

    return fmt.str();
  }
  /// -------------------------------------------------------------------------

  std::string
  ObjectTypeHpp::addSubObjectTypes(const boost::shared_ptr< SmObjectType > objectType)
  {
    std::string result;

    SmObjectType::ListT objects;
    objectType->inheritedBy(objects);
    for (SmObjectType::ListT::const_iterator itr = objects.begin(); //
    itr != objects.end(); ++itr)
      result += addObjectType(*itr);

    return result;
  }
/// -------------------------------------------------------------------------


}
