/*
 * uHierarchModel.cpp
 *
 *  Created on: Aug 25, 2010
 *      Author: Jarrod Chesney
 */

#include "AppMetaConfigColumnsModel.h"

#include <assert.h>
#include <list>
#include <algorithm>

#include <QtGui/qcolor.h>
#include <QtGui/qbrush.h>

#include "SmOrmModel.h"
#include "SmValueType.h"
#include "SmColumn.h"

#include "QtModelUserRoles.h"

//---------------------------------------------------------------------------
//                           AppMetaConfigColumnsModel private
//---------------------------------------------------------------------------
namespace AppMetaConfigColumnsModelPrivate
{

  struct Node
  {
      AppMetaConfigColumnsModelPrivate::Node* parent;
      int row;
      boost::shared_ptr< SmColumn > smColumn;
      typedef std::vector< AppMetaConfigColumnsModelPrivate::Node* > ChildrenT;
      ChildrenT children;
      std::string name;

      Node() :
        parent(NULL), row(0)
      {
      }
  };

}

using namespace AppMetaConfigColumnsModelPrivate;

//---------------------------------------------------------------------------
//                       AppMetaConfigColumnsModel Implementation
//---------------------------------------------------------------------------

AppMetaConfigColumnsModel::AppMetaConfigColumnsModel(boost::shared_ptr< SmModel > smModel,
    const boost::shared_ptr< SmTable >& smTable) :
  mRootNode(NULL), //
      mSmModel(smModel), //
      mSmTable(smTable)
{
  setupModel();
} //---------------------------------------------------------------------------

AppMetaConfigColumnsModel::~AppMetaConfigColumnsModel()
{
}
//---------------------------------------------------------------------------

// Abstract implementation
QVariant
AppMetaConfigColumnsModel::headerData(int prmSection,
    Qt::Orientation prmOrientation,
    int prmRole) const
{
  if (prmOrientation == Qt::Vertical || prmRole != Qt::DisplayRole)
    return QVariant();

  switch (prmSection)
  {
    case ColName:
      return "Name";

    case ColDataType:
      return "Data Type";

    case ColDataLength:
      return "Data Length";

    case ColValueType:
      return "Value Type";

    default:
      assert(false);
  }

  return QVariant();
}
//---------------------------------------------------------------------------

QModelIndex
AppMetaConfigColumnsModel::index(int row, int column, const QModelIndex& parent) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      row,
  //      column,
  //      reinterpret_cast< uintptr_t > (parent.internalPointer()));

  // Special Case  - If the parent is the root
  if (parent == QModelIndex())
    return createIndex(row, column, mRootNode->children.at(row));

  AppMetaConfigColumnsModelPrivate::Node& parentNode = Node(parent);
  return createIndex(row, column, parentNode.children.at(row));
}
//---------------------------------------------------------------------------

QModelIndex
AppMetaConfigColumnsModel::parent(const QModelIndex& index) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      index.row(),
  //      index.column(),
  //      reinterpret_cast< uintptr_t > (index.internalPointer()));

  assert(index.isValid());

  AppMetaConfigColumnsModelPrivate::Node& node = Node(index);

  // Every node in the model will have a parent
  if (node.parent->smColumn == NULL)
    return QModelIndex();

  return createIndex(node.parent->row, 0, node.parent);
}
//---------------------------------------------------------------------------

int
AppMetaConfigColumnsModel::rowCount(const QModelIndex& parent) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      parent.row(),
  //      parent.column(),
  //      reinterpret_cast< uintptr_t > (parent.internalPointer()));

  if (mRootNode == NULL)
    return 0;

  // Special Case  - If the parent is the root
  if (parent == QModelIndex())
    return mRootNode->children.size();

  return Node(parent).children.size();
}
//---------------------------------------------------------------------------

int
AppMetaConfigColumnsModel::columnCount(const QModelIndex& parent) const
{
  return ColCount;
}
//---------------------------------------------------------------------------

Qt::ItemFlags
AppMetaConfigColumnsModel::flags(const QModelIndex &index) const
{
  Qt::ItemFlags f = QAbstractItemModel::flags(index);
  const int col = index.column();

  if (col == ColName || col == ColValueType)
    f |= Qt::ItemIsEditable;

  return f;
}
//---------------------------------------------------------------------------

QVariant
AppMetaConfigColumnsModel::data(const QModelIndex& index, int role) const
{
  // We only provide certain data.
  assert (index.isValid() && (0 <= index.column() && index.column() < ColCount));
  const AppMetaConfigColumnsModelPrivate::Node& node = Node(index);
  assert(node.smColumn.get());

  switch (index.column())
  {
    case ColName:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case Qt::EditRole:
          return QString(node.smColumn->name().c_str());

      }// END switch (role)
      break;
    }
    case ColValueType:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case Qt::EditRole:
          if (node.smColumn->valueType().get() == NULL)
            return QVariant();

          return QString(node.smColumn->valueType()->name().c_str());

        case Qt::ComboOptionsRole:
        {
          assert(mSmModel.get() != NULL);

          QStringList list;
          list.push_back("");

          for (SmModel::ValueTypeListT::const_iterator
              itr = mSmModel->ValueTypes().begin(); //
          itr != mSmModel->ValueTypes().end(); ++itr)
          {
            const SmValueType::PtrT& valueType = *itr;
            list.push_back(valueType->name().c_str());
          }

          list.sort();
          return list;
        }

      }// END switch (role)
      break;
    }
    case ColDataType:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
          return QString(node.smColumn->dataType().c_str());

      }// END switch (role)
      break;
    }
    case ColDataLength:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
          return node.smColumn->dataLength();

      }// END switch (role)
      break;
    }

  } // END switch (index.column())
  return QVariant();
}
//---------------------------------------------------------------------------

bool
AppMetaConfigColumnsModel::setData(const QModelIndex &index,
    const QVariant &value,
    int role)
{
  // We only provide certain data.
  assert (index.isValid() && (0 <= index.column() && index.column() < ColCount));
  const AppMetaConfigColumnsModelPrivate::Node& node = Node(index);
  assert(node.smColumn.get());

  if (role != Qt::EditRole)
    return false;

  switch (index.column())
  {
    case ColName:
    {
      node.smColumn->setName(value.toString().toStdString());
      return true;
    }
    case ColValueType:
    {
      assert(mSmModel.get() != NULL);
      const std::string strVal = value.toString().toStdString();
      for (SmModel::ValueTypeListT::const_iterator
          itr = mSmModel->ValueTypes().begin(); //
      itr != mSmModel->ValueTypes().end(); ++itr)
      {
        const SmValueType::PtrT& valueType = *itr;
        if (valueType->name() == strVal)
        {
          node.smColumn->setValueType(valueType);
          return true;
        }

      }
      return false;
    }
  }

  return false;
}
//---------------------------------------------------------------------------

AppMetaConfigColumnsModelPrivate::Node&
AppMetaConfigColumnsModel::Node(const QModelIndex& index) const
{
  if (!index.isValid())
    assert(false);

  AppMetaConfigColumnsModelPrivate::Node
      * obj = static_cast< AppMetaConfigColumnsModelPrivate::Node* > (index.internalPointer());
  obj = dynamic_cast< AppMetaConfigColumnsModelPrivate::Node* > (obj);
  assert(obj);
  return *obj;
}
//---------------------------------------------------------------------------

void
AppMetaConfigColumnsModel::setupModel()
{
  mRootNode = &mNodes[SmColumn::PtrT()];

  for (SmModel::ColumnListT::const_iterator itr = mSmModel->Columns().begin();//
  itr != mSmModel->Columns().end(); ++itr)
  {
    const SmColumn::PtrT& smColumn = *itr;
    assert(smColumn.get());

    if (mSmTable != smColumn->table())
      continue;

    // Get the parent (recursion is fun)
    AppMetaConfigColumnsModelPrivate::Node* node = &mNodes[smColumn];

    // Set the parent
    mRootNode->children.push_back(node);
    node->parent = mRootNode;
    node->row = mRootNode->children.size() - 1;
    node->smColumn = smColumn;

  }
}
//---------------------------------------------------------------------------
