#include "AppMetaConfig.h"

#include <QtGui/qstandarditemmodel.h>
#include <QtGui/qmessagebox.h>

#include "Main.h"

#include "SmOrmModel.h"
#include "SmSchema.h"
#include "SmTable.h"
#include "AppMetaConfigObjectsModel.h"
#include "AppMetaConfigColumnsModel.h"
#include "PropertiesModel.h"
#include "ComboItemDelegate.h"

AppMetaConfig::AppMetaConfig(QWidget *parent)
{
  ui.setupUi(parent);

  ui.treeObjects->setItemDelegateForColumn(AppMetaConfigObjectsModel::ColDataTable,
      new ComboItemDelegate(parent));

  ui.treeObjects->setItemDelegateForColumn(AppMetaConfigObjectsModel::ColObjectTypeTable,
      new ComboItemDelegate(parent));

  ui.tblColumns->setItemDelegateForColumn(AppMetaConfigColumnsModel::ColValueType,
      new ComboItemDelegate(parent));

  connect(ui.btnLoad, SIGNAL(clicked()), this, SLOT(DoLoad()));
  connect(ui.btnSave, SIGNAL(clicked()), this, SLOT(DoSave()));
  connect(ui.btnAddObjectType, SIGNAL(clicked()), this, SLOT(DoAddObjectType()));
  connect(ui.btnAddSubObjectType,
      SIGNAL(clicked()),
      this,
      SLOT(DoAddSubObjectType()));

}
// ----------------------------------------------------------------------------

AppMetaConfig::~AppMetaConfig()
{

}
// ----------------------------------------------------------------------------

void
AppMetaConfig::Reset()
{
  mSmModel.reset(new SmModel(Main::Inst().metaDataDatabaseConnectionSettings()));

  ui.treeObjects->setModel(NULL);
  ui.tblColumns->setModel(NULL);
  ui.treeProperties->setModel(NULL);

  mQtObjectsModel.reset();
  mQtPropertyModel.reset();
  mQtColumnModel.reset();
}
// ----------------------------------------------------------------------------

void
AppMetaConfig::DoLoad()
{
  try
  {
    mSmModel.reset(new SmModel(Main::Inst().metaDataDatabaseConnectionSettings()));
    assert(mSmModel->load());
  }
  catch (std::runtime_error& e)
  {
    QString msg = "An exception occured while loading the data\n";
    msg += e.what();
    QMessageBox::critical(ui.treeObjects, "Load Exception", msg);
    return;
  }

  mQtObjectsModel.reset(new AppMetaConfigObjectsModel(mSmModel));
  ui.treeObjects->setModel(mQtObjectsModel.get());
  ui.treeObjects->resizeColumnToContents(0);
  ui.treeObjects->resizeColumnToContents(1);
  DoItemSelected(QModelIndex(), QModelIndex());

connect(ui.treeObjects->selectionModel(),
    SIGNAL(currentRowChanged(const QModelIndex&,const QModelIndex&)),
    this,
    SLOT(DoItemSelected(const QModelIndex&, const QModelIndex&)));

}
// ----------------------------------------------------------------------------

void
AppMetaConfig::DoSave()
{
  try
  {
    assert(mSmModel->save());
    if (mQtPropertyModel.get() != NULL)
      mQtPropertyModel->save();
  }
  catch (std::exception& e)
  {
    QString msg = "An error occured saving the data, error was :\n";
    msg += e.what();
    QMessageBox::critical(ui.btnSave, "Save Failed", msg);
    return;
  }
}
// ----------------------------------------------------------------------------

void
AppMetaConfig::DoAddObjectType()
{
  if (mQtObjectsModel.get() == NULL)
    return;

  SelectIndex(mQtObjectsModel->AddObjectType());
}
// ----------------------------------------------------------------------------

void
AppMetaConfig::DoAddSubObjectType()
{
  if (mQtObjectsModel.get() == NULL)
    return;

  QModelIndexList
      selectedIndexs = ui.treeObjects->selectionModel()->selectedRows();

  SmObjectType::PtrT objectType;

  if (!selectedIndexs.empty())
    objectType = mQtObjectsModel->ObjectType(selectedIndexs.front());

  SelectIndex(mQtObjectsModel->AddObjectType(objectType));
}
// ----------------------------------------------------------------------------

void
AppMetaConfig::DoItemSelected(const QModelIndex & current,
    const QModelIndex & previous)
{
  ui.tblColumns->setModel(NULL);
  ui.treeProperties->setModel(NULL);

  if (!current.isValid())
    return;

  SmObjectType::PtrT objectType = mQtObjectsModel->ObjectType(current);
  assert(objectType.get());

  // Object Properties, Get the last object types table.
  std::string objectTypeTable = objectType->config()->sqlOpts()->tableName();
  for (SmObjectType::PtrT objItr = objectType; objItr->inherits().get() != NULL; objItr = objItr->inherits())
  {
    if (objItr->inherits()->objectTypeTable().get() != NULL)
    {
      objectTypeTable = objItr->inherits()->objectTypeTable()->fullName();
      break;
    }
  }

  mQtPropertyModel.reset(new PropertiesModel(mSmModel,
      objectType->id(),
      objectTypeTable));
  ui.treeProperties->setModel(mQtPropertyModel.get());
  ui.treeProperties->expandAll();
  ui.treeProperties->resizeColumnToContents(0);
  ui.treeProperties->resizeColumnToContents(1);

  // Columns
  if (objectType->dataTable().get() != NULL)
  {
    mQtColumnModel.reset(new AppMetaConfigColumnsModel(mSmModel,
        objectType->dataTable()));
    ui.tblColumns->setModel(mQtColumnModel.get());
    ui.tblColumns->resizeColumnToContents(1);
  }
}
// ----------------------------------------------------------------------------

void
AppMetaConfig::SelectIndex(QModelIndex index)
{
  // Create a reverse list of parents
  QModelIndexList parents;
  for (QModelIndex parent = index; //
  parent != QModelIndex(); parent = parent.parent())
    parents.push_front(parent);

  // Expand the parents in order
  foreach(QModelIndex parent, parents)
  ui.treeObjects->expand(parent);

  // Select the item
  ui.treeObjects->selectionModel()->select(index,
      QItemSelectionModel::ClearAndSelect);
}
// ----------------------------------------------------------------------------
