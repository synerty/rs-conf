/*
 * uHierarchModel.cpp
 *
 *  Created on: Aug 25, 2010
 *      Author: Jarrod Chesney
 */

#include "AppMetaConfigObjectsModel.h"

#include <assert.h>
#include <list>
#include <algorithm>

#include <QtGui/qcolor.h>
#include <QtGui/qbrush.h>

#include "SmOrmModel.h"
#include "SmObjectType.h"
#include "SmValueType.h"
#include "SmTable.h"

#include "QtModelUserRoles.h"

//---------------------------------------------------------------------------
//                           AppMetaConfigObjectsModel private
//---------------------------------------------------------------------------
namespace AppMetaConfigObjectsModelPrivate
{

  enum SmObjectTypeE
  {
    NoObject,
    ObjectTypeNode
  };

  struct Node
  {
      AppMetaConfigObjectsModelPrivate::Node* parent;
      int row;
      SmVariantT* variant;
      SmObjectTypeE objectType;
      typedef std::vector< AppMetaConfigObjectsModelPrivate::Node* > ChildrenT;
      ChildrenT children;
      std::string name;

      Node() :
        parent(NULL), row(0), variant(NULL), objectType(NoObject)
      {
      }
  };

}

using namespace AppMetaConfigObjectsModelPrivate;

//---------------------------------------------------------------------------
//                       AppMetaConfigObjectsModel Implementation
//---------------------------------------------------------------------------

AppMetaConfigObjectsModel::AppMetaConfigObjectsModel(boost::shared_ptr< SmModel > smModel) :
  mRootNode(NULL), //
      mSmModel(smModel)
{
  setupModel();

} //---------------------------------------------------------------------------

AppMetaConfigObjectsModel::~AppMetaConfigObjectsModel()
{
}
//---------------------------------------------------------------------------

// Abstract implementation
QVariant
AppMetaConfigObjectsModel::headerData(int prmSection,
    Qt::Orientation prmOrientation,
    int prmRole) const
{
  if (prmOrientation == Qt::Vertical || prmRole != Qt::DisplayRole)
    return QVariant();

  switch (prmSection)
  {
    case ColName:
      return "Name";

    case ColDataTable:
      return "Data Table";

    case ColObjectTypeTable:
      return "Object Type Table";

    default:
      assert(false);
  }

  return QVariant();
}
//---------------------------------------------------------------------------

QModelIndex
AppMetaConfigObjectsModel::index(int row, int column, const QModelIndex& parent) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      row,
  //      column,
  //      reinterpret_cast< uintptr_t > (parent.internalPointer()));

  // Special Case  - If the parent is the root
  if (parent == QModelIndex())
    return createIndex(row, column, mRootNode->children.at(row));

  AppMetaConfigObjectsModelPrivate::Node& parentNode = Node(parent);
  return createIndex(row, column, parentNode.children.at(row));
}
//---------------------------------------------------------------------------

QModelIndex
AppMetaConfigObjectsModel::parent(const QModelIndex& index) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      index.row(),
  //      index.column(),
  //      reinterpret_cast< uintptr_t > (index.internalPointer()));

  assert(index.isValid());

  AppMetaConfigObjectsModelPrivate::Node& node = Node(index);

  // Every node in the model will have a parent
  if (node.parent->variant == NULL)
    return QModelIndex();

  return createIndex(node.parent->row, 0, node.parent);
}
//---------------------------------------------------------------------------

int
AppMetaConfigObjectsModel::rowCount(const QModelIndex& parent) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      parent.row(),
  //      parent.column(),
  //      reinterpret_cast< uintptr_t > (parent.internalPointer()));

  if (mRootNode == NULL)
    return 0;

  // Special Case  - If the parent is the root
  if (parent == QModelIndex())
    return mRootNode->children.size();

  return Node(parent).children.size();
}
//---------------------------------------------------------------------------

int
AppMetaConfigObjectsModel::columnCount(const QModelIndex& parent) const
{
  return ColCount;
}
//---------------------------------------------------------------------------

Qt::ItemFlags
AppMetaConfigObjectsModel::flags(const QModelIndex &index) const
{
  Qt::ItemFlags f = QAbstractItemModel::flags(index);

  const AppMetaConfigObjectsModelPrivate::Node& node = Node(index);
  const int col = index.column();
  switch (node.objectType)
  {
    case ObjectTypeNode:
    {
      if (col == ColName || col == ColDataTable || col == ColObjectTypeTable)
        f |= Qt::ItemIsEditable;
      break;
    }
    case NoObject:
    {
      assert(false);
      break;
    }
  }

  return f;

  //    case ColIgnore:
  //      f |= Qt::ItemIsUserCheckable;
  //      f |= Qt::ItemIsEditable;
  //      break;

}
//---------------------------------------------------------------------------

QVariant
AppMetaConfigObjectsModel::data(const QModelIndex& index, int role) const
{
  // We only provide certain data.
  if (!index.isValid() || !(0 <= index.column() && index.column() < ColCount))
    return QVariant();

  const AppMetaConfigObjectsModelPrivate::Node& node = Node(index);
  switch (node.objectType)
  {
    case ObjectTypeNode:
    {
      SmObjectType::PtrT
          object = *boost::get< SmObjectType::PtrT >(node.variant);
      return data(object, index, role);
      break;
    }
    case NoObject:
      assert(false);
      break;
  }
  assert(false);
  return QVariant();
}
//---------------------------------------------------------------------------

QVariant
AppMetaConfigObjectsModel::data(SmObjectType::PtrT& object,
    const QModelIndex& index,
    int role) const
{
  assert(object.get() != NULL);

  switch (index.column())
  {
    case ColName:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case Qt::EditRole:
          return QString(object->name().c_str());

      }// END switch (role)
      break;
    }
    case ColDataTable:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case Qt::EditRole:
          if (object->dataTable().get() == NULL)
            return QVariant();
          return QString(object->dataTable()->fullName().c_str());

        case Qt::ComboOptionsRole:
        {
          assert(mSmModel.get() != NULL);

          QStringList list;
          list.push_back("");

          for (SmModel::TableListT::const_iterator
              itr = mSmModel->Tables().begin(); //
          itr != mSmModel->Tables().end(); ++itr)
          {
            const SmTable::PtrT& table = *itr;
            QString name(table->fullName().c_str());
            if (name.contains("meta") && !name.contains("tbl_CONFIG"))
              continue;
            list.push_back(name);
          }

          list.sort();
          return list;
        }

      }// END switch (role)
      break;
    }
    case ColObjectTypeTable:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case Qt::EditRole:
          if (object->objectTypeTable().get() == NULL)
            return QVariant();
          return QString(object->objectTypeTable()->fullName().c_str());

        case Qt::ComboOptionsRole:
        {
          assert(mSmModel.get() != NULL);

          QStringList list;
          list.push_back("");

          for (SmModel::TableListT::const_iterator
              itr = mSmModel->Tables().begin(); //
          itr != mSmModel->Tables().end(); ++itr)
          {
            const SmTable::PtrT& table = *itr;
            QString name(table->fullName().c_str());
            if (!(name.contains("meta") && name.contains("OBJECT_TYPE")))
              continue;
            list.push_back(name);
          }

          list.sort();
          return list;
        }

      }// END switch (role)
      break;
    }

  } // END switch (index.column())

  return QVariant();
}
//---------------------------------------------------------------------------

bool
AppMetaConfigObjectsModel::setData(const QModelIndex &index,
    const QVariant &value,
    int role)
{
  const AppMetaConfigObjectsModelPrivate::Node& node = Node(index);
  switch (node.objectType)
  {
    case ObjectTypeNode:
    {
      SmObjectType::PtrT
          object = *boost::get< SmObjectType::PtrT >(node.variant);
      return setData(object, index, value, role);
      break;
    }
    case NoObject:
      assert(false);
      break;
  }
  return false;
}
//---------------------------------------------------------------------------

bool
AppMetaConfigObjectsModel::setData(SmObjectType::PtrT& object,
    const QModelIndex &index,
    const QVariant &value,
    int role)
{
  if (object.get() == NULL)
    return false;

  if (index.column() == ColName && role == Qt::EditRole)
  {
    object->setName(value.toString().toStdString());
    return true;
  }

  if (index.column() == ColDataTable && role == Qt::EditRole)
  {
    assert(mSmModel.get() != NULL);
    const std::string strVal = value.toString().toStdString();
    if (strVal.empty())
    {
      object->setDataTable(SmTable::PtrT());
      return true;
    }

    for (SmModel::TableListT::const_iterator itr = mSmModel->Tables().begin(); //
    itr != mSmModel->Tables().end(); ++itr)
    {
      const SmTable::PtrT& table = *itr;
      if (table->fullName() == strVal)
      {
        object->setDataTable(table);
        return true;
      }

    }
    return false;
  }

  if (index.column() == ColObjectTypeTable && role == Qt::EditRole)
  {
    assert(mSmModel.get() != NULL);
    const std::string strVal = value.toString().toStdString();
    if (strVal.empty())
    {
      object->setObjectTypeTable(SmTable::PtrT());
      return true;
    }

    for (SmModel::TableListT::const_iterator itr = mSmModel->Tables().begin(); //
    itr != mSmModel->Tables().end(); ++itr)
    {
      const SmTable::PtrT& table = *itr;
      if (table->fullName() == strVal)
      {
        object->setObjectTypeTable(table);
        return true;
      }

    }
    return false;
  }

  return false;
}
//---------------------------------------------------------------------------

AppMetaConfigObjectsModelPrivate::Node&
AppMetaConfigObjectsModel::Node(const QModelIndex& index) const
{
  if (!index.isValid())
    assert(false);

  AppMetaConfigObjectsModelPrivate::Node
      * obj = static_cast< AppMetaConfigObjectsModelPrivate::Node* > (index.internalPointer());
  obj = dynamic_cast< AppMetaConfigObjectsModelPrivate::Node* > (obj);
  assert(obj);
  return *obj;
}
//---------------------------------------------------------------------------

void
AppMetaConfigObjectsModel::setupModel()
{
  mRootNode = &mNodes[SmVariantT()];
  qDebug("mNodes.size() = %d", mNodes.size());

  for (SmObjectType::ListT::const_iterator
      itr = mSmModel->ObjectTypes().begin();//
  itr != mSmModel->ObjectTypes().end(); ++itr)
  {
    SmObjectType::PtrT object = *itr;
    QModelIndex index;
    AppMetaConfigObjectsModelPrivate::Node* node = NULL;
    objectTypeDetails(index, node, object); // Touch the node to create it
  }
}
//---------------------------------------------------------------------------


void
AppMetaConfigObjectsModel::objectTypeDetails(QModelIndex& index,
    AppMetaConfigObjectsModelPrivate::Node*& node,
    const boost::shared_ptr< SmObjectType >& object)
{
  // If the parent is root
  if (object.get() == NULL)
  {
    index = QModelIndex();
    node = mRootNode;
    return;
  }

  // This will create an empty node if it doesn't exist
  node = NodeFromMap(object);

  // If we've found the parent
  if (node->objectType != NoObject)
  {
    assert(node->objectType == ObjectTypeNode);
    index = createIndex(node->row, 0, node);
    return;
  }

  // Otherwise, create it. Value of node variable should still be correct
  index = insertObjectType(object);
}
//---------------------------------------------------------------------------

QModelIndex
AppMetaConfigObjectsModel::insertObjectType(const boost::shared_ptr<
    SmObjectType >& object)
{
  // Get the parent (recursion is fun)
  AppMetaConfigObjectsModelPrivate::Node* parentNode = NULL;
  QModelIndex parentIndex;
  objectTypeDetails(parentIndex, parentNode, object->inherits());

  AppMetaConfigObjectsModelPrivate::Node* node = NodeFromMap(object);
  assert(node->variant == NULL);

  // Set the parent
  node->parent = parentNode;
  node->row = parentNode->children.size();

  node->variant = KeyFromMap(object);
  node->objectType = ObjectTypeNode;

  beginInsertColumns(parentIndex, node->row, node->row);
  parentNode->children.push_back(node); // This makes it apart of the model
  endInsertRows();

  return createIndex(node->row, 0, node);
}
//---------------------------------------------------------------------------


orm::v1::StorableConfigI*
AppMetaConfigObjectsModel::StorableConfig(const QModelIndex& index)
{
  const AppMetaConfigObjectsModelPrivate::Node& node = Node(index);
  switch (node.objectType)
  {
    case ObjectTypeNode:
    {
      SmObjectType::PtrT
          object = *boost::get< SmObjectType::PtrT >(node.variant);
      assert(object.get() != NULL);
      return object->config();
      break;
    }
    case NoObject:
      assert(false);
      break;
  }

  return NULL;
}
//---------------------------------------------------------------------------

SmObjectType::PtrT
AppMetaConfigObjectsModel::ObjectType(const QModelIndex& index)
{
  const AppMetaConfigObjectsModelPrivate::Node& node = Node(index);
  if (node.objectType != ObjectTypeNode)
    return SmObjectType::PtrT();

  SmObjectType::PtrT* object = boost::get< SmObjectType::PtrT >(node.variant);
  assert(object != NULL && object->get() != NULL);
  return *object;
}
//---------------------------------------------------------------------------

QModelIndex
AppMetaConfigObjectsModel::AddObjectType(SmObjectType::PtrT objectType)
{
  // Create the object and add it to the model
  SmObjectType::PtrT object(new SmObjectType());
  object->setId(mSmModel->NextMetaRecordId());
  object->setName("NewObjectType");
  object->setInherits(objectType);
  mSmModel->addObjectType(object);

  QModelIndex index;
  AppMetaConfigObjectsModelPrivate::Node* node = NULL;
  objectTypeDetails(index, node, object); // Touch the node to create it

  return index;
}
//---------------------------------------------------------------------------


AppMetaConfigObjectsModelPrivate::Node*
AppMetaConfigObjectsModel::NodeFromMap(AppMetaConfigObjectsModelPrivate::SmVariantT key)
{
  return &mNodes[key];
}
//---------------------------------------------------------------------------

AppMetaConfigObjectsModelPrivate::SmVariantT*
AppMetaConfigObjectsModel::KeyFromMap(AppMetaConfigObjectsModelPrivate::SmVariantT key)
{
  NodeMapT::iterator mapItr = mNodes.find(key);
  return const_cast< SmVariantT* > (&mapItr->first);
}
//---------------------------------------------------------------------------
