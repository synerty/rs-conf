#ifndef OBJECTTYPECONFIG_H
#define OBJECTTYPECONFIG_H

#include <boost/shared_ptr.hpp>

#include <QtCore/qobject.h>
#include "ui_AppMetaConfig.h"

class SmModel;
class AppMetaConfigObjectsModel;
class PropertiesModel;
class AppMetaConfigColumnsModel;

class AppMetaConfig : public QObject
{
  Q_OBJECT

  public:
    AppMetaConfig(QWidget *parent = 0);
    virtual
    ~AppMetaConfig();

    void Reset();

  public slots:
    void
    DoLoad();

    void
    DoSave();

    void
    DoAddObjectType();

    void
    DoAddSubObjectType();

    void
    DoItemSelected(const QModelIndex & current, const QModelIndex & previous);

  private:
    void
    SelectIndex(QModelIndex index);

    Ui::AppMetaConfigClass ui;

    boost::shared_ptr< SmModel > mSmModel;

    boost::shared_ptr< AppMetaConfigObjectsModel > mQtObjectsModel;
    boost::shared_ptr< PropertiesModel > mQtPropertyModel;
    boost::shared_ptr< AppMetaConfigColumnsModel> mQtColumnModel;

};

#endif // OBJECTTYPECONFIG_H
