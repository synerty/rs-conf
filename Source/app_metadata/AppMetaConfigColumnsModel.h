/*
 * uHierarchModel.h
 *
 *  Created on: Aug 25, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _APP_META_CONFIG_COLUMN_QT_MODEL_H_
#define _APP_META_CONFIG_COLUMN_QT_MODEL_H_

// Include Std headers
#include <string>
#include <map>
#include <vector>
#include <set>

#include <boost/shared_ptr.hpp>
#include <boost/variant.hpp>
// Include Qt headers
#include <QtCore/qabstractitemmodel.h>

class SmModel;
class SmColumn;
class SmTable;

namespace AppMetaConfigColumnsModelPrivate
{
  class Node;
}

class AppMetaConfigColumnsModel : public QAbstractItemModel
{
  public:
    enum TeCol
    {
      ColName,
      ColValueType,
      ColDataType,
      ColDataLength,
      ColCount

    };

  public:
    AppMetaConfigColumnsModel(boost::shared_ptr< SmModel > smModel,
        const boost::shared_ptr< SmTable >& smTable);
    virtual
    ~AppMetaConfigColumnsModel();

  public:
    // Abstract implementation
    QVariant
    headerData(int prmSection,
        Qt::Orientation prmOrientation,
        int prmRole = Qt::DisplayRole) const;

    QModelIndex
    index(int row, int column, const QModelIndex& parent = QModelIndex()) const;

    QModelIndex
    parent(const QModelIndex& index) const;

    int
    rowCount(const QModelIndex& parent = QModelIndex()) const;

    int
    columnCount(const QModelIndex& parent = QModelIndex()) const;

    QVariant
    data(const QModelIndex& index, int prmRole = Qt::DisplayRole) const;

    Qt::ItemFlags
    flags(const QModelIndex &index) const;

    bool
    setData(const QModelIndex &index,
        const QVariant &value,
        int role = Qt::EditRole);

  private:
    /// --------------
    // ObjectModel methods

    AppMetaConfigColumnsModelPrivate::Node&
    Node(const QModelIndex& index) const;

    void
    setupModel();

    typedef std::map< boost::shared_ptr< SmColumn >,
        AppMetaConfigColumnsModelPrivate::Node > NodeMapT;
    NodeMapT mNodes;

    /*! Link to the root node.
     * This node will not be in the model, its equivilent to QModelIndex()
     */
    AppMetaConfigColumnsModelPrivate::Node* mRootNode;

    boost::shared_ptr< SmModel > mSmModel;
    const boost::shared_ptr< SmTable > mSmTable;

};
// ----------------------------------------------------------------------------


#endif /* _APP_META_CONFIG_COLUMN_QT_MODEL_H_ */
