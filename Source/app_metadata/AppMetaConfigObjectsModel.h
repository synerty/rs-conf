/*
 * uHierarchModel.h
 *
 *  Created on: Aug 25, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _APP_META_CONFIG_OBJECTS_QT_MODEL_H_
#define _APP_META_CONFIG_OBJECTS_QT_MODEL_H_

// Include Std headers
#include <string>
#include <map>
#include <vector>
#include <set>

#include <boost/shared_ptr.hpp>
#include <boost/variant.hpp>
// Include Qt headers
#include <QtCore/qabstractitemmodel.h>

#include "SmObjectType.h"

class SmModel;
class SmObjectType;

namespace AppMetaConfigObjectsModelPrivate
{
  class Node;
  typedef boost::variant< //
      boost::shared_ptr< SmObjectType >
  > SmVariantT;

}

class AppMetaConfigObjectsModel : public QAbstractItemModel
{
  public:
    enum TeCol
    {
      ColName,
      ColDataTable,
      ColObjectTypeTable,
      ColCount

    };

  public:
    AppMetaConfigObjectsModel(boost::shared_ptr< SmModel > smModel);
    virtual
    ~AppMetaConfigObjectsModel();

  public:

    orm::v1::StorableConfigI*
    StorableConfig(const QModelIndex& index);

    SmObjectType::PtrT
    ObjectType(const QModelIndex& index);

    QModelIndex
    AddObjectType(SmObjectType::PtrT objectType = SmObjectType::PtrT());

  public:
    // Abstract implementation
    QVariant
    headerData(int prmSection,
        Qt::Orientation prmOrientation,
        int prmRole = Qt::DisplayRole) const;

    QModelIndex
    index(int row, int column, const QModelIndex& parent = QModelIndex()) const;
    QModelIndex
    parent(const QModelIndex& index) const;

    int
    rowCount(const QModelIndex& parent = QModelIndex()) const;

    int
    columnCount(const QModelIndex& parent = QModelIndex()) const;

    QVariant
    data(const QModelIndex& index, int prmRole = Qt::DisplayRole) const;

    Qt::ItemFlags
    flags(const QModelIndex &index) const;

    bool
    setData(const QModelIndex &index,
        const QVariant &value,
        int role = Qt::EditRole);

  private:
    /// --------------
    // ObjectModel methods

    QVariant
    data(boost::shared_ptr< SmObjectType >& object,
        const QModelIndex& index,
        int role) const;

    bool
    setData(boost::shared_ptr< SmObjectType >& object,
        const QModelIndex &index,
        const QVariant &value,
        int role);

    AppMetaConfigObjectsModelPrivate::Node&
    Node(const QModelIndex& index) const;

    void
    setupModel();

    void
    objectTypeDetails(QModelIndex& index,
        AppMetaConfigObjectsModelPrivate::Node*& node,
        const boost::shared_ptr< SmObjectType >& object);

    QModelIndex
    insertObjectType(const boost::shared_ptr< SmObjectType >& object);

    AppMetaConfigObjectsModelPrivate::Node*
    NodeFromMap(AppMetaConfigObjectsModelPrivate::SmVariantT key);

    AppMetaConfigObjectsModelPrivate::SmVariantT*
    KeyFromMap(AppMetaConfigObjectsModelPrivate::SmVariantT key);

    typedef std::map< AppMetaConfigObjectsModelPrivate::SmVariantT,
        AppMetaConfigObjectsModelPrivate::Node > NodeMapT;
    NodeMapT mNodes;

    /*! Link to the root node.
     * This node will not be in the model, its equivilent to QModelIndex()
     */
    AppMetaConfigObjectsModelPrivate::Node* mRootNode;

    boost::shared_ptr< SmModel > mSmModel;

};
// ----------------------------------------------------------------------------


#endif /* _APP_META_CONFIG_OBJECTS_QT_MODEL_H_ */
