/*
 * SmValues.cpp
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#include "SmValue.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "SmOrmModel.h"
#include "SmValueType.h"

ORM_V1_CALL_STORABLE_SETUP(SmValue)
ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(SmValue, int32_t, id)
template class orm::v1::StorageWrapper<SmModel, SmValue, int32_t>;
template class orm::v1::StorableWrapper<SmModel, SmValue>;
// ----------------------------------------------------------------------------
/// --------------
// Declare the properties for this storable

ORM_V1_DEFINE_SIMPLE_PROPERTY(SmValue, id, Id, int32_t)
ORM_V1_DEFINE_RELATED_PROPERTY(SmModel, SmValue, type, Type, int32_t, SmValueType)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmValue, name, Name, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmValue, toolTip, ToolTip, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmValue, help, Help, std::string)
// ----------------------------------------------------------------------------

SmValue::SmValue()
{
  mPropId.setup(this);
  mPropType.setup(this);
  mPropName.setup(this);
  mPropToolTip.setup(this);
  mPropHelp.setup(this);
}
// ----------------------------------------------------------------------------

SmValue::~SmValue()
{
}
// ----------------------------------------------------------------------------

bool
SmValue::setupStatic()
{
  using namespace orm::v1;
  typedef StorableConfig< SmValue > CfgT;

  CfgT::setSqlOpts()->setName("SmValue");
  CfgT::setSqlOpts()->setTableName("sams_meta.tbl_VALUE");

  CfgT::addFieldMeta(IdPropT::setupStatic("ID", true));
  CfgT::addFieldMeta(TypePropT::setupStatic("VALUE_TYPE",
      &SmValueType::id,
      &SmModel::ValueType,
      &SmValueType::Values,
      false,
      false));
  CfgT::addFieldMeta(NamePropT::setupStatic("NAME"));
  CfgT::addFieldMeta(ToolTipPropT::setupStatic("TOOLTIP"));
  CfgT::addFieldMeta(HelpPropT::setupStatic("HELP"));

  return true;
}
// ----------------------------------------------------------------------------
