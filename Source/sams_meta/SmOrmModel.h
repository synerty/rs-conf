/*
 * SmModel.h
 *
 *  Created on: Dec 18, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _SAMS_META_MODEL_H_
#define _SAMS_META_MODEL_H_

#include "orm/v1/Model.h"
#include "orm/v1/Sequance.h"

class SmSchema;
class SmTable;
class SmColumn;

class SmObjectType;
class SmObjectType2;

class SmValue;
class SmValueType;



/*! SAMS Metadata model
 * This class manges the model data.
 *
 */
class SmModel : public orm::v1::Model< SmModel >
{

  public:
    SmModel(const orm::v1::Conn& ormConn = orm::v1::Conn());
    virtual
    ~SmModel();

  public:

  ORM_V1_MODEL_DECLARE_STORABLE(SmSchema, Schema, int32_t)
  ORM_V1_MODEL_DECLARE_STORABLE(SmTable, Table, int32_t)
  ORM_V1_MODEL_DECLARE_STORABLE(SmColumn, Column, int32_t)

  ORM_V1_MODEL_DECLARE_STORABLE(SmObjectType, ObjectType, int32_t)
  ORM_V1_MODEL_DECLARE_STORABLE(SmObjectType2, ObjectType2, int32_t)

  ORM_V1_MODEL_DECLARE_STORABLE(SmValueType, ValueType, int32_t)
  ORM_V1_MODEL_DECLARE_STORABLE(SmValue, Value, int32_t)

  public:
    int32_t
    NextMetaRecordId();

  private:
    orm::v1::Sequance mMetaId;

};

#endif /* _SAMS_META_MODEL_H_ */
