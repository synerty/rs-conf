/*
 * SmModel.cpp
 *
 *  Created on: Dec 18, 2010
 *      Author: Jarrod Chesney
 */

#include "SmOrmModel.h"

#include "orm/v1/ModelWrapper.ini"

template class orm::v1::ModelWrapper< SmModel, SmSchema >;
template class orm::v1::ModelWrapper< SmModel, SmTable >;
template class orm::v1::ModelWrapper< SmModel, SmColumn >;
template class orm::v1::ModelWrapper< SmModel, SmObjectType >;
template class orm::v1::ModelWrapper< SmModel, SmObjectType2 >;
template class orm::v1::ModelWrapper< SmModel, SmValueType >;
template class orm::v1::ModelWrapper< SmModel, SmValue >;

SmModel::SmModel(const orm::v1::Conn& ormConn) :
  BaseModel(ormConn),//
      mMetaId("sams_meta.\"seq_ID\"", mDb)
{
  // These will get initialised in this order, so don't change the order.
  addStorageWrapper(mSchemaStorageWrapper);
  addStorageWrapper(mTableStorageWrapper);
  addStorageWrapper(mColumnStorageWrapper);
  addStorageWrapper(mObjectTypeStorageWrapper);
  addStorageWrapper(mObjectType2StorageWrapper);
  addStorageWrapper(mValueTypeStorageWrapper);
  addStorageWrapper(mValueStorageWrapper);
}
// ----------------------------------------------------------------------------

SmModel::~SmModel()
{
}
// ----------------------------------------------------------------------------

int32_t
SmModel::NextMetaRecordId()
{
  return mMetaId.next();
}
// ----------------------------------------------------------------------------

ORM_V1_MODEL_DEFINE_STORABLE(SmModel, SmSchema, Schema, int32_t)
ORM_V1_MODEL_DEFINE_STORABLE(SmModel, SmTable, Table, int32_t)
ORM_V1_MODEL_DEFINE_STORABLE(SmModel, SmColumn, Column, int32_t)

ORM_V1_MODEL_DEFINE_STORABLE(SmModel, SmObjectType, ObjectType, int32_t)
ORM_V1_MODEL_DEFINE_STORABLE(SmModel, SmObjectType2, ObjectType2, int32_t)

ORM_V1_MODEL_DEFINE_STORABLE(SmModel, SmValueType, ValueType, int32_t)
ORM_V1_MODEL_DEFINE_STORABLE(SmModel, SmValue, Value, int32_t)
