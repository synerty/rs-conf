/*
 * SmValueTypes.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _SM_VALUE_TYPE_H_
#define _SM_VALUE_TYPE_H_

#include <string>
#include <stdint.h>

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include "orm/v1/Storable.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"
#include "orm/v1/RelatedToManyProperty.h"

class SmTable;
class SmModel;
class SmValue;
class SmValueType;

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(SmValueType, int32_t)

class SmValueType : public orm::v1::Storable< SmModel, SmValueType >
{
  public:
  /// --------------
  // Types

  public:
  SmValueType();
  virtual
  ~SmValueType();

  public:
  /// --------------
  // Setup methods

  static bool
  setupStatic();

  private:
  /// --------------
  // Declare the properties for this storable

  ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, int32_t)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(name, Name, std::string)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(comment, Comment, std::string)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(generateEnum, GenerateEnum, bool)
  ORM_V1_DECLARE_RELATED_PROPERTY(table, Table, int32_t, SmTable)
  ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(values, Values, SmValue)

};
// ----------------------------------------------------------------------------

#endif /* _SM_VALUE_TYPE_H_ */
