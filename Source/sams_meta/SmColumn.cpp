/*
 * SmColumns.cpp
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#include "SmColumn.h"

#include <boost/format.hpp>

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "SmOrmModel.h"
#include "SmTable.h"
#include "SmValueType.h"

ORM_V1_CALL_STORABLE_SETUP(SmColumn)
ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(SmColumn, int32_t, id)
template class orm::v1::StorageWrapper<SmModel, SmColumn, int32_t>;
template class orm::v1::StorableWrapper<SmModel, SmColumn>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(SmColumn, id, Id, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmColumn, name, Name, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmColumn, niceName, NiceName, std::string)
ORM_V1_DEFINE_RELATED_PROPERTY(SmModel, SmColumn, table, Table, int32_t, SmTable)
ORM_V1_DEFINE_RELATED_PROPERTY(SmModel, SmColumn, valueType, ValueType, int32_t, SmValueType)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmColumn, dataType, DataType, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmColumn, dataLength, DataLength, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmColumn, isNullable, Nullable, bool)
ORM_V1_DEFINE_RELATED_PROPERTY(SmModel, SmColumn, relatesTo, RelatesTo, int32_t, SmColumn)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmColumn, defaultValue, DefaultValue, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmColumn, isUserModified, UserModified, bool)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmColumn, isGenerateStorableProperty, GenerateStorableProperty, bool)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmColumn, isPrimaryKey, PrimaryKey, bool)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(SmColumn, relatedFrom, RelatedFrom, SmColumn)
// ----------------------------------------------------------------------------

SmColumn::SmColumn()
{
  mPropId.setup(this);
  mPropName.setup(this);
  mPropNiceName.setup(this);
  mPropTable.setup(this);
  mPropValueType.setup(this);
  mPropDataType.setup(this);
  mPropDataLength.setup(this);
  mPropNullable.setup(this);
  mPropRelatesTo.setup(this);
  mPropDefaultValue.setup(this);
  mPropUserModified.setup(this);
  mPropGenerateStorableProperty.setup(this);
  mPropPrimaryKey.setup(this);

  // Relates to many properties
  mPropRelatedFrom.setup(this);
}
// ----------------------------------------------------------------------------

SmColumn::~SmColumn()
{
}
// ----------------------------------------------------------------------------

bool
SmColumn::setupStatic()
{
  using namespace orm::v1;
  typedef StorableConfig< SmColumn > CfgT;
  CfgT::setSqlOpts()->setName("SmColumn");
  CfgT::setSqlOpts()->setTableName("sams_meta.tbl_COLUMN");

  CfgT::addFieldMeta(IdPropT::setupStatic("ID", true));
  CfgT::addFieldMeta(NamePropT::setupStatic("NAME"));
  CfgT::addFieldMeta(NiceNamePropT::setupStatic("NICE_NAME"));
  CfgT::addFieldMeta(TablePropT::setupStatic("TABLE",
      &SmTable::id,
      &SmModel::Table,
      &SmTable::Columns,
      false,
      false));
  CfgT::addFieldMeta(ValueTypePropT::setupStatic("VALUE_TYPE",
      &SmValueType::id,
      &SmModel::ValueType));
  CfgT::addFieldMeta(DataTypePropT::setupStatic("DATA_TYPE"));
  CfgT::addFieldMeta(DataLengthPropT::setupStatic("DATA_LENGTH"));
  CfgT::addFieldMeta(NullablePropT::setupStatic("IS_NULLABLE"));
  CfgT::addFieldMeta(RelatesToPropT::setupStatic("RELATES_TO",
      &SmColumn::id,
      &SmModel::Column,
      &SmColumn::RelatedFrom));
  CfgT::addFieldMeta(DefaultValuePropT::setupStatic("DEFAULT_VALUE"));
  CfgT::addFieldMeta(UserModifiedPropT::setupStatic("MANUALLY_MODIFIED",
      false,
      false,
      false));
  CfgT::addFieldMeta(GenerateStorablePropertyPropT::setupStatic("GENERATE_STORABLE_PROPERTY",
      false,
      false,
      true));
  CfgT::addFieldMeta(PrimaryKeyPropT::setupStatic("PRIMARY_KEY",
      false,
      false,
      false));
  return true;
}
// ----------------------------------------------------------------------------

std::string
SmColumn::fullName() const
{
  assert(table().get());
  return table()->fullName() + "." + name();
}
// ----------------------------------------------------------------------------

std::string
SmColumn::displayName() const
{
  boost::format fmt("%s (%s,%d%s)%s");
  fmt % name() % dataType() % dataLength();
  fmt % (isNullable() ? ",Nullable" : "");
  fmt % (isDefaultValueNull() ? "" : ("=" + defaultValue()));
  return fmt.str();
}
// ----------------------------------------------------------------------------

std::string
SmColumn::comapareString() const
{
  boost::format fmt("%s (%s,%d%s)%s%s%s");
  fmt % name() % dataType() % dataLength();
  fmt % (isNullable() ? ",Nullable" : "");
  fmt % (isUserModified() || relatesTo().get() == NULL ? "" : ("->"
      + relatesTo()->fullName()));
  fmt % (isDefaultValueNull() ? "" : ("=" + defaultValue()));
  fmt % (!isUserModified() && isPrimaryKey() ? ",PK" : "");
  return fmt.str();
}
// ----------------------------------------------------------------------------

std::string
SmColumn::userModifiedComapareString() const
{
  boost::format fmt("%s (%s,%d)%s");
  fmt % name() % dataType() % dataLength();
  fmt % (isDefaultValueNull() ? "" : ("=" + defaultValue()));
  return fmt.str();
}
// ----------------------------------------------------------------------------

std::string
SmColumn::cppDataType() const
{
  if (dataType() == "int2")
    return "int16_t";

  if (dataType() == "int4")
    return "int32_t";

  if (dataType() == "int8")
    return "int64_t";

  if (dataType() == "float4")
    return "float";

  if (dataType() == "float8")
    return "double";

  if (dataType() == "string")
    return "std::string";

  if (dataType() == "bool")
    return "bool";

  assert(false);

  return "ERROR";
}
// ----------------------------------------------------------------------------

std::string
SmColumn::cppDefaultValue() const
{
  if (dataType() == "int2")
    return defaultValue();

  if (dataType() == "int4")
    return defaultValue();

  if (dataType() == "int8")
    return defaultValue();

  if (dataType() == "float4")
    return defaultValue();

  if (dataType() == "float8")
    return defaultValue();

  if (dataType() == "string")
    return "\"" + defaultValue() + "\"";

  if (dataType() == "bool")
    return defaultValue();

  assert(false);

  return "ERROR";
}
// ----------------------------------------------------------------------------

void
SmColumn::unExport()
{
  ListT list;
  relatedFrom(list);
  for (ListT::iterator itr = list.begin(); //
  itr != list.end(); ++itr)
    (*itr)->unExport();

  setGenerateStorableProperty(false);

}
// ----------------------------------------------------------------------------
