/*
 * SmTables.cpp
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#include "SmTable.h"

#include <boost/format.hpp>

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "SmOrmModel.h"

#include "SmSchema.h"
#include "SmColumn.h"

ORM_V1_CALL_STORABLE_SETUP(SmTable)
ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(SmTable, int32_t, id)
template class orm::v1::StorageWrapper<SmModel, SmTable, int32_t>;
template class orm::v1::StorableWrapper<SmModel, SmTable>;
// ----------------------------------------------------------------------------
/// --------------
// Declare the properties for this storable

ORM_V1_DEFINE_SIMPLE_PROPERTY(SmTable, id, Id, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmTable, name, Name, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmTable, niceName, NiceName, std::string)
ORM_V1_DEFINE_RELATED_PROPERTY(SmModel, SmTable, inherits, Inherits, int32_t, SmTable)
ORM_V1_DEFINE_MAPPED_PROPERTY(SmTable, inheritType, InheritType,std::string, orm::v1::SqlOpts::InheritanceTypeE)
ORM_V1_DEFINE_RELATED_PROPERTY(SmModel, SmTable, schema, Schema, int32_t, SmSchema)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmTable, generateStorable, GenerateStorable, bool)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmTable, isInheritsUserDefined, InheritsUserDefined, bool)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(SmTable, inheritedBy, InheritedBy, SmTable)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(SmTable, columns, Columns, SmColumn)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(SmTable, objectTypes, ObjectTypes, SmObjectType)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(SmTable, valueTypes, ValueTypes, SmValueType)
// ----------------------------------------------------------------------------

SmTable::SmTable()
{
  mPropId.setup(this);
  mPropName.setup(this);
  mPropNiceName.setup(this);
  mPropInherits.setup(this);
  mPropInheritType.setup(this);
  mPropSchema.setup(this);
  mPropGenerateStorable.setup(this);
  mPropInheritsUserDefined.setup(this);

  // Relates to many properties
  mPropInheritedBy.setup(this);
  mPropColumns.setup(this);
  mPropObjectTypes.setup(this);
  mPropValueTypes.setup(this);
}
// ----------------------------------------------------------------------------

SmTable::~SmTable()
{
}
// ----------------------------------------------------------------------------

bool
SmTable::setupStatic()
{
  using namespace orm::v1;
  typedef StorableConfig< SmTable > CfgT;

  CfgT::setSqlOpts()->setName("SmTable");
  CfgT::setSqlOpts()->setTableName("sams_meta.tbl_TABLE");

  CfgT::addFieldMeta(IdPropT::setupStatic("ID", true));
  CfgT::addFieldMeta(NamePropT::setupStatic("NAME"));
  CfgT::addFieldMeta(NiceNamePropT::setupStatic("NICE_NAME"));
  CfgT::addFieldMeta(InheritsPropT::setupStatic("INHERITS",
      &SmTable::id,
      &SmModel::Table,
      &SmTable::InheritedBy));

  InheritTypePropMapT inheritMap;
  inheritMap.insert(std::make_pair("", orm::v1::SqlOpts::NoInheritance));
  inheritMap.insert(std::make_pair("JOINED",
      orm::v1::SqlOpts::JoinedInheritance));
  inheritMap.insert(std::make_pair("CONCRETE",
      orm::v1::SqlOpts::ConcreteInheritance));

  CfgT::addFieldMeta(InheritTypePropT::setupStatic("INHERIT_TYPE", inheritMap));

  CfgT::addFieldMeta(SchemaPropT::setupStatic("SCHEMA",
      &SmSchema::id,
      &SmModel::Schema,
      &SmSchema::Tables,
      false,
      false));

  CfgT::addFieldMeta(GenerateStorablePropT::setupStatic("GENERATE_STORABLE",
      false,
      false,
      true));

  CfgT::addFieldMeta(InheritsUserDefinedPropT::setupStatic("INHERITS_IS_USER_DEFINED",
      false,
      false,
      false));

  return true;
}
// ----------------------------------------------------------------------------

std::string
SmTable::fullName() const
{
  assert(schema().get() != NULL);
  return schema()->name() + "." + name();
}
// ----------------------------------------------------------------------------

std::string
SmTable::displayName() const
{
  std::string name_ = name();

  if (inherits().get() != NULL)
  {
    boost::format inherit("(%s:%s)");
    inherit % inherits()->fullName();
    inherit % inheritTypeFromMap();
    name_ += inherit.str();
  }

  return name_;
}
// ----------------------------------------------------------------------------

std::string
SmTable::compareName() const
{
  std::string name_ = name();

  if (!isInheritsUserDefined() && inherits().get() != NULL)
  {
    boost::format inherit("(%s:%s)");
    inherit % inherits()->fullName();
    inherit % inheritTypeFromMap();
    name_ += inherit.str();
  }

  return name_;
}
// ----------------------------------------------------------------------------

std::string
SmTable::primaryKeyFieldName() const
{
  ColumnsListT cols;
  columns(cols);
  for (ColumnsListT::iterator itr = cols.begin();//
  itr != cols.end(); ++itr)
  {
    SmColumn::PtrT& column = *itr;
    if (column->isPrimaryKey())
      return column->name();
  }

  throw NoPrimaryKeyException();
}
// ----------------------------------------------------------------------------

std::string
SmTable::cppInheritType() const
{
  if (inheritType() == orm::v1::SqlOpts::JoinedInheritance)
    return "orm::v1::SqlOpts::JoinedInheritance";

  if (inheritType() == orm::v1::SqlOpts::ConcreteInheritance)
    return "orm::v1::SqlOpts::ConcreteInheritance";

  boost::format info("Unknown inheritance type %s in SmTable::cppInheritType");
  info % inheritType();
  throw std::runtime_error(info.str());
}
// ----------------------------------------------------------------------------

void
SmTable::unExport()
{
  ListT list;
  inheritedBy(list);
  for (ListT::iterator itr = list.begin(); //
  itr != list.end(); ++itr)
    (*itr)->unExport();

  setGenerateStorable(false);

}
// ----------------------------------------------------------------------------


