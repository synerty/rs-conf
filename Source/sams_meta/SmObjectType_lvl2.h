/*
 * SmObjectType2s.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _SM_OBJECT_TYPE_2_H_
#define _SM_OBJECT_TYPE_2_H_

#include <string>
#include <stdint.h>

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

/*! simSCADA Sim Objects
 * Storage class to load simSCADA objects
 *
 */
#include "orm/v1/Storable.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"

#include "SmObjectType.h"
#include "SmTable.h"

class SmModel;
class SmObjectType2;

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(SmObjectType2, int32_t)

class SmObjectType2 : public orm::v1::Storable< SmModel, SmObjectType2, SmObjectType >
{
  public:
    /// --------------
    // Types


  public:
    SmObjectType2();
    virtual
    ~SmObjectType2();

  public:
    /// --------------
    // Setup methods

    static bool
    setupStatic();

  private:



  ORM_V1_DECLARE_SIMPLE_PROPERTY_INHERITED(id, Id, int32_t)
  ORM_V1_DECLARE_SIMPLE_PROPERTY_INHERITED(name, Name, std::string)
  ORM_V1_DECLARE_RELATED_PROPERTY(table, Table, int32_t, SmTable)

};
// ----------------------------------------------------------------------------

#endif /* _SM_OBJECT_TYPE_2_H_ */
