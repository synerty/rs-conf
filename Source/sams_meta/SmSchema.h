/*
 * SmSchemas.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _SM_SCHEMA_H_
#define _SM_SCHEMA_H_

#include <string>
#include <stdint.h>

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

/*! simSCADA Sim Objects
 * Storage class to load simSCADA objects
 *
 */
#include "orm/v1/Storable.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedToManyProperty.h"

class SmModel;
class SmTable;
class SmSchema;

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(SmSchema, int32_t)

class SmSchema : public orm::v1::Storable< SmModel, SmSchema >
{
  public:
  SmSchema();
  virtual
  ~SmSchema();

  public:
  /// --------------
  // Setup methods

  static bool
  setupStatic();

  private:
  /// --------------
  // Declare the properties for this storable

  ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, int32_t)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(name, Name, std::string)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(niceName, NiceName, std::string)
  ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(tables, Tables, SmTable)

};
// ----------------------------------------------------------------------------

#endif /* _SM_SCHEMA_H_ */
