/*
 * SmColumns.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _SM_COLUMN_H_
#define _SM_COLUMN_H_

#include <string>
#include <stdint.h>

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include "orm/v1/Storable.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"
#include "orm/v1/RelatedToManyProperty.h"

class SmModel;
class SmColumn;
class SmTable;
class SmValueType;

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(SmColumn, int32_t)

class SmColumn : public orm::v1::Storable< SmModel, SmColumn >
{
  public:
  SmColumn();
  virtual
  ~SmColumn();

  public:
  /// --------------
  // Setup methods

  static bool
  setupStatic();

  public:

  ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, int32_t)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(name, Name, std::string)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(niceName, NiceName, std::string)
  ORM_V1_DECLARE_RELATED_PROPERTY(table, Table, int32_t, SmTable)
  ORM_V1_DECLARE_RELATED_PROPERTY(valueType, ValueType, int32_t, SmValueType)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(dataType, DataType, std::string)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(dataLength, DataLength, int32_t)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(isNullable, Nullable, bool)
  ORM_V1_DECLARE_RELATED_PROPERTY(relatesTo, RelatesTo, int32_t, SmColumn)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(defaultValue, DefaultValue, std::string)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(isUserModified, UserModified, bool)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(isGenerateStorableProperty, GenerateStorableProperty, bool)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(isPrimaryKey, PrimaryKey, bool)
  ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(relatedFrom, RelatedFrom, SmColumn)

  public:
  std::string
  fullName() const;

  std::string
  displayName() const;

  std::string
  comapareString() const;

  std::string
  userModifiedComapareString() const;

  std::string
  cppDataType() const;

  std::string
  cppDefaultValue() const;

  void
  unExport();

};
// ----------------------------------------------------------------------------

#endif /* _SM_COLUMN_H_ */
