/*
 * SmSchemas.cpp
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#include "SmSchema.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

ORM_V1_CALL_STORABLE_SETUP(SmSchema)
ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(SmSchema, int32_t, id)
template class orm::v1::StorageWrapper<SmModel, SmSchema, int32_t>;
template class orm::v1::StorableWrapper<SmModel, SmSchema>;
// ----------------------------------------------------------------------------
/// --------------
// Declare the properties for this storable

ORM_V1_DEFINE_SIMPLE_PROPERTY(SmSchema, id, Id, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmSchema, name, Name, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmSchema, niceName, NiceName, std::string)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(SmSchema, tables, Tables, SmTable)
// ----------------------------------------------------------------------------

SmSchema::SmSchema()
{
  mPropId.setup(this);
  mPropName.setup(this);
  mPropNiceName.setup(this);

  // Relates to many properties
  mPropTables.setup(this);
}
// ----------------------------------------------------------------------------

SmSchema::~SmSchema()
{
}
// ----------------------------------------------------------------------------

bool
SmSchema::setupStatic()
{
  using namespace orm::v1;
  typedef StorableConfig< SmSchema > CfgT;
  CfgT::setSqlOpts()->setName("SmSchema");
  CfgT::setSqlOpts()->setTableName("sams_meta.tbl_SCHEMA");

  CfgT::addFieldMeta(IdPropT::setupStatic("ID", true));
  CfgT::addFieldMeta(NamePropT::setupStatic("NAME"));
  CfgT::addFieldMeta(NiceNamePropT::setupStatic("NICE_NAME"));
  return true;
}
// ----------------------------------------------------------------------------

