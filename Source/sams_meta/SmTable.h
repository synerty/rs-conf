/*
 * SmTables.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _SM_TABLE_H_
#define _SM_TABLE_H_

#include <string>
#include <stdint.h>
#include <stdexcept>

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

/*! simSCADA Sim Objects
 * Storage class to load simSCADA objects
 *
 */
#include "orm/v1/Storable.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/MappedProperty.h"
#include "orm/v1/RelatedProperty.h"
#include "orm/v1/RelatedToManyProperty.h"
#include "orm/v1/SqlOpts.h"

struct NoPrimaryKeyException : std::runtime_error
{
    NoPrimaryKeyException() :
      std::runtime_error("Could not find column with primary key")
    {
    }
};
//#include "SmSchema.h"

class SmModel;
class SmTable;
class SmSchema;
class SmColumn;
class SmObjectType;
class SmValueType;

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(SmTable, int32_t)

class SmTable : public orm::v1::Storable< SmModel, SmTable >
{
  public:
  SmTable();
  virtual
  ~SmTable();

  public:
  /// --------------
  // Setup methods

  static bool
  setupStatic();

  public:
  std::string
  fullName() const;

  std::string
  displayName() const;

  std::string
  compareName() const;

  std::string
  primaryKeyFieldName() const;

  std::string
  cppInheritType() const;

  void
  unExport();

  private:
  /// --------------
  // Declare the properties for this storable

  ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, int32_t)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(name, Name, std::string)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(niceName, NiceName, std::string)
  ORM_V1_DECLARE_RELATED_PROPERTY(inherits, Inherits, int32_t, SmTable)
  ORM_V1_DECLARE_MAPPED_PROPERTY(inheritType, InheritType,std::string, orm::v1::SqlOpts::InheritanceTypeE)
  ORM_V1_DECLARE_RELATED_PROPERTY(schema, Schema, int32_t, SmSchema)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(generateStorable, GenerateStorable, bool)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(isInheritsUserDefined, InheritsUserDefined, bool)
  ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(inheritedBy, InheritedBy, SmTable)
  ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(columns, Columns, SmColumn)
  ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(objectTypes, ObjectTypes, SmObjectType)
  ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(valueTypes, ValueTypes, SmValueType)

};
// ----------------------------------------------------------------------------

#endif /* _SM_TABLE_H_ */
