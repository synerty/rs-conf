/*
 * SmValues.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _SM_VALUE_H_
#define _SM_VALUE_H_

#include <string>
#include <stdint.h>

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include "orm/v1/Storable.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"

class SmModel;
class SmValueType;
class SmValue;

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(SmValue, int32_t)

class SmValue : public orm::v1::Storable< SmModel, SmValue >
{
  public:
    /// --------------
    // Types

  public:
    SmValue();
    virtual
    ~SmValue();

  public:
    /// --------------
    // Setup methods

    static bool
    setupStatic();

  private:
    /// --------------
    // Declare the properties for this storable

  ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, int32_t)
  ORM_V1_DECLARE_RELATED_PROPERTY(type, Type, int32_t, SmValueType)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(name, Name, std::string)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(toolTip, ToolTip, std::string)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(help, Help, std::string)

};
// ----------------------------------------------------------------------------

#endif /* _SM_VALUE_H_ */
