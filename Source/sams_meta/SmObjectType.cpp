/*
 * SmObjectTypes.cpp
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#include "SmObjectType.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "SmOrmModel.h"
#include "SmTable.h"

ORM_V1_CALL_STORABLE_SETUP( SmObjectType)
ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(SmObjectType, int32_t, id)
template class orm::v1::StorageWrapper<SmModel, SmObjectType, int32_t>;
template class orm::v1::StorableWrapper<SmModel, SmObjectType>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(SmObjectType, id, Id, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmObjectType, name, Name, std::string)
ORM_V1_DEFINE_RELATED_PROPERTY(SmModel, SmObjectType, inherits, Inherits, int32_t, SmObjectType)
ORM_V1_DEFINE_RELATED_PROPERTY(SmModel, SmObjectType, dataTable, DataTable, int32_t, SmTable)
ORM_V1_DEFINE_RELATED_PROPERTY(SmModel, SmObjectType, objectTypeTable, ObjectTypeTable, int32_t, SmTable)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(SmObjectType, inheritedBy, InheritedBy, SmObjectType)
// ----------------------------------------------------------------------------

SmObjectType::SmObjectType()
{
  mPropId.setup(this);
  mPropName.setup(this);
  mPropInherits.setup(this);
  mPropDataTable.setup(this);
  mPropObjectTypeTable.setup(this);

  // Relates to many properties
  mPropInheritedBy.setup(this);
}
// ----------------------------------------------------------------------------

SmObjectType::~SmObjectType()
{
}
// ----------------------------------------------------------------------------

bool
SmObjectType::setupStatic()
{
  using namespace orm::v1;
  typedef StorableConfig< SmObjectType > CfgT;
  CfgT::setSqlOpts()->setName("SmObjectType");
  CfgT::setSqlOpts()->setTableName("sams_meta.tbl_OBJECT_TYPE");

  CfgT::addFieldMeta(IdPropT::setupStatic("ID", true));
  CfgT::addFieldMeta(NamePropT::setupStatic("NAME"));
  CfgT::addFieldMeta(InheritsPropT::setupStatic("INHERITS",
      &SmObjectType::id,
      &SmModel::ObjectType,
      &SmObjectType::InheritedBy));
  CfgT::addFieldMeta(DataTablePropT::setupStatic("DATA_TABLE",
      &SmTable::id,
      &SmModel::Table,
      &SmTable::ObjectTypes));
  CfgT::addFieldMeta(ObjectTypeTablePropT::setupStatic("OBJECT_TYPE_TABLE",
      &SmTable::id,
      &SmModel::Table,
      &SmTable::ObjectTypes));
  return true;
}
// ----------------------------------------------------------------------------
