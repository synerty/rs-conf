/*
 * SmObjectTypes.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _SM_OBJECT_TYPE_H_
#define _SM_OBJECT_TYPE_H_

#include <string>
#include <stdint.h>

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

/*! simSCADA Sim Objects
 * Storage class to load simSCADA objects
 *
 */
#include "orm/v1/Storable.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"
#include "orm/v1/RelatedToManyProperty.h"

class SmTable;
class SmModel;
class SmObjectType;

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(SmObjectType, int32_t)

class SmObjectType : public orm::v1::Storable< SmModel, SmObjectType >
{
  public:
    /// --------------
    // Types


  public:
    SmObjectType();
    virtual
    ~SmObjectType();

  public:
    /// --------------
    // Setup methods

    static bool
    setupStatic();

  private:

  ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, int32_t)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(name, Name, std::string)
  ORM_V1_DECLARE_RELATED_PROPERTY(inherits, Inherits, int32_t, SmObjectType)
  ORM_V1_DECLARE_RELATED_PROPERTY(dataTable, DataTable, int32_t, SmTable)
  ORM_V1_DECLARE_RELATED_PROPERTY(objectTypeTable, ObjectTypeTable, int32_t, SmTable)
  ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(inheritedBy, InheritedBy, SmObjectType)

};
// ----------------------------------------------------------------------------

#endif /* _SM_OBJECT_TYPE_H_ */
