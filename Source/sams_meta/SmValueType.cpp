/*
 * SmValueTypes.cpp
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#include "SmValueType.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "SmTable.h"
#include "SmOrmModel.h"

ORM_V1_CALL_STORABLE_SETUP( SmValueType)
ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(SmValueType, int32_t, id)
template class orm::v1::StorageWrapper<SmModel, SmValueType, int32_t>;
template class orm::v1::StorableWrapper<SmModel, SmValueType>;
// ----------------------------------------------------------------------------
/// --------------
// Declare the properties for this storable

ORM_V1_DEFINE_SIMPLE_PROPERTY(SmValueType, id, Id, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmValueType, name, Name, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmValueType, comment, Comment, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(SmValueType, generateEnum, GenerateEnum, bool)
ORM_V1_DEFINE_RELATED_PROPERTY(SmModel, SmValueType, table, Table, int32_t, SmTable)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(SmValueType, values, Values, SmValue)
// ----------------------------------------------------------------------------

SmValueType::SmValueType()
{
  mPropId.setup(this);
  mPropName.setup(this);
  mPropComment.setup(this);
  mPropGenerateEnum.setup(this);
  mPropTable.setup(this);

  // Relates to many properties
  mPropValues.setup(this);
}
// ----------------------------------------------------------------------------

SmValueType::~SmValueType()
{
}
// ----------------------------------------------------------------------------

bool
SmValueType::setupStatic()
{
  using namespace orm::v1;
  typedef StorableConfig< SmValueType > CfgT;

  CfgT::setSqlOpts()->setName("SmValueType");
  CfgT::setSqlOpts()->setTableName("sams_meta.tbl_VALUE_TYPE");

  CfgT::addFieldMeta(IdPropT::setupStatic("ID", true));
  CfgT::addFieldMeta(NamePropT::setupStatic("NAME", false, false));
  CfgT::addFieldMeta(CommentPropT::setupStatic("COMMENT", false, true));
  CfgT::addFieldMeta(GenerateEnumPropT::setupStatic("GENERATE_ENUM", false, false, true));
  CfgT::addFieldMeta(TablePropT::setupStatic("TABLE",
      &SmTable::id,
      &SmModel::Table,
      &SmTable::ValueTypes, false, true));
  return true;
}
// ----------------------------------------------------------------------------
