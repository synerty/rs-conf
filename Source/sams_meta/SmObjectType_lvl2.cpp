/*
 * SmObjectType2s.cpp
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#include "SmObjectType_lvl2.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "SmOrmModel.h"

ORM_V1_CALL_STORABLE_SETUP( SmObjectType2)
ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(SmObjectType2, int32_t, id)
template class orm::v1::StorageWrapper<SmModel, SmObjectType2, int32_t>;
template class orm::v1::StorableWrapper<SmModel, SmObjectType2>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY_INHERITED(SmObjectType2, id, Id, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY_INHERITED(SmObjectType2, name, Name, std::string)
ORM_V1_DEFINE_RELATED_PROPERTY(SmModel, SmObjectType2, table, Table, int32_t, SmTable)
// ----------------------------------------------------------------------------

SmObjectType2::SmObjectType2()
{
  mPropId.setup(this);
  mPropName.setup(this);
  mPropTable.setup(this);
}
// ----------------------------------------------------------------------------

SmObjectType2::~SmObjectType2()
{
}
// ----------------------------------------------------------------------------

bool
SmObjectType2::setupStatic()
{
  using namespace orm::v1;
  typedef StorableConfig< SmObjectType2 > CfgT;
  CfgT::setSqlOpts()->setName("SmObjectType2");
  CfgT::setSqlOpts()->setTableName("sams_meta.tbl_OBJECT_TYPE");

  typedef StorableConfig< BaseStorableT > InhertiedCfgT;
  CfgT::setSqlOpts()->addJoin(SqlJoin("ID", InhertiedCfgT::setSqlOpts(), "ID"));

  CfgT::setSqlOpts()->addCondition("false");

  CfgT::addFieldMeta(IdPropT::setupStatic("ID", true));
  CfgT::addFieldMeta(NamePropT::setupStatic("NAME"));
  CfgT::addFieldMeta(TablePropT::setupStatic("DATA_TABLE",
      &SmTable::id,
      &SmModel::Table));
  return true;
}
// ----------------------------------------------------------------------------
