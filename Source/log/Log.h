#ifndef LOG_H
#define LOG_H

#include <deque>
#include <string>
#include <auto_ptr.h>

#include <fstream>

#include <QtCore/qobject.h>
#include <QtCore/qglobal.h>
#include <QtCore/qmutex.h>

#include "ui_Log.h"

class LogModel;

class Log : public QObject
{
  Q_OBJECT

  public:
    struct Msg
    {
        std::string message;
        std::string type;
        std::string module;

        Msg(std::string type, std::string module, std::string message);
        Msg();

        bool
        operator==(const Msg& other) const;
    };

  public:
    Log(QWidget *parent = 0);

    virtual
    ~Log();

    static void
    msgHandler(QtMsgType prmMessageType, const char *prmFormat);

  public slots:
    void
    lineCountChanged(int newCount);

    void
    debugChanged(int newState);

  private:
    friend class LogModel;

    void
    logMessage(const Msg& msg);

    std::ofstream mLogFile;

    Ui::LogClass ui;

    static QMutex sLogMutex;
    static LogModel* sModel;
    static Log* sInst;
    static std::deque< Msg > sLogData;
    static int sLogSize;
    static bool sDebugEnabled;
};

#endif // LOG_H
