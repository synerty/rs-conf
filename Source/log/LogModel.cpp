/*
 * uHierarchModel.cpp
 *
 *  Created on: Aug 25, 2010
 *      Author: Jarrod Chesney
 */

#include "LogModel.h"

#include <assert.h>
#include <list>
#include <algorithm>

#include <QtCore/qmutex.h>
#include <QtGui/qcolor.h>
#include <QtGui/qbrush.h>

#include "Log.h"

//---------------------------------------------------------------------------
//                           LogModel private
//---------------------------------------------------------------------------
namespace LogModelPrivate
{

  struct Node
  {
      LogModelPrivate::Node* parent;
      int row;
      Log::Msg* data;
      typedef std::deque< LogModelPrivate::Node* > ChildrenT;
      ChildrenT children;

      Node() :
        parent(NULL), row(0), data(NULL)
      {
      }
  };

}

using namespace LogModelPrivate;

//---------------------------------------------------------------------------
//                       LogModel Implementation
//---------------------------------------------------------------------------

LogModel::LogModel(std::deque< Log::Msg >& data) :
  mRootNode(NULL), //
      mData(data)
{
  setupModel();
} //---------------------------------------------------------------------------

LogModel::~LogModel()
{
  QMutexLocker mtx(&Log::sLogMutex);

  Log::sModel = NULL;
}
//---------------------------------------------------------------------------

// Abstract implementation
QVariant
LogModel::headerData(int prmSection,
    Qt::Orientation prmOrientation,
    int prmRole) const
{
  if (prmOrientation == Qt::Vertical || prmRole != Qt::DisplayRole)
    return QVariant();

  switch (prmSection)
  {
    case ColType:
      return "Type";

    case ColModule:
      return "Module";

    case ColMessage:
      return "Message";

    default:
      assert(false);
  }

  return QVariant();
}
//---------------------------------------------------------------------------

QModelIndex
LogModel::index(int row, int column, const QModelIndex& parent) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      row,
  //      column,
  //      reinterpret_cast< uintptr_t > (parent.internalPointer()));

  if (row == -1 || column == -1)
    return QModelIndex();

  // Special Case  - If the parent is the root
  if (parent == QModelIndex())
    return createIndex(row, column, mRootNode->children.at(row));

  LogModelPrivate::Node& parentNode = Node(parent);
  return createIndex(row, column, parentNode.children.at(row));
}
//---------------------------------------------------------------------------

QModelIndex
LogModel::parent(const QModelIndex& index) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      index.row(),
  //      index.column(),
  //      reinterpret_cast< uintptr_t > (index.internalPointer()));

  assert(index.isValid());

  LogModelPrivate::Node& node = Node(index);

  // Every node in the model will have a parent
  if (node.parent->data == NULL)
    return QModelIndex();

  return createIndex(node.parent->row, 0, node.parent);
}
//---------------------------------------------------------------------------

int
LogModel::rowCount(const QModelIndex& parent) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      parent.row(),
  //      parent.column(),
  //      reinterpret_cast< uintptr_t > (parent.internalPointer()));

  if (mRootNode == NULL)
    return 0;

  // Special Case  - If the parent is the root
  if (parent == QModelIndex())
    return mRootNode->children.size();

  return Node(parent).children.size();
}
//---------------------------------------------------------------------------

int
LogModel::columnCount(const QModelIndex& parent) const
{
  return ColCount;
}
//---------------------------------------------------------------------------

QVariant
LogModel::data(const QModelIndex& index, int role) const
{
  // We only provide certain data.
  assert(index.isValid() && (0 <= index.column() && index.column() < ColCount));
  const LogModelPrivate::Node& node = Node(index);
  assert(node.data);

  if (role != Qt::DisplayRole && role != Qt::ToolTipRole)
    return QVariant();

  switch (index.column())
  {
    case ColType:
      return QString(node.data->type.c_str());

    case ColModule:
      return QString(node.data->module.c_str());

    case ColMessage:
      return QString(node.data->message.c_str());

  } // END switch (index.column())
  return QVariant();
}
//---------------------------------------------------------------------------

LogModelPrivate::Node&
LogModel::Node(const QModelIndex& index) const
{
  if (!index.isValid())
    assert(false);

  LogModelPrivate::Node
      * obj = static_cast< LogModelPrivate::Node* > (index.internalPointer());
  obj = dynamic_cast< LogModelPrivate::Node* > (obj);
  assert(obj);
  return *obj;
}
//---------------------------------------------------------------------------

void
LogModel::EraseNode(LogModelPrivate::Node* node)
{
  // Cleanup the node, we only have a valid reference to the node, not key
  for (NodeMapT::iterator itr = mNodes.begin();//
  itr != mNodes.end(); ++itr)
  {
    if (&(itr->second) == node)
    {
      mNodes.erase(itr);
      return;
    }
  }

  // Node not found
  assert(false);
}
//---------------------------------------------------------------------------

void
LogModel::linesRemoved()
{
  Log::Msg* newFront = &mData.front();

  int linesToRemove = 0;

  for (int i = 0; i < mRootNode->children.size(); i++)
  {
    if (mRootNode->children.at(i)->data == newFront)
      break;
    linesToRemove++;
  }

  if (linesToRemove == 0)
    return;

  beginRemoveRows(QModelIndex(), 0, linesToRemove - 1);

  for (int i = 0; i < linesToRemove; i++)
  {
    EraseNode(mRootNode->children.front());
    mRootNode->children.pop_front();
  }

  for (int i = 0; i < mRootNode->children.size(); i++)
    mRootNode->children.at(i)->row = i;

  endRemoveRows();
}
//---------------------------------------------------------------------------

void
LogModel::lineAdded()
{
  linesRemoved();

  int row = mRootNode->children.size();
  beginInsertRows(QModelIndex(), row, row);

  Log::Msg& msg = mData.back();

  // Get the parent (recursion is fun)
  LogModelPrivate::Node* node = &mNodes[&msg];

  // Set the parent
  mRootNode->children.push_back(node);
  node->parent = mRootNode;
  node->row = row;
  node->data = &msg;

  endInsertRows();
}
//---------------------------------------------------------------------------

void
LogModel::setupModel()
{
  mRootNode = &mNodes[NULL];

  for (DataT::iterator itr = mData.begin();//
  itr != mData.end(); ++itr)
  {
    Log::Msg& msg = *itr;

    if (msg == Log::Msg())
      continue;

    // Get the parent (recursion is fun)
    LogModelPrivate::Node* node = &mNodes[&msg];

    // Set the parent
    mRootNode->children.push_back(node);
    node->parent = mRootNode;
    node->row = mRootNode->children.size() - 1;
    node->data = &msg;

  }
}
//---------------------------------------------------------------------------
