/*
 * uHierarchModel.h
 *
 *  Created on: Aug 25, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _LOG_MODEL_H_
#define _LOG_MODEL_H_

// Include Std headers
#include <string>
#include <map>
#include <deque>

#include <boost/shared_ptr.hpp>

// Include Qt headers
#include <QtCore/qabstractitemmodel.h>

#include "Log.h"

namespace LogModelPrivate
{
  class Node;
}

class LogModel : public QAbstractItemModel
{
  public:
    enum TeCol
    {
      ColType,
      ColModule,
      ColMessage,
      ColCount

    };

  public:
    LogModel(std::deque< Log::Msg >& data);
    virtual
    ~LogModel();

    void
    linesRemoved();

    void
    lineAdded();

  public:
    // Abstract implementation
    QVariant
    headerData(int prmSection,
        Qt::Orientation prmOrientation,
        int prmRole = Qt::DisplayRole) const;

    QModelIndex
    index(int row, int column, const QModelIndex& parent = QModelIndex()) const;

    QModelIndex
    parent(const QModelIndex& index) const;

    int
    rowCount(const QModelIndex& parent = QModelIndex()) const;

    int
    columnCount(const QModelIndex& parent = QModelIndex()) const;

    QVariant
    data(const QModelIndex& index, int prmRole = Qt::DisplayRole) const;

  private:
    /// --------------
    // ObjectModel methods

    LogModelPrivate::Node&
    Node(const QModelIndex& index) const;

    void
    EraseNode(LogModelPrivate::Node* node);

    void
    setupModel();

    typedef std::map< Log::Msg*, LogModelPrivate::Node > NodeMapT;
    NodeMapT mNodes;

    /*! Link to the root node.
     * This node will not be in the model, its equivilent to QModelIndex()
     */
    LogModelPrivate::Node* mRootNode;

    typedef std::deque< Log::Msg > DataT;
    DataT& mData;

};
// ----------------------------------------------------------------------------


#endif /* _LOG_MODEL_H_ */
