#include "Log.h"

#include <iostream>

#include <boost/format.hpp>

#include "LogModel.h"

QMutex Log::sLogMutex;
LogModel* Log::sModel = NULL;
Log* Log::sInst = NULL;
std::deque< Log::Msg > Log::sLogData __attribute__((init_priority(64000)));
int Log::sLogSize = 100;
bool Log::sDebugEnabled = false;

Log::Msg::Msg()
{
}

Log::Msg::Msg(std::string type_, std::string module_, std::string message_) :
  message(message_), type(type_), module(module_)
{
}

bool
Log::Msg::operator==(const Msg& other) const
{
  return type == other.type //
      && module == other.module //
      && message == other.message;
}

Log::Log(QWidget *parent)
{
  assert(sInst == NULL);

  QMutexLocker mtx(&sLogMutex);

  ui.setupUi(parent);

  ui.txtLogSize->setValue(sLogSize);
  ui.chkDebugEnabled->setChecked(sDebugEnabled);

  connect(ui.txtLogSize,
      SIGNAL(valueChanged(int)),
      this,
      SLOT(lineCountChanged(int)));

  connect(ui.chkDebugEnabled,
      SIGNAL(stateChanged(int)),
      this,
      SLOT(debugChanged(int)));


  sModel = new LogModel(sLogData);
  ui.tableView->setModel(sModel);
  ui.tableView->resizeColumnToContents(2);

  mLogFile.open("debug.log");
  assert(mLogFile.is_open());

  sInst = this;
}

Log::~Log()
{
  QMutexLocker mtx(&sLogMutex);

  sInst = NULL;
  mLogFile.close();
}

void
Log::lineCountChanged(int newCount)
{
  QMutexLocker mtx(&sLogMutex);

  sLogSize = newCount;
  while (sLogData.size() > sLogSize)
    sLogData.pop_front();

  if (sModel != NULL)
    sModel->linesRemoved();
}

void
Log::debugChanged(int newState)
{
  QMutexLocker mtx(&sLogMutex);

  sDebugEnabled = (newState == Qt::Checked);
}

void
Log::logMessage(const Msg& msg)
{
  boost::format str("%s\t%s\t%s");
  str % msg.type % msg.module % msg.message;
  mLogFile << str.str() << std::endl;
}

void
Log::msgHandler(QtMsgType prmMessageType, const char *prmFormat)
{
  QMutexLocker mtx(&sLogMutex);

  std::string type;
  switch (prmMessageType)
  {
    case QtDebugMsg:
      type = "DEBUG";
      break;

    case QtWarningMsg:
      type = "WARNING";
      break;

    case QtCriticalMsg: // also QtSystemMsg
    case QtFatalMsg:
      type = "ERROR";
      break;

    default:
      type = "OTHER";
  }

  while (sLogData.size() > sLogSize)
    sLogData.pop_front();

  Msg msg(type, "", prmFormat);

  sLogData.push_back(msg);

  if (sModel != NULL && (prmMessageType != QtDebugMsg || sDebugEnabled))
    sModel->lineAdded();

  if (sInst != NULL)
    sInst->logMessage(msg);

}
