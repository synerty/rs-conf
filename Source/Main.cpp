/*
 * main.cpp
 *
 *  Created on: Feb 4, 2010
 *      Author: Jarrod Chesney
 */

#include "Main.h"

#include <iostream>

#include <boost/format.hpp>

#include <QtGui/qapplication.h>
#include <QtGui/qmessagebox.h>

// Load the Qt Postgres lib
//#include <QtPlugin>
//Q_IMPORT_PLUGIN(qsqlpsql)

#include "MainUi.h"
#include "Log.h"

// ----------------------------------------------------------------------------
//                         main - Application Entry
// ----------------------------------------------------------------------------

class RsApplication : public QApplication
{
  public:
    RsApplication(int &argc, char **argv) :
      QApplication(argc, argv)
    {
    }

    bool
    notify(QObject* obj, QEvent* evt)
    {
      try
      {
        return QApplication::notify(obj, evt);
      }
      catch (std::exception& e)
      {
        QString msg = "An application exception has occured :\n";
        msg += e.what();
        QMessageBox::critical(NULL, "Application Exception Occured", msg);
        return false;
      }

      return true;
    }
};

int
main(int argc, char *argv[])
{
  RsApplication a(argc, argv);

  QApplication::setApplicationName("Db App Meta Compiler");
  QApplication::setApplicationVersion("1.0.0");
  QApplication::setOrganizationName("Synerty Pty Ltd");
  QApplication::setOrganizationDomain("synerty.com");

  // Install the debug handler
  qInstallMsgHandler(Log::msgHandler);

  Main::Inst().Run();
  return a.exec();
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
//                                 Main class
// ----------------------------------------------------------------------------

// Initialise static variables
Main Main::sInst;

// ----------------------------------------------------------------------------

Main::Main()

{
}
// ----------------------------------------------------------------------------

Main&
Main::Inst()
{
  return sInst;
}
// ----------------------------------------------------------------------------

Main::~Main()
{
}
// ----------------------------------------------------------------------------

void
Main::Run()
{
  mGui.reset(new MainUi(*this));
  mGui->show();
}
// ----------------------------------------------------------------------------

void
Main::exitApplication()
{
  QApplication::exit(0);
}
// ----------------------------------------------------------------------------

// ---------------
// Glaboal interface

QWidget*
Main::MsgBoxParent()
{
  return mGui.get();
}
// ----------------------------------------------------------------------------

orm::v1::Conn
Main::databaseConnectionSettings()
{
  switch (mGui->selectedDatabase())
  {
    case samsDb:
    {
      orm::v1::Conn conn;
      conn.Type = orm::v1::Conn::PostGreSQL;
      conn.Database = "sams";
      conn.Host = "127.0.0.1";
      conn.Port = 5432;
      conn.Username = "postgres";
      conn.Password = "postgres";
      return conn;
    }
    case sysCfgDb:
    {
      orm::v1::Conn conn;
      conn.Type = orm::v1::Conn::PostGreSQL;
      conn.Database = "sams";
      conn.Host = "10.37.129.4";
      conn.Port = 5481;
      conn.Username = "postgres";
      conn.Password = "postgres";
      return conn;
    }
    case enmacDb:
    {
      orm::v1::Conn conn;
      conn.Type = orm::v1::Conn::Oracle;
      conn.Database = "NMS";
      conn.Host = "10.2.56.180";
      conn.Port = 1521;
      conn.Username = "enmac";
      conn.Password = "password";
      return conn;
    }
    case simScadaDb:
    {
      orm::v1::Conn conn;
      conn.Type = orm::v1::Conn::SQLite;
      //conn.Database = "Z:\\synerty\\projects\\wedn\\simscada\\wedn.sqlite";
      conn.Database = "C:\\synerty\\sams\\exp_simscada\\simscada_waikato.sqlite";
      return conn;
    }
    case ttaSimScadaDb:
    {
      orm::v1::Conn conn;
      conn.Type = orm::v1::Conn::SQLite;
      //conn.Database = "G:\\db\\simscada_waikato_dual.sqlite";
      conn.Database = "C:\\aurora\\Foxboro_SimSCADA_DB_v4_6_7_v11_DELTA.sqlite";
      return conn;
    }
    case ttaSynTestDb:
    {
      orm::v1::Conn conn;
      conn.Type = orm::v1::Conn::SQLite;
      conn.Database = "G:\\db\\syntest.sqlite";
      return conn;
    }
    case ttaEnmacDb:
    {
      orm::v1::Conn conn;
      conn.Type = orm::v1::Conn::Oracle;
      conn.Database = "NMS";
      conn.Host = "10.211.55.15";
      conn.Port = 1521;
      conn.Username = "enmac";
      conn.Password = "password";
      return conn;
    }
    default:
      assert(false);
      break;
  }

  throw std::runtime_error("Unknown database main::databaseConnectionSettings");
}
// ----------------------------------------------------------------------------

orm::v1::Conn
Main::metaDataDatabaseConnectionSettings()
{
  switch (mGui->selectedDatabase())
  {
    case samsDb:
      return databaseConnectionSettings();

    case sysCfgDb:
    {
      orm::v1::Conn conn;
      conn.Type = orm::v1::Conn::PostGreSQL;
      conn.Database = "wedn_110111";
      conn.Host = "10.37.129.4";
      conn.Port = 5481;
      conn.Username = "postgres";
      conn.Password = "postgres";
      return conn;
    }

    case enmacDb:
    {
      orm::v1::Conn conn;
      conn.Type = orm::v1::Conn::PostGreSQL;
      conn.Database = "rsconf_enmac";
      conn.Host = "127.0.0.1";
      conn.Port = 5432;
      conn.Username = "postgres";
      conn.Password = "postgres";
      return conn;
    }
    case simScadaDb:
    {
      orm::v1::Conn conn;
      conn.Type = orm::v1::Conn::PostGreSQL;
      conn.Database = "rsconf_simscada";
      conn.Host = "127.0.0.1";
      conn.Port = 5432;
      conn.Username = "postgres";
      conn.Password = "postgres";
      return conn;
    }
    case ttaSimScadaDb:
    {
      orm::v1::Conn conn;
      conn.Type = orm::v1::Conn::PostGreSQL;
      conn.Database = "tta_rsconf_simscada";
      conn.Host = "127.0.0.1";
      conn.Port = 5432;
      conn.Username = "postgres";
      conn.Password = "postgres";
      return conn;
    }
    case ttaSynTestDb:
    {
      orm::v1::Conn conn;
      conn.Type = orm::v1::Conn::PostGreSQL;
      conn.Database = "tta_rsconf_syntest";
      conn.Host = "127.0.0.1";
      conn.Port = 5432;
      conn.Username = "postgres";
      conn.Password = "postgres";
      return conn;
    }
    case ttaEnmacDb:
    {
      orm::v1::Conn conn;
      conn.Type = orm::v1::Conn::PostGreSQL;
      conn.Database = "tta_rsconf_enmac";
      conn.Host = "127.0.0.1";
      conn.Port = 5432;
      conn.Username = "postgres";
      conn.Password = "postgres";
      return conn;
    }

    default:
      assert(false);
  }
}
// ----------------------------------------------------------------------------

Main::DbocSettings
Main::dbocSettings()
{
  switch (mGui->selectedDatabase())
  {
    case samsDb:
    {
      DbocSettings settings;
      settings.path = "c:\\workspaces\\sams\\DbSamsLib\\Source\\sams";
      settings.modelName = "db_sams";
      settings.modelClass = "SamsModel";
      return settings;
    }
    case sysCfgDb:
    {
      DbocSettings settings;
      settings.path = "c:\\workspaces\\sams\\DbSysCfgLib\\Source\\syscfg";
      settings.modelName = "db_syscfg";
      settings.modelClass = "SysCfgModel";
      return settings;
    }
    case enmacDb:
    {
      DbocSettings settings;
      settings.path = "c:\\workspaces\\sams\\DbEnmacLib\\Source\\enmac";
      settings.modelName = "db_enmac";
      settings.modelClass = "EnmacModel";
      return settings;
    }
    case simScadaDb:
    {
      DbocSettings settings;
      settings.path = "c:\\workspaces\\sams\\DbSimScadaLib\\Source\\simscada";
      settings.modelName = "db_simscada";
      settings.modelClass = "SimScadaModel";
      return settings;
    }
    case ttaSimScadaDb:
    {
      DbocSettings settings;
      settings.path = "c:\\workspaces\\syntest\\DbSimScadaLib\\Source\\simscada";
      settings.modelName = "db_simscada";
      settings.modelClass = "SimScadaModel";
      return settings;
    }
    case ttaSynTestDb:
    {
      DbocSettings settings;
      settings.path = "c:\\workspaces\\syntest\\DbSynTestLib\\Source\\syntest";
      settings.modelName = "db_syntest";
      settings.modelClass = "SynTestModel";
      return settings;
    }
    case ttaEnmacDb:
    {
      DbocSettings settings;
      settings.path = "c:\\workspaces\\syntest\\DbEnmacLib\\Source\\enmac";
      settings.modelName = "db_enmac";
      settings.modelClass = "EnmacModel";
      return settings;
    }
    default:
      assert(false);
  }
  throw std::runtime_error("uncought enum");
  return DbocSettings();
}
// ----------------------------------------------------------------------------

Main::DbMetaSettings
Main::dbmetaSettings()
{
  switch (mGui->selectedDatabase())
  {
    case samsDb:
    {
      DbMetaSettings settings;
      settings.isManuallyConfigured = false;
      settings.useQsqlMetaData = false;
      return settings;
    }
    case sysCfgDb:
    {
      DbMetaSettings settings;
      settings.isManuallyConfigured = false;
      settings.useQsqlMetaData = false;
      return settings;
    }
    case enmacDb:
    {
      DbMetaSettings settings;
      settings.isManuallyConfigured = false;
      settings.useQsqlMetaData = false;
      return settings;
    }
    case simScadaDb:
    {
      DbMetaSettings settings;
      settings.isManuallyConfigured = false;
      settings.useQsqlMetaData = true;
      settings.defaultSchema = "sim";
      return settings;
    }
    case ttaSimScadaDb:
    {
      DbMetaSettings settings;
      settings.isManuallyConfigured = false;
      settings.useQsqlMetaData = true;
      settings.defaultSchema = "sim";
      return settings;
    }
    case ttaSynTestDb:
    {
      DbMetaSettings settings;
      settings.isManuallyConfigured = false;
      settings.useQsqlMetaData = true;
      settings.defaultSchema = "st";
      return settings;
    }
    case ttaEnmacDb:
    {
      DbMetaSettings settings;
      settings.isManuallyConfigured = false;
      settings.useQsqlMetaData = false;
      return settings;
    }
    default:
      assert(false);
      break;
  }
}
// ----------------------------------------------------------------------------


// ---------------
// Private functions

// ----------------------------------------------------------------------------


