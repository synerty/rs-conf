/*
 * uHierarchModel.cpp
 *
 *  Created on: Aug 25, 2010
 *      Author: Jarrod Chesney
 */

#include "ValueEditorExplorerModel.h"

#include <assert.h>
#include <list>
#include <algorithm>

#include <QtGui/qcolor.h>
#include <QtGui/qbrush.h>

#include "SmOrmModel.h"
#include "SmValueType.h"
#include "SmValue.h"
#include "SmTable.h"

#include "QtModelUserRoles.h"

//---------------------------------------------------------------------------
//                           ValueEditorExplorerModel private
//---------------------------------------------------------------------------
namespace ValueEditorExplorerModelPrivate
{

  enum SmValueTypeE
  {
    NoObject,
    ValueTypeNode,
    ValueNode
  };

  struct Node
  {
      ValueEditorExplorerModelPrivate::Node* parent;
      int row;
      SmVariantT* variant;
      SmValueTypeE objectType;
      typedef std::vector< ValueEditorExplorerModelPrivate::Node* > ChildrenT;
      ChildrenT children;
      std::string name;

      Node() :
        parent(NULL), row(0), variant(NULL), objectType(NoObject)
      {
      }
  };

}

using namespace ValueEditorExplorerModelPrivate;

//---------------------------------------------------------------------------
//                       ValueEditorExplorerModel Implementation
//---------------------------------------------------------------------------

ValueEditorExplorerModel::ValueEditorExplorerModel(boost::shared_ptr< SmModel > smModel) :
  mRootNode(NULL), //
      mSmModel(smModel)
{
  setupModel();
} //---------------------------------------------------------------------------

ValueEditorExplorerModel::~ValueEditorExplorerModel()
{
  qDebug("Destructing ValueEditorExplorerModel");
}
//---------------------------------------------------------------------------

// Abstract implementation
QVariant
ValueEditorExplorerModel::headerData(int prmSection,
    Qt::Orientation prmOrientation,
    int prmRole) const
{
  if (prmOrientation == Qt::Vertical || prmRole != Qt::DisplayRole)
    return QVariant();

  switch (prmSection)
  {
    case ColName:
      return "Name";

    case ColTable:
      return "Table";

    default:
      assert(false);
  }

  return QVariant();
}
//---------------------------------------------------------------------------

QModelIndex
ValueEditorExplorerModel::index(int row, int column, const QModelIndex& parent) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      row,
  //      column,
  //      reinterpret_cast< uintptr_t > (parent.internalPointer()));

  // Special Case  - If the parent is the root
  if (parent == QModelIndex())
    return createIndex(row, column, mRootNode->children.at(row));

  ValueEditorExplorerModelPrivate::Node& parentNode = Node(parent);
  return createIndex(row, column, parentNode.children.at(row));
}
//---------------------------------------------------------------------------

QModelIndex
ValueEditorExplorerModel::parent(const QModelIndex& index) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      index.row(),
  //      index.column(),
  //      reinterpret_cast< uintptr_t > (index.internalPointer()));

  assert(index.isValid());

  ValueEditorExplorerModelPrivate::Node& node = Node(index);

  // Every node in the model will have a parent
  if (node.parent->variant == NULL)
    return QModelIndex();

  return createIndex(node.parent->row, 0, node.parent);
}
//---------------------------------------------------------------------------

int
ValueEditorExplorerModel::rowCount(const QModelIndex& parent) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      parent.row(),
  //      parent.column(),
  //      reinterpret_cast< uintptr_t > (parent.internalPointer()));

  if (mRootNode == NULL)
    return 0;

  // Special Case  - If the parent is the root
  if (parent == QModelIndex())
    return mRootNode->children.size();

  return Node(parent).children.size();
}
//---------------------------------------------------------------------------

int
ValueEditorExplorerModel::columnCount(const QModelIndex& parent) const
{
  return ColCount;
}
//---------------------------------------------------------------------------

Qt::ItemFlags
ValueEditorExplorerModel::flags(const QModelIndex &index) const
{
  Qt::ItemFlags f = QAbstractItemModel::flags(index);

  const ValueEditorExplorerModelPrivate::Node& node = Node(index);
  const int col = index.column();
  switch (node.objectType)
  {
    case ValueTypeNode:
    {
      if (col == ColName || col == ColTable)
        f |= Qt::ItemIsEditable;
      break;
    }
    case ValueNode:
    {
      if (col == ColName)
        f |= Qt::ItemIsEditable;
      break;
    }
    case NoObject:
    {
      assert(false);
      break;
    }
  }

  return f;
}
//---------------------------------------------------------------------------

QVariant
ValueEditorExplorerModel::data(const QModelIndex& index, int role) const
{
  // We only provide certain data.
  if (!index.isValid() || !(0 <= index.column() && index.column() < ColCount))
    return QVariant();

  const ValueEditorExplorerModelPrivate::Node& node = Node(index);
  switch (node.objectType)
  {
    case ValueTypeNode:
    {
      SmValueType::PtrT object = *boost::get< SmValueType::PtrT >(node.variant);
      assert(object.get());
      return data(object, index, role);
      break;
    }
    case ValueNode:
    {
      SmValue::PtrT object = *boost::get< SmValue::PtrT >(node.variant);
      assert(object.get());
      return data(object, index, role);
      break;
    }
    case NoObject:
      assert(false);
      break;
  }
  assert(false);
  return QVariant();
}
//---------------------------------------------------------------------------

QVariant
ValueEditorExplorerModel::data(SmValueType::PtrT& object,
    const QModelIndex& index,
    int role) const
{
  assert(object.get() != NULL);

  switch (role)
  {
    case Qt::DisplayRole:
    case Qt::ToolTipRole:
    case Qt::EditRole:
    {
      switch (index.column())
      {
        case ColName:
          return QString(object->name().c_str());

        case ColTable:
        {
          if (object->table().get() != NULL)
            return QString(object->table()->fullName().c_str());
          break;
        }
      }
      break;
    }
  }

  if (role == Qt::ComboOptionsRole && index.column() == ColTable)
  {
    assert(mSmModel.get() != NULL);

    QStringList list;
    list.push_back("");

    for (SmModel::TableListT::const_iterator itr = mSmModel->Tables().begin(); //
    itr != mSmModel->Tables().end(); ++itr)
    {
      const SmTable::PtrT& table = *itr;
      QString name(table->fullName().c_str());
      if (!(name.contains("VALUE") && name.contains("meta")))
        continue;
      list.push_back(name);
    }

    list.sort();
    return list;
  }

  return QVariant();
}
//---------------------------------------------------------------------------

QVariant
ValueEditorExplorerModel::data(SmValue::PtrT& object,
    const QModelIndex& index,
    int role) const
{
  assert(object.get() != NULL);

  switch (role)
  {
    case Qt::DisplayRole:
    case Qt::ToolTipRole:
    case Qt::EditRole:
    {
      switch (index.column())
      {
        case ColName:
          return QString(object->name().c_str());
      }
      break;
    }
  }

  return QVariant();
}
//---------------------------------------------------------------------------

bool
ValueEditorExplorerModel::setData(const QModelIndex &index,
    const QVariant &value,
    int role)
{
  const ValueEditorExplorerModelPrivate::Node& node = Node(index);
  switch (node.objectType)
  {
    case ValueTypeNode:
    {
      SmValueType::PtrT object = *boost::get< SmValueType::PtrT >(node.variant);
      return setData(object, index, value, role);
      break;
    }
    case ValueNode:
    {
      SmValue::PtrT object = *boost::get< SmValue::PtrT >(node.variant);
      return setData(object, index, value, role);
      break;
    }
    case NoObject:
      assert(false);
      break;
  }
  return false;
}
//---------------------------------------------------------------------------

bool
ValueEditorExplorerModel::setData(SmValueType::PtrT& object,
    const QModelIndex &index,
    const QVariant &value,
    int role)
{
  assert(object.get());

  if (index.column() == ColName && role == Qt::EditRole)
  {
    object->setName(value.toString().toStdString());
    return true;
  }

  if (index.column() == ColTable && role == Qt::EditRole)
  {
    assert(mSmModel.get() != NULL);
    const std::string strVal = value.toString().toStdString();
    for (SmModel::TableListT::const_iterator itr = mSmModel->Tables().begin(); //
    itr != mSmModel->Tables().end(); ++itr)
    {
      const SmTable::PtrT& table = *itr;
      if (table->fullName() == strVal)
      {
        object->setTable(table);
        return true;
      }
    }

  }

  return false;
}
//---------------------------------------------------------------------------

bool
ValueEditorExplorerModel::setData(SmValue::PtrT& object,
    const QModelIndex &index,
    const QVariant &value,
    int role)
{
  assert(object.get());

  if (index.column() == ColName && role == Qt::EditRole)
  {
    object->setName(value.toString().toStdString());
    return true;
  }

  return false;
}
//---------------------------------------------------------------------------

ValueEditorExplorerModelPrivate::Node&
ValueEditorExplorerModel::Node(const QModelIndex& index) const
{
  if (!index.isValid())
    assert(false);

  ValueEditorExplorerModelPrivate::Node
      * obj = static_cast< ValueEditorExplorerModelPrivate::Node* > (index.internalPointer());
  obj = dynamic_cast< ValueEditorExplorerModelPrivate::Node* > (obj);
  assert(obj);
  return *obj;
}
//---------------------------------------------------------------------------

void
ValueEditorExplorerModel::setupModel()
{
  mRootNode = &mNodes[SmVariantT()];
  qDebug("mNodes.size() = %d", mNodes.size());

  for (SmValueType::ListT::const_iterator itr = mSmModel->ValueTypes().begin();//
  itr != mSmModel->ValueTypes().end(); ++itr)
    addType(*itr);

  for (SmValue::ListT::const_iterator itr = mSmModel->Values().begin();//
  itr != mSmModel->Values().end(); ++itr)
    addValue(*itr);
}
//---------------------------------------------------------------------------

QModelIndex
ValueEditorExplorerModel::addType(const boost::shared_ptr< SmValueType >& type)
{
  // Get the parent (recursion is fun)
  ValueEditorExplorerModelPrivate::Node* node = NodeFromMap(type);
  assert(node->variant == NULL);

  // Set the parent
  node->parent = mRootNode;
  node->row = mRootNode->children.size();

  node->variant = KeyFromMap(type);
  node->objectType = ValueTypeNode;

  beginInsertColumns(QModelIndex(), node->row, node->row);
  mRootNode->children.push_back(node); // This makes it apart of the model
  endInsertRows();

  return createIndex(node->row, 0, node);
}
//---------------------------------------------------------------------------

QModelIndex
ValueEditorExplorerModel::addValue(const boost::shared_ptr< SmValue >& value)
{
  // Get the parent (recursion is fun)
  ValueEditorExplorerModelPrivate::Node
      * parentNode = NodeFromMap(value->type());
  ValueEditorExplorerModelPrivate::Node* node = NodeFromMap(value);

  assert(parentNode->variant != NULL);
  assert(node->variant == NULL);

  QModelIndex parentIndex = createIndex(parentNode->row, 0, parentNode);

  // Set the parent
  node->parent = parentNode;
  node->row = parentNode->children.size();

  node->variant = KeyFromMap(value);
  node->objectType = ValueNode;

  beginInsertColumns(parentIndex, node->row, node->row);
  parentNode->children.push_back(node); // This makes it apart of the model
  endInsertRows();

  return createIndex(node->row, 0, node);
}
//---------------------------------------------------------------------------

SmValueType::PtrT
ValueEditorExplorerModel::type(const QModelIndex& index)
{
  const ValueEditorExplorerModelPrivate::Node& node = Node(index);
  if (node.objectType != ValueTypeNode)
    return SmValueType::PtrT();

  SmValueType::PtrT* object = boost::get< SmValueType::PtrT >(node.variant);
  assert(object != NULL && object->get() != NULL);
  return *object;
}
//---------------------------------------------------------------------------

SmValueType::PtrT
ValueEditorExplorerModel::smValueType(const QModelIndex& index)
{
  const ValueEditorExplorerModelPrivate::Node& node = Node(index);
  if (node.objectType != ValueTypeNode)
    return SmValueType::PtrT();

  SmValueType::PtrT* object = boost::get< SmValueType::PtrT >(node.variant);
  assert(object != NULL && object->get() != NULL);
  return *object;
}
//---------------------------------------------------------------------------

SmValue::PtrT
ValueEditorExplorerModel::smValue(const QModelIndex& index)
{
  const ValueEditorExplorerModelPrivate::Node& node = Node(index);
  if (node.objectType != ValueNode)
    return SmValue::PtrT();

  SmValue::PtrT* object = boost::get< SmValue::PtrT >(node.variant);
  assert(object != NULL && object->get() != NULL);
  return *object;
}
//---------------------------------------------------------------------------


ValueEditorExplorerModelPrivate::Node*
ValueEditorExplorerModel::NodeFromMap(ValueEditorExplorerModelPrivate::SmVariantT key)
{
  return &mNodes[key];
}
//---------------------------------------------------------------------------

ValueEditorExplorerModelPrivate::SmVariantT*
ValueEditorExplorerModel::KeyFromMap(ValueEditorExplorerModelPrivate::SmVariantT key)
{
  NodeMapT::iterator mapItr = mNodes.find(key);
  return const_cast< SmVariantT* > (&mapItr->first);
}
//---------------------------------------------------------------------------
