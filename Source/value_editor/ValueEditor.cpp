#include "ValueEditor.h"

#include <boost/format.hpp>

#include <QtGui/qstandarditemmodel.h>
#include <QtGui/qmessagebox.h>

#include "Main.h"

#include "SmOrmModel.h"
#include "SmValueType.h"
#include "SmValue.h"
#include "SmTable.h"
#include "SmColumn.h"

//#include "orm/v1/MvLink.h"
#include "orm/v1/DynamicStorable.h"
#include "orm/v1/SimpleProperty.h"

#include "ValueEditorExplorerModel.h"
#include "PropertiesModel.h"
#include "ComboItemDelegate.h"
#include "QtModelTest.h"

ValueEditor::ValueEditor(QWidget *parent)
{
  ui.setupUi(parent);

  ui.treeObjects->setItemDelegateForColumn(ValueEditorExplorerModel::ColTable,
      new ComboItemDelegate(parent));

  connect(ui.btnLoad, SIGNAL(clicked()), this, SLOT(DoLoad()));
  connect(ui.btnAddType, SIGNAL(clicked()), this, SLOT(DoAddType()));
  connect(ui.btnAddValue, SIGNAL(clicked()), this, SLOT(DoAddValue()));
  connect(ui.btnSave, SIGNAL(clicked()), this, SLOT(DoSave()));

  connect(ui.treeObjects,
      SIGNAL(clicked(const QModelIndex&)),
      this,
      SLOT(DoItemSelected(const QModelIndex&)));

}
// ----------------------------------------------------------------------------

ValueEditor::~ValueEditor()
{

}
// ----------------------------------------------------------------------------

void
ValueEditor::Reset()
{
  mSmModel.reset(new SmModel(Main::Inst().metaDataDatabaseConnectionSettings()));

  ui.treeObjects->setModel(new QStandardItemModel(ui.treeObjects));
  mQtExplorerModel.reset();

  ui.treeProperties->setModel(NULL);
  mQtPropertyModel.reset();

}
// ----------------------------------------------------------------------------

void
ValueEditor::DoLoad()
{
  mSmModel.reset(new SmModel(Main::Inst().metaDataDatabaseConnectionSettings()));
  assert(mSmModel->load());

  mQtExplorerModel.reset(new ValueEditorExplorerModel(mSmModel));
  ui.treeObjects->setModel(mQtExplorerModel.get());

  ui.treeObjects->resizeColumnToContents(0);

  QtModelTest(mQtExplorerModel.get()).run();

}
// ----------------------------------------------------------------------------

void
ValueEditor::DoSave()
{
  try
  {
    assert(mSmModel->save());
    if (mQtPropertyModel.get() != NULL)
      assert(mQtPropertyModel->save());
  }
  catch (std::exception& e)
  {
    QString msg = "An error occured saving the data, error was :\n";
    msg += e.what();
    QMessageBox::critical(ui.btnSave, "Save Failed", msg);
    return;
  }
}
// ----------------------------------------------------------------------------

void
ValueEditor::DoAddType()
{
  assert(mQtExplorerModel.get());
  SmValueType::PtrT type(new SmValueType());
  type->setId(mSmModel->NextMetaRecordId());
  type->setName("NewType");
  type->setGenerateEnum(true);
  mSmModel->addValueType(type);

  SelectIndex(mQtExplorerModel->addType(type));
}
// ----------------------------------------------------------------------------

void
ValueEditor::DoAddValue()
{
  assert(mQtExplorerModel.get());

  QModelIndexList
      selectedIndexs = ui.treeObjects->selectionModel()->selectedRows();

  SmValueType::PtrT type;

  if (!selectedIndexs.empty())
    type = mQtExplorerModel->type(selectedIndexs.front());

  if (type.get() == NULL)
    return;

  SmValue::PtrT value(new SmValue());
  value->setId(mSmModel->NextMetaRecordId());
  value->setType(type);
  value->setName("NewValue");
  mSmModel->addValue(value);

  SelectIndex(mQtExplorerModel->addValue(value));

}
// ----------------------------------------------------------------------------

void
ValueEditor::DoItemSelected(const QModelIndex & current)
{
  assert(mQtExplorerModel.get());

  using namespace orm::v1;
  ui.treeProperties->setModel(NULL);
  mQtPropertyModel.reset();

  if (!current.isValid())
    return;

  SmValue::PtrT selectedVal = mQtExplorerModel->smValue(current);

  if (selectedVal.get() != NULL)
  {
    SmTable::PtrT table = selectedVal->type()->table();
    if (table.get() == NULL)
      return;

    mQtPropertyModel.reset(new PropertiesModel(mSmModel,
        selectedVal->id(),
        table->fullName()));

    ui.treeProperties->setModel(mQtPropertyModel.get());
    ui.treeProperties->expandAll();
    ui.treeProperties->resizeColumnToContents(0);
    ui.treeProperties->resizeColumnToContents(1);

    return;
  }

  SmValueType::PtrT selectedValTyp = mQtExplorerModel->smValueType(current);

  if (selectedValTyp.get() != NULL)
  {
    mQtPropertyModel.reset(new PropertiesModel(mSmModel,
        selectedValTyp->id(),
        selectedValTyp->config()->sqlOpts()->tableName()));

    ui.treeProperties->setModel(mQtPropertyModel.get());
    ui.treeProperties->expandAll();
    ui.treeProperties->resizeColumnToContents(0);
    return;
  }

  ui.treeProperties->setModel(NULL);
  return;

}

void
ValueEditor::SelectIndex(QModelIndex index)
{
  // Create a reverse list of parents
  QModelIndexList parents;
  for (QModelIndex parent = index; //
  parent != QModelIndex(); parent = parent.parent())
    parents.push_front(parent);

  // Expand the parents in order
  foreach(QModelIndex parent, parents)
      ui.treeObjects->expand(parent);

  // Select the item
  ui.treeObjects->selectionModel()->select(index,
      QItemSelectionModel::ClearAndSelect);
}
// ----------------------------------------------------------------------------
