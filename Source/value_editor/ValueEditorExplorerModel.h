/*
 * uHierarchModel.h
 *
 *  Created on: Aug 25, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _VALUE_EDITOR_OBJECTS_QT_MODEL_H_
#define _VALUE_EDITOR_OBJECTS_QT_MODEL_H_

// Include Std headers
#include <string>
#include <map>
#include <vector>
#include <set>

#include <boost/shared_ptr.hpp>
#include <boost/variant.hpp>
// Include Qt headers
#include <QtCore/qabstractitemmodel.h>

#include "SmValueType.h"

class SmModel;
class SmValueType;

namespace ValueEditorExplorerModelPrivate
{
  class Node;
  typedef boost::variant< //
      boost::shared_ptr< SmValueType >, //
      boost::shared_ptr< SmValue > //
  > SmVariantT;

}

class ValueEditorExplorerModel : public QAbstractItemModel
{
  public:
    enum TeCol
    {
      ColName,
      ColTable,
      ColCount

    };

  public:
    ValueEditorExplorerModel(boost::shared_ptr< SmModel > smModel);
    virtual
    ~ValueEditorExplorerModel();

  public:

    boost::shared_ptr< SmValueType >
    type(const QModelIndex& index);

    boost::shared_ptr< SmValueType >
    smValueType(const QModelIndex& index);

    boost::shared_ptr< SmValue >
    smValue(const QModelIndex& index);

    QModelIndex
    addType(const boost::shared_ptr< SmValueType >& type);

    QModelIndex
    addValue(const boost::shared_ptr< SmValue >& value);

  public:
    // Abstract implementation
    QVariant
    headerData(int prmSection,
        Qt::Orientation prmOrientation,
        int prmRole = Qt::DisplayRole) const;

    QModelIndex
    index(int row, int column, const QModelIndex& parent = QModelIndex()) const;
    QModelIndex
    parent(const QModelIndex& index) const;

    int
    rowCount(const QModelIndex& parent = QModelIndex()) const;

    int
    columnCount(const QModelIndex& parent = QModelIndex()) const;

    QVariant
    data(const QModelIndex& index, int prmRole = Qt::DisplayRole) const;

    Qt::ItemFlags
    flags(const QModelIndex &index) const;

    bool
    setData(const QModelIndex &index,
        const QVariant &value,
        int role = Qt::EditRole);

  private:
    /// --------------
    // ObjectModel methods

    QVariant
    data(boost::shared_ptr< SmValueType >& object,
        const QModelIndex& index,
        int role) const;

    QVariant
    data(boost::shared_ptr< SmValue >& object,
        const QModelIndex& index,
        int role) const;

    bool
    setData(boost::shared_ptr< SmValueType >& object,
        const QModelIndex &index,
        const QVariant &value,
        int role);

    bool
    setData(boost::shared_ptr< SmValue >& object,
        const QModelIndex &index,
        const QVariant &value,
        int role);

    ValueEditorExplorerModelPrivate::Node&
    Node(const QModelIndex& index) const;

    void
    setupModel();

    ValueEditorExplorerModelPrivate::Node*
    NodeFromMap(ValueEditorExplorerModelPrivate::SmVariantT key);

    ValueEditorExplorerModelPrivate::SmVariantT*
    KeyFromMap(ValueEditorExplorerModelPrivate::SmVariantT key);

    typedef std::map< ValueEditorExplorerModelPrivate::SmVariantT,
        ValueEditorExplorerModelPrivate::Node > NodeMapT;
    NodeMapT mNodes;

    /*! Link to the root node.
     * This node will not be in the model, its equivilent to QModelIndex()
     */
    ValueEditorExplorerModelPrivate::Node* mRootNode;

    boost::shared_ptr< SmModel > mSmModel;

};
// ----------------------------------------------------------------------------


#endif /* _VALUE_EDITOR_OBJECTS_QT_MODEL_H_ */
