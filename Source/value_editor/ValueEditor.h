#ifndef _VALUE_EDITOR_H_
#define _VALUE_EDITOR_H_

#include <auto_ptr.h>

#include <boost/shared_ptr.hpp>

#include <QtCore/qobject.h>
#include "ui_ValueEditor.h"

class SmModel;
class SmTable;
class ValueEditorExplorerModel;
class PropertiesModel;

namespace orm
{
  namespace v1
  {
    class DynamicStorableConfig;
    class DynamicModel;
  }
}

class ValueEditor : public QObject
{
  Q_OBJECT

  public:
    ValueEditor(QWidget *parent = 0);

    virtual
    ~ValueEditor();

    void
    Reset();

  public slots:
    void
    DoLoad();

    void
    DoSave();

    void
    DoAddType();

    void
    DoAddValue();

    void
    DoItemSelected(const QModelIndex & current);

  private:
    void
    SelectIndex(QModelIndex index);

    Ui::ValueEditorClass ui;

    boost::shared_ptr< SmModel > mSmModel;

    std::auto_ptr< ValueEditorExplorerModel > mQtExplorerModel;
    std::auto_ptr< PropertiesModel > mQtPropertyModel;

};

#endif // _VALUE_EDITOR_H_
