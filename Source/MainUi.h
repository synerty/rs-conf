#ifndef FUMAIN_H
#define FUMAIN_H

#include <auto_ptr.h>

#include <boost/shared_ptr.hpp>

#include <QtGui/QMainWindow>
#include "ui_MainUi.h"

class Main;
class QLabel;
class DbMetaConfig;
class AppMetaConfig;
class ValueEditor;
class DbObjectCompiler;
class Log;

class MainUi : public QMainWindow
{
  Q_OBJECT

  public:
    MainUi(Main& uc);
    virtual
    ~MainUi();

    int
    selectedDatabase();

  public slots:

    void
    databaseChanged(int index);

  private:
    // Functions
    void
    connectSlots();

    // Members
    Ui::MainUiClass ui;

    Main& mUc;
    boost::shared_ptr< DbMetaConfig > mDbMetaConfig;
    boost::shared_ptr< AppMetaConfig > mAppMetaConfig;
    boost::shared_ptr< ValueEditor > mValueEditor;
    boost::shared_ptr< DbObjectCompiler > mDbObjectCompiler;
    boost::shared_ptr< Log > mLog;
};

#endif // FUMAIN_H
