/*
 * QSqlMetaData.cpp
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#include "QSqlMetaData.h"

#include <boost/format.hpp>

#include <QtCore/qstring.h>
#include <QtCore/qstringlist.h>

#include <QtSql/qsqldriver.h>
#include <QtSql/qsqldatabase.h>
#include <QtSql/qsqlrecord.h>
#include <QtSql/qsqlquery.h>
#include <QtSql/qsqlfield.h>
#include <QtSql/qsqlindex.h>

#include "IsSchema.h"
#include "IsTable.h"
#include "IsColumn.h"

#include "IsOrmModel.h"

QSqlMetaData::QSqlMetaData(const std::string defaultSchema,
    boost::shared_ptr< IsModel > metadataModel) :
  mDefaultSchema(defaultSchema), //
      mDriver(*metadataModel->database().driver()),//
      mMetadataModel(metadataModel)
{

}

QSqlMetaData::~QSqlMetaData()
{
}

void
QSqlMetaData::run()
{
  importTables();

}

void
QSqlMetaData::importTables()
{
  IsSchemaPtrT schema(new IsSchema());
  schema->setName(mDefaultSchema);
  schema->addToModel(mMetadataModel);

  QStringList tables = mDriver.tables(QSql::Tables);

  for (QStringList::iterator tblItr = tables.begin(); //
  tblItr != tables.end(); ++tblItr)
    importTable(schema, *tblItr);
}

void
QSqlMetaData::importTable(IsSchemaPtrT schema, QString tableName)
{
  IsTablePtrT table(new IsTable());
  table->setSchema(schema);
  table->setName(tableName.toStdString());
  table->addToModel(mMetadataModel);

  qDebug(tableName.toStdString().c_str());

  QSqlIndex primaryIndex = mDriver.primaryIndex(tableName);

  QSqlRecord rec = mDriver.record(tableName);
  for (int i = 0; i < rec.count(); ++i)
    importField(table, primaryIndex, rec.field(i));

}

void
QSqlMetaData::importField(IsTablePtrT table,
    const QSqlIndex& primaryIndex,
    const QSqlField& field)
{
  IsColumnPtrT column(new IsColumn());
  column->setTable(table);
  column->setDataLength(0);
  column->setName(field.name().toStdString());

  std::string dataType;
  switch (field.type())
  {
    case QVariant::Int:
      dataType = "int4";
      break;

    case QVariant::String:
      dataType = "string";
      break;

    case QVariant::LongLong:
      dataType = "int8";
      break;

    case QVariant::Bool:
      dataType = "bool";
      break;

    case QVariant::Double:
      dataType = "float8";
      break;

    default:
    {
      boost::format info("Unknown type %d");
      info % field.type();
      throw std::runtime_error(info.str());
    }
  }

  column->setDataType(dataType);

  column->setPrimaryKey(primaryIndex.contains(field.name()));
  column->setNullable(field.requiredStatus() == QSqlField::Optional);
  column->addToModel(mMetadataModel);

}

