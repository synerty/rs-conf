/*
 * TableMatches.h
 *
 *  Created on: Feb 4, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _TABLE_MATCHES_H_
#define _TABLE_MATCHES_H_

#include <vector>

#include <boost/shared_ptr.hpp>

class SmModel;
class IsModel;
class SmSchema;
class SmTable;
class IsTable;

/*! TableMatches Brief Description
 * Long comment about the class
 *
 */
class TableMatches
{
  public:

    typedef boost::shared_ptr< SmSchema > SmSchemaPtrT;
    typedef boost::shared_ptr< SmTable > SmTablePtrT;
    typedef boost::shared_ptr< IsTable > IsTablePtrT;

    struct Match
    {
        SmSchemaPtrT smSchema;
        SmTablePtrT smTable;
        IsTablePtrT isTable;
        bool ignore;

        Match(SmSchemaPtrT smSchema_,
            SmTablePtrT smTable_,
            IsTablePtrT isTable_);

        Match();

        Match&
        operator=(const Match& other);

        bool
        operator<(const Match& other) const;
    };

    typedef std::vector< Match > ListT;

  public:
    TableMatches(boost::shared_ptr< SmModel >& smModel,
        boost::shared_ptr< IsModel >& isModel);
    virtual
    ~TableMatches();

  public:
    void
    Compare();

    void
    Save();

    ListT&
    Matches();

  private:

    boost::shared_ptr< SmModel >& mSmModel;
    boost::shared_ptr< IsModel >& mIsModel;

    ListT mMaches;
};

#endif /* _TABLE_MATCHES_H_ */
