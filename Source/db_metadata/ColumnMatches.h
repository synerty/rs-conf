/*
 * ColumnMatches.h
 *
 *  Created on: Feb 4, 2011
 *      Author: Jarrod Chesney
 */

#ifndef _COLUMN_MATCHES_H_
#define _COLUMN_MATCHES_H_

#include <vector>

#include <boost/shared_ptr.hpp>

class SmModel;
class IsModel;
class SmTable;
class SmColumn;
class IsColumn;

/*! ColumnMatches Brief Description
 * Long comment about the class
 *
 */
class ColumnMatches
{
  public:

    typedef boost::shared_ptr< SmTable > SmTablePtrT;
    typedef boost::shared_ptr< SmColumn > SmColumnPtrT;
    typedef boost::shared_ptr< IsColumn > IsColumnPtrT;

    struct Match
    {
        SmTablePtrT smTable;
        SmColumnPtrT smColumn;
        IsColumnPtrT isColumn;
        bool ignore;

        Match(SmTablePtrT smTable_,
            SmColumnPtrT smColumn_,
            IsColumnPtrT isColumn_);

        Match();

        Match&
        operator=(const Match& other);

        bool
        operator<(const Match& other) const;
    };

    typedef std::vector< Match > ListT;

  public:
    ColumnMatches(boost::shared_ptr< SmModel >& smModel,
        boost::shared_ptr< IsModel >& isModel);
    virtual
    ~ColumnMatches();

  public:
    void
    Compare();

    void
    Save();

    ListT&
    Matches();

  private:

    boost::shared_ptr< SmModel >& mSmModel;
    boost::shared_ptr< IsModel >& mIsModel;

    ListT mMaches;
};

#endif /* _COLUMN_MATCHES_H_ */
