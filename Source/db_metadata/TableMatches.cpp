/*
 * TableMatches.cpp
 *
 *  Created on: Feb 4, 2011
 *      Author: Jarrod Chesney
 */

#include "TableMatches.h"

#include "SmOrmModel.h"
#include "IsOrmModel.h"

#include "SmSchema.h"
#include "SmTable.h"
#include "IsSchema.h"
#include "IsTable.h"
#include "IsInherit.h"

TableMatches::Match::Match(SmSchemaPtrT smSchema_,
    SmTablePtrT smTable_,
    IsTablePtrT isTable_) :
  smSchema(smSchema_), smTable(smTable_), isTable(isTable_), ignore(false)
{
}
TableMatches::Match::Match() :
  ignore(false)
{
}

TableMatches::Match&
TableMatches::Match::operator=(const Match& other)
{
  if (this == &other)
    return *this;

  smSchema = other.smSchema;
  isTable = other.isTable;
  smTable = other.smTable;
  ignore = other.ignore;

  return *this;
}

bool
TableMatches::Match::operator<(const Match& other) const
{
  if (smSchema != other.smSchema)
    return smSchema < other.smSchema;

  if (isTable != other.isTable)
    return isTable < other.isTable;

  if (smTable != other.smTable)
    return smTable < other.smTable;

  return ignore < other.ignore;
}

TableMatches::TableMatches(boost::shared_ptr< SmModel >& smModel,
    boost::shared_ptr< IsModel >& isModel) :
  mSmModel(smModel), //
      mIsModel(isModel)
{
}

TableMatches::~TableMatches()
{
}

void
TableMatches::Compare()
{
  // NOTE : We assume that all the schemas have been matched
  // We use the SmSchema as a reference for both types of tables
  mMaches.clear();
  typedef std::set< std::string > StrSetT;

  /// ----
  // Create a map of shemas by name
  typedef std::map< std::string, SmSchema::PtrT > SchemaMapT;
  SchemaMapT schemaMap;

  for (SmSchema::ListT::const_iterator itr = mSmModel->Schemas().begin();//
  itr != mSmModel->Schemas().end(); ++itr)
    schemaMap.insert(std::make_pair((*itr)->name(), *itr));

  /// ----
  // Group the SmTables by SmSchema
  typedef std::multimap< SmSchema::PtrT, SmTable::PtrT > SmTableMapT;
  SmTableMapT smTableMap;

  for (SmTable::ListT::const_iterator itr = mSmModel->Tables().begin();//
  itr != mSmModel->Tables().end(); ++itr)
    smTableMap.insert(std::make_pair((*itr)->schema(), *itr));

  /// ----
  // Group the IsTables by SmSchema
  typedef std::map< SmSchema::PtrT, IsModel::TableMapT > IsTableMapBySmSchemaT;
  IsTableMapBySmSchemaT isTableBySmSchema;

  for (IsTable::ListT::const_iterator itr = mIsModel->Tables().begin();//
  itr != mIsModel->Tables().end(); ++itr)
  {
    IsTable::PtrT isTable = *itr;
    SchemaMapT::iterator schemaItr = schemaMap.find(isTable->schema()->name());
    assert(schemaItr != schemaMap.end());

    // This will create the entry if needed
    IsModel::TableMapT& isTableByNameMap = isTableBySmSchema[schemaItr->second];
    // Insert into the submap
    isTableByNameMap.insert(std::make_pair(isTable->name(), isTable));
  }

  /// ----
  // Now, for each schema, compare the tables
  for (SmSchema::ListT::const_iterator
      smSchemaItr = mSmModel->Schemas().begin();//
  smSchemaItr != mSmModel->Schemas().end(); ++smSchemaItr)
  {
    SmSchema::PtrT smSchema = *smSchemaItr;
    StrSetT matchedTableNames;

    // Get a ranget of SmTables
    std::pair< SmTableMapT::iterator, SmTableMapT::iterator > smTableRange;
    smTableRange = smTableMap.equal_range(smSchema);

    // Get the map of IsTables for this schema
    // It may or may not exist, so just get it this way
    IsModel::TableMapT& isTablesByName = isTableBySmSchema[smSchema];

    /// ----
    // Compare SmTables against IsTables for this SmSchema
    for (SmTableMapT::const_iterator smTableItr = smTableRange.first;//
    smTableItr != smTableRange.second; ++smTableItr)
    {
      SmTable::PtrT smTable = smTableItr->second;

      // Find IsTable with same name
      IsTable::PtrT isTable;

      { // Variable scope ( too many iterators around :-| )
        IsModel::TableMapT::iterator
            isTableItr = isTablesByName.find(smTable->name());

        // If we find the table, assign it.
        if (isTableItr != isTablesByName.end())
          isTable = isTableItr->second;
      }

      mMaches.push_back(Match(smSchema, smTable, isTable));

      if (isTable.get() != NULL)
        matchedTableNames.insert(smTable->name());
    }

    // Add all the IsTables that didn't have matching SmTables for this schema
    for (IsModel::TableMapT::const_iterator itr = isTablesByName.begin(); //
    itr != isTablesByName.end(); ++itr)
    {
      IsTable::PtrT isTable = itr->second;
      if (matchedTableNames.find(isTable->name()) == matchedTableNames.end())
        mMaches.push_back(Match(smSchema, SmTable::PtrT(), isTable));
    }
  }
}

void
TableMatches::Save()
{
  typedef std::map< const std::string, SmTable::PtrT > SmTableMapT;
  SmTableMapT smTableMap;

  for (SmTable::ListT::const_iterator itr = mSmModel->Tables().begin();//
  itr != mSmModel->Tables().end(); ++itr)
    smTableMap.insert(std::make_pair((*itr)->fullName(), *itr));

  SmTable::ListT newTables;

  for (ListT::iterator itr = mMaches.begin(); itr != mMaches.end(); ++itr)
  {
    Match& match = *itr;

    if (match.ignore)
      continue;

    // New Table
    if (match.smTable.get() == NULL)
    {
      assert(match.isTable.get() != NULL);
      SmTable::PtrT newTable(new SmTable());
      newTable->setId(mSmModel->NextMetaRecordId());
      newTable->setName(match.isTable->name());
      newTable->setSchema(match.smSchema);

      if (match.isTable->inherits().get() != NULL)
      {
        // I don't care if it doesn't exist and creates a new entry
        // It will just have to be compared/saved twice.
        newTable->setInherits(smTableMap[match.isTable->inherits()->fullName()]);
        newTable->setInheritTypeFromMap(match.isTable->inheritType());
      }

      mSmModel->addTable(newTable);
      newTables.push_back(newTable);
      continue;
    }

    // Delete Table
    if (match.isTable.get() == NULL)
    {
      assert(match.smTable.get() != NULL);
      mSmModel->removeTable(match.smTable);
      continue;
    }

    // They match, update if required
    if (match.isTable->displayName() != match.smTable->compareName())
    {
      match.smTable->setName(match.isTable->name());

      if (!match.smTable->isInheritsUserDefined())
      {
        if (match.isTable->inherits().get() == NULL)
        {
          match.smTable->setInherits(SmTable::PtrT());
          match.smTable->setInheritTypeFromMap("");

        }
        else
        {
          // I don't care if it doesn't exist and creates a new entry
          // It will just have to be compared/saved twice.
          match.smTable->setInherits(smTableMap[match.isTable->inherits()->fullName()]);
          match.smTable->setInheritTypeFromMap(match.isTable->inheritType());
        }
      }
      continue;
    }
  }

  assert(mSmModel->save());

  // Create a map of tables by full name
  typedef std::map< std::string, SmTable::PtrT > SmTableByNameT;
  SmTableByNameT smTableByName;
  for (SmTable::ListT::const_iterator itr = mSmModel->Tables().begin();//
  itr != mSmModel->Tables().end(); ++itr)
  {
    SmTable::PtrT table = *itr;
    smTableByName.insert(std::make_pair(table->fullName(), table));
  }

  for (SmTable::ListT::iterator itr = newTables.begin();//
  itr != newTables.end(); ++itr)
  {
    SmTable::PtrT newTable = *itr;

    // Set inherited if required
    IsInherit::PtrT isInherit = mIsModel->Inherit(newTable->fullName());
    if (isInherit.get() == NULL)
      continue;

    SmTableByNameT::iterator
        smTableItr = smTableByName.find(isInherit->dstTable()->fullName());
    assert(smTableItr != smTableByName.end());

    newTable->setInherits(smTableItr->second);
  }

  assert(mSmModel->save());
}

TableMatches::ListT&
TableMatches::Matches()
{
  return mMaches;
}
