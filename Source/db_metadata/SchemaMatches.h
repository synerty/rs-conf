/*
 * SchemaMatches.h
 *
 *  Created on: Feb 4, 2011
 *      Author: Jarrod Chesney
 */

#ifndef SCHEMAMATCHES_H_
#define SCHEMAMATCHES_H_

#include <vector>

#include <boost/shared_ptr.hpp>

class SmModel;
class IsModel;
class SmSchema;
class IsSchema;

/*! SchemaMatches Brief Description
 * Long comment about the class
 *
 */
class SchemaMatches
{
  public:

    typedef boost::shared_ptr< SmSchema > SmSchemaPtrT;
    typedef boost::shared_ptr< IsSchema > IsSchemaPtrT;

    struct Match
    {
        SmSchemaPtrT smSchema;
        IsSchemaPtrT isSchema;
        bool ignore;

        Match(SmSchemaPtrT smSchema_, IsSchemaPtrT isSchema_);

        Match();

        Match&
        operator=(const Match& other);

        bool
        operator<(const Match& other) const;
    };

    typedef std::vector< Match > ListT;

  public:
    SchemaMatches(boost::shared_ptr< SmModel >& smModel,
        boost::shared_ptr< IsModel >& isModel);
    virtual
    ~SchemaMatches();

  public:
    void
    Compare();

    void
    Save();

    ListT&
    Matches();

  private:

    boost::shared_ptr< SmModel >& mSmModel;
    boost::shared_ptr< IsModel >& mIsModel;

    ListT mMaches;
};

#endif /* SCHEMAMATCHES_H_ */
