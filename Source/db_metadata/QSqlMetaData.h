/*
 * QSqlMetaData.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef QSQLMETADATA_H_
#define QSQLMETADATA_H_

#include <boost/shared_ptr.hpp>
#include <string>

class QSqlDriver;
class QSqlIndex;
class QSqlField;
class QString;

class IsModel;
class IsSchema;
class IsTable;
class IsColumn;

typedef boost::shared_ptr< IsSchema > IsSchemaPtrT;
typedef boost::shared_ptr< IsTable > IsTablePtrT;
typedef boost::shared_ptr< IsColumn > IsColumnPtrT;

class QSqlMetaData
{
  public:
    QSqlMetaData(const std::string defaultSchema,
        boost::shared_ptr< IsModel > metadataModel);
    virtual
    ~QSqlMetaData();

    void
    run();

  private:

    void
    importTables();

    void
    importTable(IsSchemaPtrT schema, QString tableName);

    void
    importField(IsTablePtrT table,
        const QSqlIndex& primaryIndex,
        const QSqlField& field);

    std::string mDefaultSchema;
    QSqlDriver& mDriver;
    boost::shared_ptr< IsModel > mMetadataModel;

};

#endif /* QSQLMETADATA_H_ */
