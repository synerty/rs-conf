/*
 * uHierarchModel.cpp
 *
 *  Created on: Aug 25, 2010
 *      Author: Jarrod Chesney
 */

#include "DbMetaConfigModel.h"

#include <assert.h>
#include <list>
#include <algorithm>

#include <QtGui/qcolor.h>
#include <QtGui/qbrush.h>

#include "IsOrmModel.h"
#include "IsSchema.h"
#include "IsTable.h"
#include "IsColumn.h"
#include "IsInherit.h"
#include "IsRelate.h"

#include "SmOrmModel.h"
#include "SmSchema.h"
#include "SmTable.h"
#include "SmColumn.h"

#include "DbOcFormat.h"

#include "QtModelUserRoles.h"

//---------------------------------------------------------------------------
//                           DbMetaConfigModel private
//---------------------------------------------------------------------------
namespace DbMetaConfigModelPrivate
{

  enum SmObjectTypeE
  {
    NoObject,
    Schema,
    Table,
    Column
  };

  struct Node
  {
      DbMetaConfigModelPrivate::Node* parent;
      int row;
      SmVariantT* object;
      SmObjectTypeE objectType;
      typedef std::vector< DbMetaConfigModelPrivate::Node* > ChildrenT;
      ChildrenT children;
      std::string name;

      Node() :
          parent(NULL), row(0), object(NULL), objectType(NoObject)
      {
      }
  };

}

using namespace DbMetaConfigModelPrivate;

//---------------------------------------------------------------------------
//                       DbMetaConfigModel Implementation
//---------------------------------------------------------------------------

DbMetaConfigModel::DbMetaConfigModel(boost::shared_ptr< SmModel > smModel,
    boost::shared_ptr< IsModel > isModel) :
    mRootNode(NULL), //
    mSmModel(smModel), //
    mIsModel(isModel)
{

} //---------------------------------------------------------------------------

DbMetaConfigModel::~DbMetaConfigModel()
{
}
//---------------------------------------------------------------------------

// Abstract implementation
QVariant
DbMetaConfigModel::headerData(int prmSection,
    Qt::Orientation prmOrientation,
    int prmRole) const
{
  if (prmOrientation == Qt::Vertical || prmRole != Qt::DisplayRole)
    return QVariant();

  switch (prmSection)
  {
    case ColNiceName:
      return "Nice Name";

    case ColSamsMeta:
      return "Sams Meta";

    case ColDbMetaConfig:
      return "Info Schema";

    case ColIgnore:
      return "Ignore";

    case ColInheritorRelatesTo:
      return "Inherits / Relates To";

    case ColExport:
      return "Export";

    case ColPrimaryKey:
      return "PK";

    case ColNullable:
      return "Nullable";

    case ColUserModified:
      return "Inh/Rel User Defined";

    default:
      assert(false);
  }

  return QVariant();
}
//---------------------------------------------------------------------------

QModelIndex
DbMetaConfigModel::index(int row, int column, const QModelIndex& parent) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      row,
  //      column,
  //      reinterpret_cast< uintptr_t > (parent.internalPointer()));

  // Special Case  - If the parent is the root
  if (parent == QModelIndex())
    return createIndex(row, column, mRootNode->children.at(row));

  DbMetaConfigModelPrivate::Node& parentNode = Node(parent);
  return createIndex(row, column, parentNode.children.at(row));
}
//---------------------------------------------------------------------------

QModelIndex
DbMetaConfigModel::parent(const QModelIndex& index) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      index.row(),
  //      index.column(),
  //      reinterpret_cast< uintptr_t > (index.internalPointer()));

  assert(index.isValid());

  DbMetaConfigModelPrivate::Node& node = Node(index);

  // Every node in the model will have a parent
  if (node.parent->object == NULL)
    return QModelIndex();

  return createIndex(node.parent->row, 0, node.parent);
}
//---------------------------------------------------------------------------

int
DbMetaConfigModel::rowCount(const QModelIndex& parent) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      parent.row(),
  //      parent.column(),
  //      reinterpret_cast< uintptr_t > (parent.internalPointer()));

  if (mRootNode == NULL)
    return 0;

  // Special Case  - If the parent is the root
  if (parent == QModelIndex())
    return mRootNode->children.size();

  return Node(parent).children.size();
}
//---------------------------------------------------------------------------

int
DbMetaConfigModel::columnCount(const QModelIndex& parent) const
{
  return ColCount;
}
//---------------------------------------------------------------------------

Qt::ItemFlags
DbMetaConfigModel::flags(const QModelIndex &index) const
{
  Qt::ItemFlags f = QAbstractItemModel::flags(index);

  const DbMetaConfigModelPrivate::Node& node = Node(index);

  if (node.objectType == Schema && !mTableNodes.empty())
    return f;
  //
  //  if (node.objectType == Table && !mColumnNodes.empty())
  //    return f;

  switch (index.column())
  {
    case ColIgnore:
      f |= Qt::ItemIsUserCheckable;
      f |= Qt::ItemIsEditable;
      break;

    case ColExport:
    {
      if (node.objectType == Table || node.objectType == Column)
      {
        f |= Qt::ItemIsUserCheckable;
        f |= Qt::ItemIsEditable;
      }
      break;
    }

    case ColPrimaryKey:
    {
      if (node.objectType == Column)
      {
        f |= Qt::ItemIsUserCheckable;
        f |= Qt::ItemIsEditable;
      }
      break;
    }

    case ColNullable:
    {
      if (node.objectType == Column)
      {
        f |= Qt::ItemIsUserCheckable;
        f |= Qt::ItemIsEditable;
      }
      break;
    }

    case ColUserModified:
    {
      if (node.objectType == Table || node.objectType == Column)
      {
        f |= Qt::ItemIsUserCheckable;
        f |= Qt::ItemIsEditable;
      }
      break;
    }

    case ColNiceName:
      f |= Qt::ItemIsEditable;
      break;

    case ColSamsMeta:
      f |= Qt::ItemIsEditable;
      break;

    case ColInheritorRelatesTo:
      if (node.objectType == Table || node.objectType == Column)
        f |= Qt::ItemIsEditable;
      break;
  }

  return f;
}
//---------------------------------------------------------------------------

QVariant
DbMetaConfigModel::data(const QModelIndex& index, int role) const
{
  // We only provide certain data.
  if (!index.isValid() //
  || !(0 <= index.column() && index.column() < ColCount))
  {
    return QVariant();
  }

  const DbMetaConfigModelPrivate::Node& node = Node(index);
  switch (node.objectType)
  {
    case Schema:
    {
      SchemaMatches::Match * match = *boost::get< SchemaMatches::Match* >(node.object);
      return data(match, index, role);
      break;
    }
    case Table:
    {
      TableMatches::Match * match = *boost::get< TableMatches::Match* >(node.object);
      return data(match, index, role);
      break;
    }
    case Column:
    {
      ColumnMatches::Match * match = *boost::get< ColumnMatches::Match* >(node.object);
      return data(match, index, role);
      break;
    }
    case NoObject:
      assert(false);
      break;
  }
  assert(false);
  return QVariant();
}
//---------------------------------------------------------------------------

QVariant
DbMetaConfigModel::data(SchemaMatches::Match* match,
    const QModelIndex& index,
    int role) const
{
  switch (role)
  {
    case Qt::ForegroundRole:
    {
      if (match->isSchema.get() == NULL //
      || match->smSchema.get() == NULL)
      {
        return QBrush(QColor("Red"));
      }
      return QVariant();
    }

    case Qt::NeedsAttentionRole:
    {
      return (match->isSchema.get() == NULL //
      || match->smSchema.get() == NULL);
    }
  }

  switch (index.column())
  {
    case ColNiceName:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case Qt::EditRole:
          return QString((
              match->smSchema.get() == NULL ? "" :
                  match->smSchema->niceName().c_str()));
          break;
      } // END switch (role)
      break;
    }

    case ColSamsMeta:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case Qt::EditRole:
          return QString((
              match->smSchema.get() == NULL ? "" :
                  match->smSchema->name().c_str()));
          break;
        case Qt::ComboOptionsRole:
        {
          QStringList list;
          list.push_back("");
          for (SmSchema::ListT::const_iterator itr = mSmModel->Schemas().begin(); //
          itr != mSmModel->Schemas().end(); //
              ++itr)
              {
            list.push_back((*itr)->name().c_str());
          }

          return list;
          break;
        }
      } // END switch (role)
      break;
    }

    case ColDbMetaConfig:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case Qt::EditRole:
          return QString((
              match->isSchema.get() == NULL ? "" :
                  match->isSchema->name().c_str()));
          break;
      }
      break;
    }

    case ColIgnore:
    {
      switch (role)
      {
        case Qt::ToolTipRole:
        case Qt::CheckStateRole:
          return (match->ignore ? Qt::Checked : Qt::Unchecked);
          break;
      }
    }
      break;

    case ColInheritorRelatesTo:
      break;

    case ColCount:
      assert(false);
      break;
  } // END switch (index.column())

  return QVariant();
}
//---------------------------------------------------------------------------

QVariant
DbMetaConfigModel::data(TableMatches::Match* match,
    const QModelIndex& index,
    int role) const
{
  const bool needsAttention = match->isTable.get() == NULL //
  || match->smTable.get() == NULL //
  || match->isTable->displayName() != match->smTable->compareName();

  switch (role)
  {
    case Qt::ForegroundRole:
    {
      if (needsAttention)
      {
        return QBrush(QColor("Red"));
      }
      return QVariant();
    }

    case Qt::NeedsAttentionRole:
    {
      return needsAttention;
    }
  }

  switch (index.column())
  {
    case ColNiceName:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case Qt::EditRole:
          return QString((
              match->smTable.get() == NULL ? "" :
                  match->smTable->niceName().c_str()));
          break;
      } // END switch (role)
      break;
    }

    case ColSamsMeta:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case Qt::EditRole:
          return QString((
              match->smTable.get() == NULL ? "" :
                  match->smTable->name().c_str()));
          break;
        case Qt::ComboOptionsRole:
        {
          QStringList list;
          list.push_back("");
          for (SmTable::ListT::const_iterator itr = mSmModel->Tables().begin(); //
          itr != mSmModel->Tables().end(); //
              ++itr)
              {
            SmTable::PtrT smTable = *itr;
            // Only for this schema
            if (smTable->schema()->name() == match->isTable->schema()->name())
              list.push_back(smTable->name().c_str());
          }

          return list;
          break;
        }
      }
      break;
    } // END switch (role)

    case ColDbMetaConfig:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case Qt::EditRole:
          return QString((
              match->isTable.get() == NULL ? "" :
                  match->isTable->displayName().c_str()));
          break;
      }
      break;
    }

    case ColIgnore:
    {
      switch (role)
      {
        case Qt::ToolTipRole:
        case Qt::CheckStateRole:
          return (match->ignore ? Qt::Checked : Qt::Unchecked);
          break;
      }
      break;
    }

    case ColUserModified:
    {
      switch (role)
      {
        case Qt::ToolTipRole:
        case Qt::CheckStateRole:
        {
          if (match->smTable.get() == NULL)
            return false;
          return (
              match->smTable->isInheritsUserDefined() ? Qt::Checked :
                  Qt::Unchecked);
          break;
        }
      }
      break;
    }

    case ColInheritorRelatesTo:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case Qt::EditRole:
          if (match->smTable.get() != NULL //
          && match->smTable->inherits().get() != NULL)
          {
            return match->smTable->inherits()->fullName().c_str();
          }
          break;
        case Qt::ComboOptionsRole:
        {
          if (match->smSchema.get() == NULL)
            return QVariant();

          QStringList list;
          list.push_back("");

          for (TablesByFullNameMapT::const_iterator itr = mTablesByFullName.begin(); //
          itr != mTablesByFullName.end(); ++itr)
            list.push_back(itr->first.c_str());

          list.sort();
          return list;
          break;
        }
      }
      break;
    }

    case ColExport:
    {
      switch (role)
      {
        case Qt::ToolTipRole:
        case Qt::CheckStateRole:
        {
          if (match->smTable.get() == NULL)
            return QVariant();
          return (
              match->smTable->generateStorable() ? Qt::Checked : Qt::Unchecked);
          break;
        }
      }
      break;
    }

    case ColPrimaryKey:
    {
      break;
    }

    case ColNullable:
    {
      break;
    }

    case ColCount:
      assert(false);
  } // END switch (index.column())

  return QVariant();
}
//---------------------------------------------------------------------------

QVariant
DbMetaConfigModel::data(ColumnMatches::Match* match,
    const QModelIndex& index,
    int role) const
{

  bool needsAttention = false;
  if (match->isColumn.get() == NULL || match->smColumn.get() == NULL)
  {
    needsAttention = true;
  }
  else if (match->smColumn->isUserModified())
  {
    needsAttention = match->isColumn->userModifiedComapareString()
        != match->smColumn->userModifiedComapareString();
  }
  else
  {
    needsAttention = match->isColumn->DisplayName()
        != match->smColumn->comapareString();
  }

  const bool isExported = match->smColumn.get() != NULL
      && dboc::isExportEnabled(match->smColumn);

  switch (role)
  {
    case Qt::ForegroundRole:
    {
      if (needsAttention)
        return QBrush(QColor("Red"));

      if (!isExported)
        return QBrush(QColor("Light Grey"));

      return QVariant();
    }

    case Qt::NeedsAttentionRole:
    {
      return needsAttention;
    }

    case Qt::FontRole:
    {
      if (match->smColumn.get() != NULL && match->smColumn->isPrimaryKey())
      {
        QFont font;
        font.setBold(true);
        return font;
      }
      else
      {
        break;
      }
    }
  }

  switch (index.column())
  {
    case ColNiceName:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case Qt::EditRole:
          return QString((
              match->smColumn.get() == NULL ? "" :
                  match->smColumn->niceName().c_str()));
          break;
      } // END switch (role)
      break;
    }

    case ColSamsMeta:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
          return QString((
              match->smColumn.get() == NULL ? "" :
                  match->smColumn->displayName().c_str()));

        case Qt::EditRole:
        {
          if (!isExported)
            return QString((
                match->smColumn.get() == NULL ? "" :
                    match->smColumn->name().c_str()));
          break;
        }

        case Qt::ComboOptionsRole:
        {
          QStringList list;
          list.push_back("");
          for (SmColumn::ListT::const_iterator itr = mSmModel->Columns().begin(); //
          itr != mSmModel->Columns().end(); //
              ++itr)
              {
            SmColumn::PtrT smColumn = *itr;
            // Only for this schema
            if (smColumn->table()->name() == match->isColumn->table()->name())
              list.push_back(smColumn->name().c_str());
          }

          return list;
          break;
        }
      }
      break;
    } // END switch (role)

    case ColDbMetaConfig:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case Qt::EditRole:
          return QString((
              match->isColumn.get() == NULL ? "" :
                  match->isColumn->DisplayName().c_str()));
          break;
      }
      break;
    }

    case ColIgnore:
    {
      switch (role)
      {
        case Qt::ToolTipRole:
        case Qt::CheckStateRole:
          return (match->ignore ? Qt::Checked : Qt::Unchecked);
          break;
      }
    }
      break;

    case ColUserModified:
    {
      switch (role)
      {
        case Qt::ToolTipRole:
        case Qt::CheckStateRole:
        {
          if (match->smColumn.get() == NULL)
            return false;
          return (
              match->smColumn->isUserModified() ? Qt::Checked : Qt::Unchecked);
          break;
        }
      }
      break;
    }

    case ColInheritorRelatesTo:
    {
      switch (role)
      {
        case Qt::DisplayRole:
        case Qt::ToolTipRole:
        case Qt::EditRole:
          if (match->smColumn.get() != NULL //
          && match->smColumn->relatesTo().get() != NULL)
          {
            return match->smColumn->relatesTo()->fullName().c_str();
          }
          break;
        case Qt::ComboOptionsRole:
        {
          if (match->smColumn.get() == NULL)
            return QVariant();

          QStringList columnNames;
          columnNames.push_back("");

          for (ColumnsByFullNameMapT::const_iterator itr = mColumnsByFullName.begin(); //
          itr != mColumnsByFullName.end(); ++itr)
          {
            const SmColumn::PtrT& smColumn = itr->second;
            assert(smColumn.get());
            assert(smColumn->table().get());

            if (smColumn->table()->generateStorable() //
            && dboc::isExportEnabled(smColumn))
              columnNames.push_back(smColumn->fullName().c_str());
          }

          columnNames.sort();
          return columnNames;

          break;
        }
      }
      break;
    }

    case ColExport:
    {
      switch (role)
      {
        case Qt::ToolTipRole:
        case Qt::CheckStateRole:
        {
          if (match->smColumn.get() == NULL)
            return QVariant();
          return (
              match->smColumn->isGenerateStorableProperty() ? Qt::Checked :
                  Qt::Unchecked);
          break;
        }
      }
      break;
    }

    case ColPrimaryKey:
    {
      switch (role)
      {
        case Qt::ToolTipRole:
        case Qt::CheckStateRole:
        {
          if (match->smColumn.get() == NULL)
            return QVariant();
          return (match->smColumn->isPrimaryKey() ? Qt::Checked : Qt::Unchecked);
          break;
        }
      }
      break;
    }

    case ColNullable:
    {
      switch (role)
      {
        case Qt::ToolTipRole:
        case Qt::CheckStateRole:
        {
          if (match->smColumn.get() == NULL)
            return QVariant();
          return (match->smColumn->isNullable() ? Qt::Checked : Qt::Unchecked);
          break;
        }
      }
      break;
    }

    case ColCount:
      assert(false);
  } // END switch (index.column())

  return QVariant();
}
//---------------------------------------------------------------------------

bool
DbMetaConfigModel::setData(const QModelIndex &index,
    const QVariant &value,
    int role)
{
  const DbMetaConfigModelPrivate::Node& node = Node(index);
  switch (node.objectType)
  {
    case Schema:
    {
      SchemaMatches::Match * match = *boost::get< SchemaMatches::Match* >(node.object);
      return setData(match, index, value, role);
      break;
    }

    case Table:
    {
      TableMatches::Match * tableMatch = *boost::get< TableMatches::Match* >(node.object);
      return setData(tableMatch, index, value, role);
      break;
    }

    case Column:
    {
      ColumnMatches::Match * match = *boost::get< ColumnMatches::Match* >(node.object);
      return setData(match, index, value, role);
      break;
    }
    case NoObject:
      assert(false);
      break;
  }
  return false;
}
//---------------------------------------------------------------------------

bool
DbMetaConfigModel::setData(SchemaMatches::Match* match,
    const QModelIndex &index,
    const QVariant &value,
    int role)
{
  switch (index.column())
  {
    case ColIgnore:
    {
      match->ignore = (value.toUInt() == Qt::Checked);
      return true;
    }
    case ColNiceName:
    {
      if (match->smSchema.get())
      {
        match->smSchema->setNiceName(value.toString().toStdString());
        return true;
      }
      return false;
    }
    case ColSamsMeta:
    {
      for (SmSchema::ListT::const_iterator itr = mSmModel->Schemas().begin(); //
      itr != mSmModel->Schemas().end(); //
          ++itr)
          {
        if ((*itr)->name() == value.toString().toStdString())
        {
          match->smSchema = *itr;
          return true;
        }
      }
      return false;
    }

    case ColInheritorRelatesTo:
      return false;

  }

  return false;
}
//---------------------------------------------------------------------------

bool
DbMetaConfigModel::setData(TableMatches::Match* match,
    const QModelIndex &index,
    const QVariant &value,
    int role)
{
  switch (index.column())
  {
    case ColIgnore:
    {
      match->ignore = (value.toUInt() == Qt::Checked);
      return true;
    }
    case ColExport:
    {
      if (match->smTable.get() == NULL)
        return false;

      if (value.toUInt() == Qt::Checked)
        match->smTable->setGenerateStorable(value.toUInt() == Qt::Checked);
      else
        match->smTable->unExport();

      return true;
    }

    case ColUserModified:
    {
      if (match->smTable.get() == NULL)
        return false;

      match->smTable->setInheritsUserDefined(value.toUInt() == Qt::Checked);
      return true;
    }
    case ColNiceName:
    {
      if (match->smTable.get())
      {
        match->smTable->setNiceName(value.toString().toStdString());
        return true;
      }
      return false;
    }
    case ColSamsMeta:
    {
      for (SmTable::ListT::const_iterator itr = mSmModel->Tables().begin(); //
      itr != mSmModel->Tables().end(); //
          ++itr)
          {
        SmTable::PtrT smTable = *itr;
        // Only for this schema
        if (smTable->schema()->name() == match->isTable->schema()->name() //
        && smTable->name() == value.toString().toStdString())
        {
          match->smTable = *itr;
          return true;
        }
      }
      return false;
    }

    case ColInheritorRelatesTo:
    {
      if (match->smTable.get() == NULL)
        return false;

      std::string strVal = value.toString().toStdString();
      if (strVal.empty())
      {
        match->smTable->setInherits(SmTable::PtrT());
      }
      else
      {
        TablesByFullNameMapT::iterator itr = mTablesByFullName.find(strVal);

        if (itr == mTablesByFullName.end())
          return false;

        match->smTable->setInherits(itr->second);
      }
      match->smTable->setInheritsUserDefined(true);
      assert(mSmModel->save());
      return true;
    }

  }

  return false;
}
//---------------------------------------------------------------------------

bool
DbMetaConfigModel::setData(ColumnMatches::Match* match,
    const QModelIndex &index,
    const QVariant &value,
    int role)
{
  switch (index.column())
  {
    case ColIgnore:
    {
      match->ignore = (value.toUInt() == Qt::Checked);
      return true;
    }

    case ColUserModified:
    {
      if (match->smColumn.get() == NULL)
        return false;

      match->smColumn->setUserModified(value.toUInt() == Qt::Checked);
      return true;
    }

    case ColExport:
    {
      if (match->smColumn.get() == NULL)
        return false;

      match->smColumn->setGenerateStorableProperty(value.toUInt()
          == Qt::Checked);
      return true;
    }

    case ColPrimaryKey:
    {
      if (match->smColumn.get() == NULL)
        return false;

      match->smColumn->setPrimaryKey(value.toUInt() == Qt::Checked);
      match->smColumn->setUserModified(true);

      return true;
    }

    case ColNullable:
    {
      if (match->smColumn.get() == NULL)
        return false;

      match->smColumn->setNullable(value.toUInt() == Qt::Checked);
      match->smColumn->setUserModified(true);

      return true;
    }

    case ColNiceName:
    {
      if (match->smColumn.get())
      {
        match->smColumn->setNiceName(value.toString().toStdString());
        return true;
      }
      return false;
    }

    case ColSamsMeta:
    {
      for (SmColumn::ListT::const_iterator itr = mSmModel->Columns().begin(); //
      itr != mSmModel->Columns().end(); //
          ++itr)
          {
        SmColumn::PtrT smColumn = *itr;
        // Only for this schema
        if (smColumn->table()->name() == match->isColumn->table()->name() //
        && smColumn->name() == value.toString().toStdString())
        {
          match->smColumn = *itr;
          return true;
        }
      }
      return false;
    }

    case ColInheritorRelatesTo:
    {
      if (match->smColumn.get() == NULL)
        return false;

      std::string strVal = value.toString().toStdString();
      if (strVal.empty())
      {
        match->smColumn->setRelatesTo(SmColumn::PtrT());
      }
      else
      {
        ColumnsByFullNameMapT::iterator itr = mColumnsByFullName.find(strVal);

        if (itr == mColumnsByFullName.end())
          return false;

        match->smColumn->setRelatesTo(itr->second);
      }

      match->smColumn->setUserModified(true);
      assert(mSmModel->save());
      return true;
    }

  }

  return false;
}
//---------------------------------------------------------------------------

DbMetaConfigModelPrivate::Node&
DbMetaConfigModel::Node(const QModelIndex& index) const
{
  if (!index.isValid())
    assert(false);

  DbMetaConfigModelPrivate::Node * obj = static_cast< DbMetaConfigModelPrivate::Node* >(index.internalPointer());obj
  = dynamic_cast< DbMetaConfigModelPrivate::Node* >(obj);
  assert(obj);
  return *obj;
}
//---------------------------------------------------------------------------

void
DbMetaConfigModel::setupModel(SchemaMatches& matcher)
{
  SchemaMatches::ListT& matches = matcher.Matches();

  mRootNode = &mNodes[SmVariantT()];
  qDebug("mNodes.size() = %d", mNodes.size());

  for (SchemaMatches::ListT::iterator itr = matches.begin(); //
  itr != matches.end(); ++itr)
  {
    SchemaMatches::Match& match = *itr;
    SmVariantT variant = &match;

    DbMetaConfigModelPrivate::Node* node = &mNodes[variant];
    qDebug("(SmSchema, node) mNodes.size() = %d", mNodes.size());

    // Set the parent
    mRootNode->children.push_back(node);
    node->parent = mRootNode;
    node->row = mRootNode->children.size() - 1;

    NodeMapT::iterator mapItr = mNodes.find(variant);
    node->object = const_cast< SmVariantT* >(&mapItr->first);
    node->objectType = Schema;

    mSchemaNodes.insert(std::make_pair(match.smSchema, node));
  }
}
//---------------------------------------------------------------------------

void
DbMetaConfigModel::setupModel(TableMatches& matcher)
{
  TableMatches::ListT& matches = matcher.Matches();

  for (TableMatches::ListT::iterator itr = matches.begin(); //
  itr != matches.end(); ++itr)
  {
    TableMatches::Match& match = *itr;
    SmVariantT variant = &match;

    DbMetaConfigModelPrivate::Node* node = &mNodes[variant];

    DbMetaConfigModelPrivate::Node* parentNode = NULL;
    { // Scope the variables
      SchemaNodeMapT::iterator schemaFindItr = mSchemaNodes.find(match.smSchema);
      assert(schemaFindItr != mSchemaNodes.end());
      parentNode = schemaFindItr->second;
    }

    // Set the parent
    parentNode->children.push_back(node);
    node->parent = parentNode;
    node->row = parentNode->children.size() - 1;

    NodeMapT::iterator mapItr = mNodes.find(variant);
    node->object = const_cast< SmVariantT* >(&mapItr->first);
    node->objectType = Table;

    mTableNodes.insert(std::make_pair(match.smTable, node));

  }

  for (SmTable::ListT::const_iterator itr = mSmModel->Tables().begin(); //
  itr != mSmModel->Tables().end(); ++itr)
  {
    SmTable::PtrT smTable = *itr;
    mTablesByFullName.insert(std::make_pair(smTable->fullName(), smTable));
  }
}
//---------------------------------------------------------------------------

void
DbMetaConfigModel::setupModel(ColumnMatches& matcher)
{
  typedef std::multimap< SmTable::PtrT, ColumnMatches::Match* > MatchMapT;
  MatchMapT matchMap;

  ColumnMatches::ListT& matches = matcher.Matches();
  for (ColumnMatches::ListT::iterator itr = matches.begin(); //
  itr != matches.end(); ++itr)
  {
    ColumnMatches::Match & match = *itr;
    matchMap.insert(std::make_pair(match.smTable, &match));
  }

// Keep the last table and node we used, the columns should be ordered by table
  SmTable::PtrT smTable;
  DbMetaConfigModelPrivate::Node* parentNode;

  for (MatchMapT::iterator itr = matchMap.begin(); //
  itr != matchMap.end(); ++itr)
  {
    ColumnMatches::Match* match = itr->second;
    SmVariantT variant = match;
    DbMetaConfigModelPrivate::Node* node = &mNodes[variant];

    if (match->smTable != smTable)
    {
      TableNodeMapT::iterator tableFindItr = mTableNodes.find(itr->first);
      assert(tableFindItr != mTableNodes.end());
      parentNode = tableFindItr->second;
      smTable = itr->first;
    }

    // Set the parent
    parentNode->children.push_back(node);
    node->parent = parentNode;
    node->row = parentNode->children.size() - 1;

    NodeMapT::iterator mapItr = mNodes.find(variant);
    node->object = const_cast< SmVariantT* >(&mapItr->first);
    node->objectType = Column;

    mColumnNodes.insert(std::make_pair(match->smColumn, node));

  }

  for (SmColumn::ListT::const_iterator itr = mSmModel->Columns().begin(); //
  itr != mSmModel->Columns().end(); ++itr)
  {
    SmColumn::PtrT smColumn = *itr;
    mColumnsByFullName.insert(std::make_pair(smColumn->fullName(), smColumn));
  }

}
//---------------------------------------------------------------------------
