#ifndef INFOSCHEMA_H
#define INFOSCHEMA_H

#include "ui_DbMetaConfig.h"

#include <map>

#include <boost/shared_ptr.hpp>

#include <QtCore/qobject.h>

#include "SchemaMatches.h"
#include "TableMatches.h"
#include "ColumnMatches.h"

class SmModel;
class IsModel;
class DbMetaConfigModel;

class SmSchema;
class IsSchema;

class DbMetaConfig : public QObject
{
  Q_OBJECT

  public:
    DbMetaConfig(QWidget *parent = 0);
    virtual
    ~DbMetaConfig();

    void Reset();

  public slots:
    void
    DoGotoNextIssue();

    void
    DoSaveSchemas();

    void
    DoSaveTables();

    void
    DoSaveColumns();

    void
    DoSave();

    void
    DoLoad();

  private:
    void
    Indexes(QModelIndexList& indexes, QModelIndex parent);

    void
    loadFromQsqMetadata(const std::string defaultSchema);

    Ui::DbMetaConfigClass ui;

    boost::shared_ptr< SmModel > mSmModel;
    boost::shared_ptr< IsModel > mIsModel;
    boost::shared_ptr< DbMetaConfigModel > mQtModel;

    SchemaMatches mSchemaMatches;
    TableMatches mTableMatches;
    ColumnMatches mColumnMatches;

};

#endif // INFOSCHEMA_H
