/*
 * SchemaMatches.cpp
 *
 *  Created on: Feb 4, 2011
 *      Author: Jarrod Chesney
 */

#include "SchemaMatches.h"

#include "SmOrmModel.h"
#include "IsOrmModel.h"

#include "SmSchema.h"
#include "IsSchema.h"

SchemaMatches::Match::Match(SmSchemaPtrT smSchema_, IsSchemaPtrT isSchema_) :
  smSchema(smSchema_), isSchema(isSchema_), ignore(false)
{
}
SchemaMatches::Match::Match() :
  ignore(false)
{
}

SchemaMatches::Match&
SchemaMatches::Match::operator=(const Match& other)
{
  if (this == &other)
    return *this;

  isSchema = other.isSchema;
  smSchema = other.smSchema;
  ignore = other.ignore;

  return *this;
}

bool
SchemaMatches::Match::operator<(const Match& other) const
{
  if (isSchema != other.isSchema)
    return isSchema < other.isSchema;

  if (smSchema != other.smSchema)
    return smSchema < other.smSchema;

  return ignore < other.ignore;
}

SchemaMatches::SchemaMatches(boost::shared_ptr< SmModel >& smModel,
    boost::shared_ptr< IsModel >& isModel) :
  mSmModel(smModel), //
      mIsModel(isModel)
{
}

SchemaMatches::~SchemaMatches()
{
}

void
SchemaMatches::Compare()
{
  mMaches.clear();
  typedef std::set< std::string > StrSetT;
  StrSetT matchedSchemaNames;

  for (SmSchema::ListT::const_iterator itr = mSmModel->Schemas().begin();//
  itr != mSmModel->Schemas().end(); ++itr)
  {
    SmSchema::PtrT smSchema = *itr;
    // find schema
    IsSchema::PtrT isSchema = mIsModel->Schema(smSchema->name());
    mMaches.push_back(Match(smSchema, isSchema));

    if (isSchema.get() != NULL)
      matchedSchemaNames.insert(smSchema->name());
  }

  for (IsSchema::ListT::const_iterator itr = mIsModel->Schemas().begin(); itr
      != mIsModel->Schemas().end(); ++itr)
  {
    IsSchema::PtrT isSchema = *itr;

    if (matchedSchemaNames.find(isSchema->name()) == matchedSchemaNames.end())
      mMaches.push_back(Match(SmSchema::PtrT(), isSchema));
  }
}

void
SchemaMatches::Save()
{
  for (ListT::iterator itr = mMaches.begin(); itr != mMaches.end(); ++itr)
  {
    Match& match = *itr;

    if (match.ignore)
      continue;

    // New Schmea
    if (match.smSchema.get() == NULL)
    {
      assert (match.isSchema.get() != NULL);
      SmSchema::PtrT newSchema(new SmSchema());
      newSchema->setId(mSmModel->NextMetaRecordId());
      newSchema->setName(match.isSchema->name());
      mSmModel->addSchema(newSchema);
      continue;
    }

    // Delete Schmea
    if (match.isSchema.get() == NULL)
    {
      assert (match.smSchema.get() != NULL);
      mSmModel->removeSchema(match.smSchema);
      continue;
    }

    // They match, update if required
    if (match.isSchema->name() != match.smSchema->name())
    {
      match.smSchema->setName(match.isSchema->name());
      continue;
    }
  }

  assert(mSmModel->save());

}

SchemaMatches::ListT&
SchemaMatches::Matches()
{
  return mMaches;
}
