#include "DbMetaConfig.h"

#include <QtGui/qmessagebox.h>

#include "Main.h"
#include "IsOrmModel.h"
#include "SmOrmModel.h"
#include "DbMetaConfigModel.h"
#include "QSqlMetaData.h"
#include "ComboItemDelegate.h"

#include "QtModelUserRoles.h"

DbMetaConfig::DbMetaConfig(QWidget *parent) :
  mSchemaMatches(mSmModel, mIsModel), //
      mTableMatches(mSmModel, mIsModel), //
      mColumnMatches(mSmModel, mIsModel)
{
  ui.setupUi(parent);

  ui.treeView->setItemDelegateForColumn(DbMetaConfigModel::ColSamsMeta,
      new ComboItemDelegate(ui.treeView));

  ui.treeView->setItemDelegateForColumn(DbMetaConfigModel::ColInheritorRelatesTo,
      new ComboItemDelegate(ui.treeView));

  connect(ui.btnLoadAndCompare, SIGNAL(clicked()), this, SLOT(DoLoad()));
  connect(ui.btnNextIssue, SIGNAL(clicked()), this, SLOT(DoGotoNextIssue()));
  connect(ui.btnSaveSchemas, SIGNAL(clicked()), this, SLOT(DoSaveSchemas()));
  connect(ui.btnSaveTables, SIGNAL(clicked()), this, SLOT(DoSaveTables()));
  connect(ui.btnSaveColumns, SIGNAL(clicked()), this, SLOT(DoSaveColumns()));
  connect(ui.btnSave, SIGNAL(clicked()), this, SLOT(DoSave()));

}

DbMetaConfig::~DbMetaConfig()
{

}

void
DbMetaConfig::Reset()
{
  mSmModel.reset(new SmModel(Main::Inst().metaDataDatabaseConnectionSettings()));
  mIsModel.reset(new IsModel(Main::Inst().databaseConnectionSettings()));

  mSchemaMatches.Matches().clear();
  mTableMatches.Matches().clear();
  mColumnMatches.Matches().clear();

  ui.treeView->setModel(NULL);
  mQtModel.reset();

}

void
DbMetaConfig::DoGotoNextIssue()
{
  if (mQtModel.get() == NULL)
    return;

  // Create a depth first list of indexes
  QModelIndexList indexes;
  Indexes(indexes, QModelIndex());

  foreach(QModelIndex index, indexes)
    {
      if (index.data(Qt::NeedsAttentionRole).toBool())
      {
        // Create a reverse list of parents
        QModelIndexList parents;
        for (QModelIndex parent = index; //
        parent != QModelIndex(); parent = parent.parent())
          parents.push_front(parent);

        // Expand the parents in order
        foreach(QModelIndex parent, parents)
            ui.treeView->expand(parent);

        // Select the item
        ui.treeView->selectionModel()->select(index,
            QItemSelectionModel::ClearAndSelect);
      }
    }

}

void
DbMetaConfig::DoLoad()
{
  Main::DbMetaSettings settings = Main::Inst().dbmetaSettings();

  try
  {
    mSmModel.reset(new SmModel(Main::Inst().metaDataDatabaseConnectionSettings()));
    mIsModel.reset(new IsModel(Main::Inst().databaseConnectionSettings()));

    assert(mSmModel->load());

    if (settings.useQsqlMetaData)
      loadFromQsqMetadata(settings.defaultSchema);
    else
      assert(mIsModel->load());

    mSchemaMatches.Compare();

    mQtModel.reset(new DbMetaConfigModel(mSmModel, mIsModel));
    mQtModel->setupModel(mSchemaMatches);
    ui.treeView->setModel(mQtModel.get());
    ui.treeView->resizeColumnToContents(0);
    ui.treeView->resizeColumnToContents(1);
    ui.treeView->resizeColumnToContents(2);
    ui.treeView->resizeColumnToContents(3);
    DoGotoNextIssue();

  }
  catch (std::exception& e)
  {
    QString msg = "An error occured saving the data, error was :\n";
    msg += e.what();
    QMessageBox::critical(ui.btnSave, "Save Failed", msg);
    return;
  }
}

void
DbMetaConfig::DoSaveSchemas()
{
  try
  {
    mSchemaMatches.Save();
    mSchemaMatches.Compare();

    mTableMatches.Compare();

    mQtModel.reset(new DbMetaConfigModel(mSmModel, mIsModel));
    mQtModel->setupModel(mSchemaMatches);
    mQtModel->setupModel(mTableMatches);
    ui.treeView->setModel(mQtModel.get());
    ui.treeView->resizeColumnToContents(0);
    ui.treeView->resizeColumnToContents(1);
    ui.treeView->resizeColumnToContents(2);
    ui.treeView->resizeColumnToContents(3);
    DoGotoNextIssue();

  }
  catch (std::exception& e)
  {
    QString msg = "An error occured saving the data, error was :\n";
    msg += e.what();
    QMessageBox::critical(ui.btnSave, "Save Failed", msg);
    return;
  }
}

void
DbMetaConfig::DoSaveTables()
{
  try
  {
    mTableMatches.Save();
    mTableMatches.Compare();

    mColumnMatches.Compare();

    mQtModel.reset(new DbMetaConfigModel(mSmModel, mIsModel));
    mQtModel->setupModel(mSchemaMatches);
    mQtModel->setupModel(mTableMatches);
    mQtModel->setupModel(mColumnMatches);
    ui.treeView->setModel(mQtModel.get());
    ui.treeView->resizeColumnToContents(0);
    ui.treeView->resizeColumnToContents(1);
    ui.treeView->resizeColumnToContents(2);
    ui.treeView->resizeColumnToContents(3);
    DoGotoNextIssue();
  }
  catch (std::exception& e)
  {
    QString msg = "An error occured saving the data, error was :\n";
    msg += e.what();
    QMessageBox::critical(ui.btnSave, "Save Failed", msg);
    return;
  }
}

void
DbMetaConfig::DoSaveColumns()
{
  try
  {
    mColumnMatches.Save();
    mColumnMatches.Compare();

    mQtModel.reset(new DbMetaConfigModel(mSmModel, mIsModel));
    mQtModel->setupModel(mSchemaMatches);
    mQtModel->setupModel(mTableMatches);
    mQtModel->setupModel(mColumnMatches);
    ui.treeView->setModel(mQtModel.get());
    ui.treeView->resizeColumnToContents(0);
    ui.treeView->resizeColumnToContents(1);
    ui.treeView->resizeColumnToContents(2);
    ui.treeView->resizeColumnToContents(3);
    DoGotoNextIssue();
  }
  catch (std::exception& e)
  {
    QString msg = "An error occured saving the data, error was :\n";
    msg += e.what();
    QMessageBox::critical(ui.btnSave, "Save Failed", msg);
    return;
  }
}

void
DbMetaConfig::DoSave()
{
  try
  {
    assert(mSmModel->save());
  }
  catch (std::exception& e)
  {
    QString msg = "An error occured saving the data, error was :\n";
    msg += e.what();
    QMessageBox::critical(ui.btnSave, "Save Failed", msg);
    return;
  }
}

void
DbMetaConfig::Indexes(QModelIndexList& indexes, QModelIndex parent)
{
  if (mQtModel.get() == NULL)
    return;

  for (int i = 0; i < mQtModel->rowCount(parent); i++)
  {
    QModelIndex index = mQtModel->index(i, 0, parent);
    indexes.push_back(index);
    Indexes(indexes, index);
  }
}

void
DbMetaConfig::loadFromQsqMetadata(const std::string defaultSchema)
{
  assert(mIsModel.get());
  mIsModel->openDb();
  QSqlMetaData sqlMetaData(defaultSchema, mIsModel);
  sqlMetaData.run();
  mIsModel->closeDb();

}
