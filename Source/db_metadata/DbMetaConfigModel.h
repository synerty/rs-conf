/*
 * uHierarchModel.h
 *
 *  Created on: Aug 25, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _INFO_SCHEMA_QT_MODEL_H_
#define _INFO_SCHEMA_QT_MODEL_H_

// Include Std headers
#include <string>
#include <map>
#include <vector>
#include <set>

#include <boost/shared_ptr.hpp>
#include <boost/variant.hpp>
// Include Qt headers
#include <QtCore/qabstractitemmodel.h>

#include "SchemaMatches.h"
#include "TableMatches.h"
#include "ColumnMatches.h"

class IsModel;

class SmModel;
class SmSchema;
class SmTable;
class SmColumn;

namespace DbMetaConfigModelPrivate
{
  class Node;
  typedef boost::variant< SchemaMatches::Match*, TableMatches::Match*,
      ColumnMatches::Match* > SmVariantT;

}

class DbMetaConfigModel : public QAbstractItemModel
{
  public:
    enum TeCol
    {
      ColNiceName,
      ColSamsMeta,
      ColDbMetaConfig,
      ColIgnore,
      ColInheritorRelatesTo,
      ColExport,
      ColPrimaryKey,
      ColNullable,
      ColUserModified,
      ColCount
    };

  public:
    DbMetaConfigModel(boost::shared_ptr< SmModel > smModel,
        boost::shared_ptr< IsModel > isModel);
    virtual
    ~DbMetaConfigModel();

  public:

    void
    setupModel(SchemaMatches& matches);

    void
    setupModel(TableMatches& matches);

    void
    setupModel(ColumnMatches& matches);

  public:
    // Abstract implementation
    QVariant
    headerData(int prmSection,
        Qt::Orientation prmOrientation,
        int prmRole = Qt::DisplayRole) const;

    QModelIndex
    index(int row, int column, const QModelIndex& parent = QModelIndex()) const;
    QModelIndex
    parent(const QModelIndex& index) const;

    int
    rowCount(const QModelIndex& parent = QModelIndex()) const;

    int
    columnCount(const QModelIndex& parent = QModelIndex()) const;

    QVariant
    data(const QModelIndex& index, int prmRole = Qt::DisplayRole) const;

    Qt::ItemFlags
    flags(const QModelIndex &index) const;

    bool
    setData(const QModelIndex &index,
        const QVariant &value,
        int role = Qt::EditRole);

  private:
    /// --------------
    // ObjectModel methods

    QVariant
    data(SchemaMatches::Match* match, const QModelIndex& index, int role) const;

    QVariant
    data(TableMatches::Match* match, const QModelIndex& index, int role) const;

    QVariant
    data(ColumnMatches::Match* match, const QModelIndex& index, int role) const;

    bool
    setData(SchemaMatches::Match* match,
        const QModelIndex &index,
        const QVariant &value,
        int role);

    bool
    setData(TableMatches::Match* match,
        const QModelIndex &index,
        const QVariant &value,
        int role);

    bool
    setData(ColumnMatches::Match* match,
        const QModelIndex &index,
        const QVariant &value,
        int role);

    DbMetaConfigModelPrivate::Node&
    Node(const QModelIndex& index) const;

    typedef std::map< DbMetaConfigModelPrivate::SmVariantT,
        DbMetaConfigModelPrivate::Node > NodeMapT;
    NodeMapT mNodes;

    typedef std::map< boost::shared_ptr< SmSchema >,
        DbMetaConfigModelPrivate::Node* > SchemaNodeMapT;
    SchemaNodeMapT mSchemaNodes;

    typedef std::map< boost::shared_ptr< SmTable >,
        DbMetaConfigModelPrivate::Node* > TableNodeMapT;
    TableNodeMapT mTableNodes;

    typedef std::map< boost::shared_ptr< SmColumn >,
        DbMetaConfigModelPrivate::Node* > ColumnNodeMapT;
    ColumnNodeMapT mColumnNodes;

    typedef std::map< std::string, boost::shared_ptr< SmTable > >
        TablesByFullNameMapT;
    TablesByFullNameMapT mTablesByFullName;

    typedef std::map< std::string, boost::shared_ptr< SmColumn > >
        ColumnsByFullNameMapT;
    ColumnsByFullNameMapT mColumnsByFullName;

    /*! Link to the root node.
     * This node will not be in the model, its equivilent to QModelIndex()
     */
    DbMetaConfigModelPrivate::Node* mRootNode;

    boost::shared_ptr< SmModel > mSmModel;
    boost::shared_ptr< IsModel > mIsModel;

};
// ----------------------------------------------------------------------------


#endif /* _INFO_SCHEMA_QT_MODEL_H_ */
