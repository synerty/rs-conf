/*
 * ColumnMatches.cpp
 *
 *  Created on: Feb 4, 2011
 *      Author: Jarrod Chesney
 */

#include "ColumnMatches.h"

#include "SmOrmModel.h"
#include "IsOrmModel.h"

#include "SmSchema.h"
#include "SmTable.h"
#include "SmColumn.h"
#include "IsTable.h"
#include "IsColumn.h"

ColumnMatches::Match::Match(SmTablePtrT smTable_,
    SmColumnPtrT smColumn_,
    IsColumnPtrT isColumn_) :
  smTable(smTable_), smColumn(smColumn_), isColumn(isColumn_), ignore(false)
{
}
ColumnMatches::Match::Match() :
  ignore(false)
{
}

ColumnMatches::Match&
ColumnMatches::Match::operator=(const Match& other)
{
  if (this == &other)
    return *this;

  smTable = other.smTable;
  isColumn = other.isColumn;
  smColumn = other.smColumn;
  ignore = other.ignore;

  return *this;
}

bool
ColumnMatches::Match::operator<(const Match& other) const
{
  if (smTable != other.smTable)
    return smTable < other.smTable;

  if (isColumn != other.isColumn)
    return isColumn < other.isColumn;

  if (smColumn != other.smColumn)
    return smColumn < other.smColumn;

  return ignore < other.ignore;
}

ColumnMatches::ColumnMatches(boost::shared_ptr< SmModel >& smModel,
    boost::shared_ptr< IsModel >& isModel) :
  mSmModel(smModel), //
      mIsModel(isModel)
{
}

ColumnMatches::~ColumnMatches()
{
}

void
ColumnMatches::Compare()
{
  // NOTE : We assume that all the tables have been matched
  // We use the SmTable as a reference for both types of columns
  mMaches.clear();
  typedef std::set< std::string > StrSetT;

  /// ----
  // Create a map of tables by name
  typedef std::map< std::string, SmTable::PtrT > TableMapT;
  TableMapT tableMap;

  for (SmTable::ListT::const_iterator itr = mSmModel->Tables().begin();//
  itr != mSmModel->Tables().end(); ++itr)
  {
    std::string fullName = (*itr)->schema()->name() + "." + (*itr)->name();
    tableMap.insert(std::make_pair(fullName, *itr));
  }

  /// ----
  // Group the SmColumns by SmTable
  typedef std::multimap< SmTable::PtrT, SmColumn::PtrT > SmColumnMapT;
  SmColumnMapT smColumnMap;

  for (SmColumn::ListT::const_iterator itr = mSmModel->Columns().begin();//
  itr != mSmModel->Columns().end(); ++itr)
    smColumnMap.insert(std::make_pair((*itr)->table(), *itr));

  /// ----
  // Group the IsColumns by SmTable
  typedef std::map< SmTable::PtrT, IsModel::ColumnMapT > IsColumnMapBySmTableT;
  IsColumnMapBySmTableT isColumnBySmTable;

  for (IsColumn::ListT::const_iterator itr = mIsModel->Columns().begin();//
  itr != mIsModel->Columns().end(); ++itr)
  {
    IsColumn::PtrT isColumn = *itr;
    assert(isColumn.get() != NULL);
    assert(isColumn->table().get() != NULL);

    std::string tableFullName = isColumn->table()->fullName();

    TableMapT::iterator tableItr = tableMap.find(tableFullName);
    assert(tableItr != tableMap.end());

    // This will create the entry if needed
    IsModel::ColumnMapT
        & isColumnByNameMap = isColumnBySmTable[tableItr->second];
    // Insert into the submap
    isColumnByNameMap.insert(std::make_pair(isColumn->name(), isColumn));
  }

  /// ----
  // Now, for each table, compare the columns
  for (SmTable::ListT::const_iterator smTableItr = mSmModel->Tables().begin();//
  smTableItr != mSmModel->Tables().end(); ++smTableItr)
  {
    SmTable::PtrT smTable = *smTableItr;
    StrSetT matchedColumnNames;

    // Get a ranget of SmColumns
    std::pair< SmColumnMapT::iterator, SmColumnMapT::iterator > smColumnRange;
    smColumnRange = smColumnMap.equal_range(smTable);

    // Get the map of IsColumns for this table
    // It may or may not exist, so just get it this way
    IsModel::ColumnMapT& isColumnsByName = isColumnBySmTable[smTable];

    /// ----
    // Compare SmColumns against IsColumns for this SmTable
    for (SmColumnMapT::const_iterator smColumnItr = smColumnRange.first;//
    smColumnItr != smColumnRange.second; ++smColumnItr)
    {
      SmColumn::PtrT smColumn = smColumnItr->second;

      // Find IsColumn with same name
      IsColumn::PtrT isColumn;

      { // Variable scope ( too many iterators around :-| )
        IsModel::ColumnMapT::iterator
            isColumnItr = isColumnsByName.find(smColumn->name());

        // If we find the column, assign it.
        if (isColumnItr != isColumnsByName.end())
          isColumn = isColumnItr->second;
      }

      mMaches.push_back(Match(smTable, smColumn, isColumn));

      if (isColumn.get() != NULL)
        matchedColumnNames.insert(smColumn->name());
    }

    // Add all the IsColumns that didn't have matching SmColumns for this table
    for (IsModel::ColumnMapT::const_iterator itr = isColumnsByName.begin(); //
    itr != isColumnsByName.end(); ++itr)
    {
      IsColumn::PtrT isColumn = itr->second;
      if (matchedColumnNames.find(isColumn->name()) == matchedColumnNames.end())
        mMaches.push_back(Match(smTable, SmColumn::PtrT(), isColumn));
    }
  }
}

void
ColumnMatches::Save()
{
  typedef std::map< const std::string, SmColumn::PtrT > SmColumnMapT;
  SmColumnMapT smColumnMap;

  for (SmColumn::ListT::const_iterator itr = mSmModel->Columns().begin();//
  itr != mSmModel->Columns().end(); ++itr)
    smColumnMap.insert(std::make_pair((*itr)->fullName(), *itr));

  for (ListT::iterator itr = mMaches.begin(); itr != mMaches.end(); ++itr)
  {
    Match& match = *itr;

    if (match.ignore)
      continue;

    // New Column
    if (match.smColumn.get() == NULL)
    {
      assert(match.isColumn.get() != NULL);
      SmColumn::PtrT newColumn(new SmColumn());
      newColumn->setId(mSmModel->NextMetaRecordId());
      newColumn->setName(match.isColumn->name());

      newColumn->setDataType(match.isColumn->ConvertedDataType());
      newColumn->setNullable(match.isColumn->isNullable());

      if (!match.isColumn->isDefaultValueNull())
        newColumn->setDefaultValue(match.isColumn->defaultValue());

      if (match.isColumn->isDataLengthNull())
        newColumn->setDataLengthNull();
      else
        newColumn->setDataLength(match.isColumn->dataLength());

      if (match.isColumn->relatesTo().get() != NULL)
      {
        // I don't care if it doesn't exist and creates a new entry
        // It will just have to be compared/saved twice.
        newColumn->setRelatesTo(smColumnMap[match.isColumn->relatesTo()->fullName()]);
      }

      newColumn->setPrimaryKey(match.isColumn->isPrimaryKey());

      newColumn->setTable(match.smTable);
      mSmModel->addColumn(newColumn);
      continue;
    }

    // Delete Column
    if (match.isColumn.get() == NULL)
    {
      assert(match.smColumn.get() != NULL);
      mSmModel->removeColumn(match.smColumn);
      continue;
    }

    bool updateRequired = false;
    if (match.smColumn->isUserModified())
    {
      updateRequired = match.isColumn->userModifiedComapareString()
          != match.smColumn->userModifiedComapareString();
    }
    else
    {
      updateRequired = match.isColumn->DisplayName()
          != match.smColumn->comapareString();
    }

    // They match, update if required
    if (updateRequired)
    {
      match.smColumn->setName(match.isColumn->name());
      match.smColumn->setDataType(match.isColumn->ConvertedDataType());
      match.smColumn->setNullable(match.isColumn->isNullable());

      if (match.isColumn->isDefaultValueNull())
        match.smColumn->setDefaultValueNull();
      else
        match.smColumn->setDefaultValue(match.isColumn->defaultValue());

      if (match.isColumn->isDataLengthNull())
        match.smColumn->setDataLengthNull();
      else
        match.smColumn->setDataLength(match.isColumn->dataLength());

      if (!match.smColumn->isUserModified())
      {
        if (match.isColumn->relatesTo().get() == NULL)
        {
          match.smColumn->setRelatesTo(SmColumn::PtrT());
        }
        else
        {
          // I don't care if it doesn't exist and creates a new entry
          // It will just have to be compared/saved twice.
          match.smColumn->setRelatesTo(smColumnMap[match.isColumn->relatesTo()->fullName()]);
        }

        match.smColumn->setPrimaryKey(match.isColumn->isPrimaryKey());
      }

      continue;
    }
  }

  assert(mSmModel->save());

}

ColumnMatches::ListT&
ColumnMatches::Matches()
{
  return mMaches;
}
