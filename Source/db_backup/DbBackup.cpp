/*
 * DbBackup.cpp
 *
 *  Copyright NG SCADA Systems Pty Ltd 2012
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#include "DbBackup.h"

#include <stdexcept>

#include <boost/shared_ptr.hpp>

#include <QtCore/qprocess.h>
#include <QtCore/qstring.h>
#include <QtCore/qstringlist.h>
#include <QtCore/qdir.h>

#include <orm/v1/Conn.h>

#include "Main.h"

static const std::string PG_BACKUP_EXE = ""
  "C:\\Program Files (x86)\\PostgreSQL\\9.0\\bin\\pg_dump.exe";

DbBackup::DbBackup()
{
}

DbBackup::~DbBackup()
{
}

void
DbBackup::run()
{
  orm::v1::Conn conn = Main::Inst().metaDataDatabaseConnectionSettings();

  Main::DbocSettings settings = Main::Inst().dbocSettings();
  Main::DbMetaSettings dbMetaSettings = Main::Inst().dbmetaSettings();

  QString path = QString(settings.path.c_str());
  path += "\\..\\..";
  path += "\\db";
  path = QDir::cleanPath(path);

  if (!QDir().mkpath(path))
    throw std::runtime_error(std::string("Unable to make path ")
        + path.toStdString());


  path += "\\rsconf_metadata.sql";
  path = QDir::cleanPath(path);
  mDumpFile = path.toStdString();


  QStringList args;

  args << "--host" << QString(conn.Host.c_str());
  args << "--port" << QString::number(conn.Port);
  args << "--username" << QString(conn.Username.c_str());
  args << "--format" << "plain";
  args << "--verbose";
  args << "--file" << path;
  args << "--schema" << "sams_meta";
  args << QString(conn.Database.c_str());

  boost::shared_ptr< QObject > obj(new QObject());
  boost::shared_ptr< QProcess > proc(new QProcess(obj.get()));
  proc->start(QString(PG_BACKUP_EXE.c_str()), args);
  proc->waitForFinished();
  mOutput = QString(proc->readAllStandardOutput()).toStdString();
  mError = QString(proc->readAllStandardError()).toStdString();

}

std::string
DbBackup::dumpFile() const
{
  return mDumpFile;
}

std::string
DbBackup::output() const
{
  return mOutput;
}

std::string
DbBackup::error() const
{
  return mError;
}
