/*
 * DbBackup.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2012
 *
 *  This software is propriatory, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef DBBACKUP_H_
#define DBBACKUP_H_

#include <string>

/*! DbBackup Brief Description
 * Long comment about the class
 *
 */
class DbBackup
{
  public:
    DbBackup();
    virtual
    ~DbBackup();

    void run();

    std::string dumpFile() const;
    std::string output() const;
    std::string error() const;

  private:
    std::string mDumpFile;
    std::string mOutput;
    std::string mError;


};

#endif /* DBBACKUP_H_ */
