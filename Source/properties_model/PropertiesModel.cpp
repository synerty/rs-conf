/*
 * uHierarchModel.cpp
 *
 *  Created on: Aug 25, 2010
 *      Author: Jarrod Chesney
 */

#include "PropertiesModel.h"

#include <assert.h>
#include <list>
#include <algorithm>

#include <QtGui/qcolor.h>
#include <QtGui/qbrush.h>

#include "Main.h"

#include "orm/v1/MvLink.h"
#include "orm/v1/MvLinkStandardValue.h"
#include "orm/v1/DynamicStorable.h"

#include "SmTable.h"
#include "SmColumn.h"
#include "SmOrmModel.h"

using namespace orm::v1;
using namespace PropertiesModelPrivate;

//---------------------------------------------------------------------------
//                           PropertiesModel private
//---------------------------------------------------------------------------
namespace PropertiesModelPrivate
{

  enum SmObjectTypeE
  {
    NoObject,
    OrmTable,
    OrmField
  };

  struct Node
  {
      PropertiesModelPrivate::Node* parent;
      int row;
      SmVariantT* object;
      SmObjectTypeE objectType;
      typedef std::vector< PropertiesModelPrivate::Node* > ChildrenT;
      ChildrenT children;
      std::string name;

      Node() :
        parent(NULL), row(0), object(NULL), objectType(NoObject)
      {
      }
  };

}

template<typename TypeT>
  boost::optional< TypeT >
  defaultIntValue(std::string strVal)
  {
    boost::optional< TypeT > defaultValue;
    if (!strVal.empty())
    {
      TypeT intVal = strtoll(strVal.c_str(), NULL, 10);
      defaultValue = intVal;
    }
    return defaultValue;
  }

template<typename TypeT>
  boost::optional< TypeT >
  defaultRealValue(std::string strVal)
  {
    boost::optional< TypeT > defaultValue;
    if (!strVal.empty())
    {
      TypeT intVal = strtod(strVal.c_str(), NULL);
      defaultValue = intVal;
    }
    return defaultValue;
  }

boost::optional< bool >
defaultBoolValue(std::string strVal)
{
  boost::optional< bool > defaultValue;
  if (!strVal.empty())
  {
    bool boolVal = false;
    char b = strVal[0];
    switch (b)
    {
      case '1':
      case '-': // -1
      case 'y':
      case 'Y':
      case 't':
      case 'T':
        boolVal = true;
        break;
    };
    defaultValue = boolVal;
  }
  return defaultValue;
}

boost::optional< std::string >
defaultStrValue(std::string strVal)
{
  boost::optional< std::string > defaultValue;
  if (!strVal.empty())
    defaultValue = strVal;
  return defaultValue;
}

//---------------------------------------------------------------------------
//                       PropertiesModel Implementation
//---------------------------------------------------------------------------

PropertiesModel::PropertiesModel(boost::shared_ptr< SmModel > smModel,
    int id,
    std::string tableName) :
  mRootNode(NULL), //
      mId(id), //
      mTableName(tableName), //
      mSmModel(smModel)
{
  mDynamicModel.reset(new orm::v1::DynamicModel(Main::Inst().metaDataDatabaseConnectionSettings()));
  setupOrm();

} //---------------------------------------------------------------------------

PropertiesModel::~PropertiesModel()
{
}
//---------------------------------------------------------------------------

bool
PropertiesModel::save()
{
  return mDynamicModel->save();
}
//---------------------------------------------------------------------------

// Abstract implementation
QVariant
PropertiesModel::headerData(int prmSection,
    Qt::Orientation prmOrientation,
    int prmRole) const
{
  if (prmOrientation == Qt::Vertical || prmRole != Qt::DisplayRole)
    return QVariant();

  switch (prmSection)
  {
    case ColName:
      return "Name";

    case ColValue:
      return "Value";

    default:
      assert(false);
  }

  return QVariant();
}
//---------------------------------------------------------------------------

QModelIndex
PropertiesModel::index(int row, int column, const QModelIndex& parent) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      row,
  //      column,
  //      reinterpret_cast< uintptr_t > (parent.internalPointer()));

  // Special Case  - If the parent is the root
  if (parent == QModelIndex())
    return createIndex(row, column, mRootNode->children.at(row));

  PropertiesModelPrivate::Node& parentNode = Node(parent);
  return createIndex(row, column, parentNode.children.at(row));
}
//---------------------------------------------------------------------------

QModelIndex
PropertiesModel::parent(const QModelIndex& index) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      index.row(),
  //      index.column(),
  //      reinterpret_cast< uintptr_t > (index.internalPointer()));

  assert(index.isValid());

  PropertiesModelPrivate::Node& node = Node(index);

  // Every node in the model will have a parent
  if (node.parent->object == NULL)
    return QModelIndex();

  return createIndex(node.parent->row, 0, node.parent);
}
//---------------------------------------------------------------------------

int
PropertiesModel::rowCount(const QModelIndex& parent) const
{
  //  qDebug("Entering %s, row=%d, col=%d, ptr=%ld",
  //      __PRETTY_FUNCTION__,
  //      parent.row(),
  //      parent.column(),
  //      reinterpret_cast< uintptr_t > (parent.internalPointer()));

  if (mRootNode == NULL)
    return 0;

  // Special Case  - If the parent is the root
  if (parent == QModelIndex())
    return mRootNode->children.size();

  return Node(parent).children.size();
}
//---------------------------------------------------------------------------

int
PropertiesModel::columnCount(const QModelIndex& parent) const
{
  return ColCount;
}
//---------------------------------------------------------------------------

Qt::ItemFlags
PropertiesModel::flags(const QModelIndex &index) const
{
  Qt::ItemFlags f = QAbstractItemModel::flags(index);

  PropertiesModelPrivate::Node& node = Node(index);

  // Only field values are editable
  if (node.objectType != OrmField)
    return f;

  if (index.column() == ColName)
    return f |= Qt::ItemIsUserCheckable;

  AttrI& attr = attribute(node);

  if (attr.IsPrimaryKey())
    return f;

  MvLinkI::PtrT mvLink = attr.mvLink();
  MvLinkI::StandardValueT variant;

  // We only support standard values at this stage in development
  if (!mvLink->value(variant))
    return f;

  f |= Qt::ItemIsEditable;

  MvLinkStandardValueVisitor visitor;
  boost::apply_visitor(visitor, variant);

  // If its a boolean
  if (visitor.ValueType == MvLinkStandardValueVisitor::Bool)
    f |= Qt::ItemIsUserCheckable;

  return f;
} //---------------------------------------------------------------------------

QVariant
PropertiesModel::data(const QModelIndex& index, int role) const
{
  assert(index.isValid() && (0 <= index.column() && index.column() < ColCount));

  PropertiesModelPrivate::Node& node = Node(index);
  switch (node.objectType)
  {
    case OrmTable:
    {
      orm::v1::SqlOpts::PtrT
          object = boost::get< orm::v1::SqlOpts::PtrT >(*node.object);
      assert(object.get());

      switch (role)
      {
        case Qt::DisplayRole:
          if (index.column() == ColName)
            return QString(object->name().c_str());
          break;

        case Qt::BackgroundColorRole:
          return QBrush(QColor("Orange"));

      }
      break;
    }

    case OrmField:
    {
      AttrI& attr = attribute(node);
      MvLinkI::PtrT mvLink = attr.mvLink();

      if (role == Qt::ForegroundRole && !attr.isValid())
        return QBrush(QColor("Red"));

      switch (index.column())
      {
        case ColName:
        {
          switch (role)
          {
            case Qt::DisplayRole:
            case Qt::ToolTipRole:
              return QString(attr.FieldName().c_str());

            case Qt::CheckStateRole:
              return (mvLink->isNull() ? Qt::Unchecked : Qt::Checked);

          }
          break;
        }

        case ColValue:
        {
          if (attr.isNull())
            return QVariant();

          MvLinkI::StandardValueT variant;
          if (!mvLink->value(variant))
          {
            if (role == Qt::DisplayRole)
              return QString("Unsupported Type");
            return QVariant();
          }

          MvLinkStandardValueVisitor visitor;
          boost::apply_visitor(visitor, variant);

          if (visitor.ValueType == MvLinkStandardValueVisitor::Bool)
          {
            if (role == Qt::CheckStateRole)
              return visitor.Data;
            return QVariant();
          }

          switch (role)
          {
            case Qt::DisplayRole:
            case Qt::ToolTipRole:
            case Qt::EditRole:
              return visitor.Data;
          }
          break;
        }
      }
      break;
    }

    case NoObject:
      assert(false);
      break;
  }

  return QVariant();
}
//---------------------------------------------------------------------------

bool
PropertiesModel::setData(const QModelIndex &index,
    const QVariant &value,
    int role)
{
  assert(index.isValid() && (0 <= index.column() && index.column() < ColCount));

  PropertiesModelPrivate::Node& node = Node(index);
  assert(node.objectType == OrmField);

  AttrI& attr = attribute(node);
  MvLinkI::PtrT mvLink = attr.mvLink();

  if (index.column() == ColName)
  {
    assert(role == Qt::CheckStateRole);

    if (value.toInt() == Qt::Unchecked)
    {
      mvLink->setNull();
    }
    else if (attr.IsPrimaryKey())
    {
      // Set all the other attributes
      const AttrsT& siblingAttrs = attr.storableConfig().fields();
      for (AttrsT::const_iterator siblingAttrItr = siblingAttrs.begin(); //
      siblingAttrItr != siblingAttrs.end(); ++siblingAttrItr)
      {
        // Set them all to the default
        AttrI* siblingAttr = *siblingAttrItr;
        MvLinkI::PtrT siblingMvLink = siblingAttr->mvLink();
        // set it to its default value
        siblingMvLink->setDefault();
      }

      // Copy the value into this attr
      MvLinkI::StandardValueT baseIdVar = mId;
      assert(mvLink->setValue(baseIdVar));

      // Notify of the data changing
      for (PropertiesModelPrivate::Node::ChildrenT::const_iterator
          itr = node.parent->children.begin(); itr
          != node.parent->children.end(); ++itr)
      {
        PropertiesModelPrivate::Node& siblingNode = **itr;
        QModelIndex siblingIndexStart = createIndex(siblingNode.row,
            ColName,
            &siblingNode);
        QModelIndex siblingIndexEnd = createIndex(siblingNode.row,
            ColValue,
            &siblingNode);
        dataChanged(siblingIndexStart, siblingIndexEnd);
      }

    }
    else
    {
      //      // set it to its current value to reset the isNull
      //      MvLinkI::StandardValueT setSameVariant;
      //      assert(mvLink->value(setSameVariant));
      //      assert(mvLink->setValue(setSameVariant));
      mvLink->setDefault();
    }

    return true;
  }

  // Get the MvLink value and then visit to get the MvLink data type
  MvLinkI::StandardValueT variant;
  assert(mvLink->value(variant));

  MvLinkStandardValueVisitor visitor;
  boost::apply_visitor(visitor, variant);

  if (role == Qt::CheckStateRole || role == Qt::EditRole)
  {
    if (value.toString().isEmpty())
    {
      mvLink->setNull();
      return true;
    }
    else
    {
      return mvLink->setValue(visitor.fromQVariant(value));
    }
  }

  return false;
}
//---------------------------------------------------------------------------

PropertiesModelPrivate::Node&
PropertiesModel::Node(const QModelIndex& index) const
{
  if (!index.isValid())
    assert(false);

  PropertiesModelPrivate::Node
      * obj = static_cast< PropertiesModelPrivate::Node* > (index.internalPointer());
  obj = dynamic_cast< PropertiesModelPrivate::Node* > (obj);
  assert(obj);
  return *obj;
}
//---------------------------------------------------------------------------

AttrI&
PropertiesModel::attribute(PropertiesModelPrivate::Node& node) const
{
  AttrI** objectPtr = boost::get< AttrI* >(node.object);
  assert(objectPtr);
  assert(*objectPtr);
  return **objectPtr;
}
//---------------------------------------------------------------------------

void
PropertiesModel::setupOrm()
{
  SmTable::PtrT table;
  {
    for (SmModel::TableListT::const_iterator
        tblItr = mSmModel->Tables().begin(); //
    tblItr != mSmModel->Tables().end(); ++tblItr)
    {
      if ((*tblItr)->fullName() == mTableName)
      {
        table = *tblItr;
        break;
      }
    }
  }
  assert(table.get());

  std::list< SmTable::PtrT > tables;
  for (tables.push_front(table); //
  tables.front()->inherits() != NULL; //
  tables.push_front(tables.front()->inherits()))
    ;

  DynamicStorableConfig::PtrT startCfg = mDynamicModel->addInherited();
  startCfg->sqlOpts()->setTableName(tables.front()->fullName());
  startCfg->sqlOpts()->setName(tables.front()->fullName());

  boost::format condition("%s.\"ID\" = %d");
  condition % startCfg->sqlOpts()->sqlAlias();
  condition % mId;

  startCfg->sqlOpts()->addCondition(condition.str());

  addColumns(tables.front(), startCfg);
  tables.pop_front();

  for (; !tables.empty(); tables.pop_front())
  {
    const SmTable::PtrT& table = tables.front();
    DynamicStorableConfig::PtrT cfg = mDynamicModel->addInherited();
    cfg->sqlOpts()->setTableName(table->fullName());
    cfg->sqlOpts()->setName(table->fullName());
    cfg->sqlOpts()->addJoin(SqlJoin("ID",
        startCfg->sqlOpts(),
        "ID",
        SqlJoin::LeftJoin));
    addColumns(table, cfg);
  }

  assert(mDynamicModel->load());

  if (mDynamicModel->storables().empty())
    return;

  assert(mDynamicModel->storables().size() == 1);

  DynamicStorable::PtrT storable = mDynamicModel->storables().front();

  setupModel(*storable->config());
}
// ----------------------------------------------------------------------------

void
PropertiesModel::addColumns(const SmTable::PtrT& table,
    orm::v1::DynamicStorableConfig::PtrT& cfg)
{
  SmTable::ColumnsListT tableColumns;
  table->columns(tableColumns);
  for (SmTable::ColumnsListT::const_iterator colItr = tableColumns.begin(); //
  colItr != tableColumns.end(); ++colItr)
  {
    const SmColumn::PtrT& column = *colItr;

    if (column->dataType() == "int2")
      cfg->addFieldMeta(DynamicInt16PropertyT::setupStatic(column->name(),
          column->isPrimaryKey(),
          column->isNullable(),
          defaultIntValue< int16_t > (column->defaultValue())//
      ));

    else if (column->dataType() == "int4")
      cfg->addFieldMeta(DynamicInt32PropertyT::setupStatic(column->name(),
          column->isPrimaryKey(),
          column->isNullable(),
          defaultIntValue< int32_t > (column->defaultValue())//
      ));

    else if (column->dataType() == "int8")
      cfg->addFieldMeta(DynamicInt64PropertyT::setupStatic(column->name(),
          column->isPrimaryKey(),
          column->isNullable(),
          defaultIntValue< int64_t > (column->defaultValue())//
      ));

    else if (column->dataType() == "float4")
      cfg->addFieldMeta(DynamicReal32PropertyT::setupStatic(column->name(),
          column->isPrimaryKey(),
          column->isNullable(),
          defaultRealValue< float > (column->defaultValue())//
      ));

    else if (column->dataType() == "string")
      cfg->addFieldMeta(DynamicStringPropertyT::setupStatic(column->name(),
          column->isPrimaryKey(),
          column->isNullable(),
          defaultStrValue(column->defaultValue())//
      ));

    else if (column->dataType() == "bool")
      cfg->addFieldMeta(DynamicBoolPropertyT::setupStatic(column->name(),
          column->isPrimaryKey(),
          column->isNullable(),
          defaultBoolValue(column->defaultValue())//
      ));

    else
    {
      assert(false);
    }
    //    qDebug("Addeding %s from %s%s",
    //        column->name().c_str(),
    //        table->name().c_str(),
    //        (column->isPrimaryKey() ? ", Is PK" : ""));
  }
}

// ----------------------------------------------------------------------------

void
PropertiesModel::setupModel(orm::v1::StorableConfigI& startConfig)
{
  mRootNode = &mNodes[SmVariantT()];

  std::list< StorableConfigI* > orderedCfgs;
  for (StorableConfigI* cfg = &startConfig; cfg != NULL; cfg = cfg->inheritsFrom())
    orderedCfgs.push_front(cfg);

  for (std::list< StorableConfigI* >::iterator cfgItr = orderedCfgs.begin(); //
  cfgItr != orderedCfgs.end(); ++cfgItr)
  {
    StorableConfigI* cfg = *cfgItr;
    PropertiesModelPrivate::Node * cfgNode = NULL;

    // Scope the variables

    {
      SqlOpts::PtrT object = cfg->sqlOpts();
      cfgNode = &mNodes[SmVariantT(object)];

      // Set the parent
      mRootNode->children.push_back(cfgNode);
      cfgNode->parent = mRootNode;
      cfgNode->row = mRootNode->children.size() - 1;

      NodeMapT::iterator mapItr = mNodes.find(SmVariantT(object));
      cfgNode->object = const_cast< SmVariantT* > (&mapItr->first);
      cfgNode->objectType = OrmTable;
    }

    for (AttrsT::const_iterator itr = cfg->fields().begin(); //
    itr != cfg->fields().end(); itr++)
    {
      AttrI* data = *itr;

      PropertiesModelPrivate::Node * node = &mNodes[SmVariantT(data)];

      // Set the parent
      cfgNode->children.push_back(node);
      node->parent = cfgNode;
      node->row = cfgNode->children.size() - 1;

      NodeMapT::iterator mapItr = mNodes.find(SmVariantT(data));
      node->object = const_cast< SmVariantT* > (&mapItr->first);
      node->objectType = OrmField;
    }
  }

}
//---------------------------------------------------------------------------
