/*
 * uHierarchModel.h
 *
 *  Created on: Aug 25, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _PROPERTIES_QT_MODEL_H_
#define _PROPERTIES_QT_MODEL_H_

// Include Std headers
#include <string>
#include <map>
#include <vector>
#include <set>

#include <boost/shared_ptr.hpp>
#include <boost/variant.hpp>
// Include Qt headers
#include <QtCore/qabstractitemmodel.h>

#include "orm/v1/StorableConfig.h"
namespace orm
{
  namespace v1
  {
    class AttrI;
    class DynamicStorableConfig;
    class DynamicModel;
  }
}

class SmTable;
class SmModel;

namespace PropertiesModelPrivate
{
  class Node;
  typedef boost::variant< orm::v1::SqlOpts::PtrT, orm::v1::AttrI* > SmVariantT;

}

class PropertiesModel : public QAbstractItemModel
{
  public:
    enum TeCol
    {
      ColName,
      ColValue,
      ColCount

    };

  public:
    PropertiesModel(
        boost::shared_ptr< SmModel > smModel,int id, std::string tableName);
    virtual
    ~PropertiesModel();

    bool save();

  public:
    // Abstract implementation
    QVariant
    headerData(int prmSection,
        Qt::Orientation prmOrientation,
        int prmRole = Qt::DisplayRole) const;

    QModelIndex
    index(int row, int column, const QModelIndex& parent = QModelIndex()) const;

    QModelIndex
    parent(const QModelIndex& index) const;

    int
    rowCount(const QModelIndex& parent = QModelIndex()) const;

    int
    columnCount(const QModelIndex& parent = QModelIndex()) const;

    QVariant
    data(const QModelIndex& index, int prmRole = Qt::DisplayRole) const;

    Qt::ItemFlags
    flags(const QModelIndex &index) const;

    bool
    setData(const QModelIndex &index,
        const QVariant &value,
        int role = Qt::EditRole);

  private:
    /// --------------
    // ObjectModel methods

    PropertiesModelPrivate::Node&
    Node(const QModelIndex& index) const;

    orm::v1::AttrI&
    attribute(PropertiesModelPrivate::Node& node) const;

    void
    setupOrm();

    void
    addColumns(const boost::shared_ptr< SmTable >& table,
        boost::shared_ptr< orm::v1::DynamicStorableConfig >& cfg);

    void
    setupModel(orm::v1::StorableConfigI& startConfig);

    typedef std::map< PropertiesModelPrivate::SmVariantT,
        PropertiesModelPrivate::Node > NodeMapT;
    NodeMapT mNodes;

    /*! Link to the root node.
     * This node will not be in the model, its equivilent to QModelIndex()
     */
    PropertiesModelPrivate::Node* mRootNode;

    int mId;
    std::string mTableName;

    boost::shared_ptr< SmModel > mSmModel;
    boost::shared_ptr< orm::v1::DynamicModel > mDynamicModel;

};
// ----------------------------------------------------------------------------


#endif /* _PROPERTIES_QT_MODEL_H_ */
