/*
 * IsTables.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _IS_TABLE_H_
#define _IS_TABLE_H_

#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

/*! simSCADA Sim Objects
 * Storage class to load simSCADA objects
 *
 */
#include "orm/v1/Storable.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"
#include "orm/v1/RelatedToManyProperty.h"

#include "IsSchema.h"

class IsModel;
class IsInherit;
class IsTable;

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(IsTable, std::string)

class IsTable : public orm::v1::Storable< IsModel, IsTable >
{
  public:
  IsTable();

  virtual
  ~IsTable();

  public:
  /// --------------
  // Setup methods

  static bool
  setupStatic();

  static bool
  setupStaticOracle();

  public:
  /// --------------
  // Declare the properties for this storable

  ORM_V1_DECLARE_RELATED_PROPERTY(schema, Schema, std::string, IsSchema)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(name, Name, std::string)
  ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(inheritsList, InheritsList, IsInherit)

  public:
  std::string
  fullName() const;

  std::string
  displayName() const;

  PtrT
  inherits() const;

  std::string
  inheritType() const;

};
// ----------------------------------------------------------------------------

#endif /* _IS_TABLE_H_ */
