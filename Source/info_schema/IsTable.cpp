/*
 * IsTables.cpp
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#include "IsTable.h"

#include <boost/format.hpp>

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "IsOrmModel.h"
#include "IsInherit.h"

ORM_V1_CALL_STORABLE_SETUP( IsTable)
ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(IsTable, std::string, fullName)
template class orm::v1::StorageWrapper<IsModel, IsTable, std::string>;
template class orm::v1::StorableWrapper<IsModel, IsTable>;
// ----------------------------------------------------------------------------

/// --------------
// Declare the properties for this storable

ORM_V1_DEFINE_RELATED_PROPERTY(IsModel, IsTable, schema, Schema, std::string, IsSchema)
ORM_V1_DEFINE_SIMPLE_PROPERTY(IsTable, name, Name, std::string)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(IsTable, inheritsList, InheritsList, IsInherit)
// ----------------------------------------------------------------------------

IsTable::IsTable()
{
  mPropSchema.setup(this);
  mPropName.setup(this);

  // Relates to many properties
  mPropInheritsList.setup(this);
}
// ----------------------------------------------------------------------------

IsTable::~IsTable()
{
}
// ----------------------------------------------------------------------------

bool
IsTable::setupStatic()
{
  using namespace orm::v1;
  typedef StorableConfig< IsTable > CfgT;

  AttrI::PtrT attrSchema = SchemaPropT::setupStatic("table_schema",
      &IsSchema::name,
      &IsModel::Schema);
  AttrI::PtrT attrTable = NamePropT::setupStatic("table_name");

  // Use the table name as our "isSetup" flag
  if (CfgT::setSqlOpts()->tableName().empty())
  {
    CfgT::addFieldMeta(attrSchema);
    CfgT::addFieldMeta(attrTable);
  }

  CfgT::setSqlOpts()->setName("IsTable");
  CfgT::setSqlOpts()->setTableName("information_schema.tables");
  CfgT::setSqlOpts()->setSelectDistinct(true);
  CfgT::setSqlOpts()->clearConditions();
  CfgT::setSqlOpts()->addCondition("table_schema != 'information_schema'");
  CfgT::setSqlOpts()->addCondition("table_schema != 'pg_catalog'");
  CfgT::setSqlOpts()->addCondition("table_type = 'BASE TABLE'");
  return true;
}
// ----------------------------------------------------------------------------

bool
IsTable::setupStaticOracle()
{
  using namespace orm::v1;
  typedef StorableConfig< IsTable > CfgT;
  CfgT::setSqlOpts()->setName("IsTable");
  CfgT::setSqlOpts()->setTableName("ALL_TAB_COLUMNS");
  CfgT::setSqlOpts()->setSelectDistinct(true);
  CfgT::setSqlOpts()->clearConditions();
  CfgT::setSqlOpts()->addCondition("OWNER != 'SYS'");
  CfgT::setSqlOpts()->addCondition("OWNER != 'WMSYS'");

  SchemaPropT::setupStatic("OWNER", &IsSchema::name, &IsModel::Schema);
  NamePropT::setupStatic("TABLE_NAME");
  return true;
}
// ----------------------------------------------------------------------------


std::string
IsTable::fullName() const
{
  std::string fullName_;

  if (schema().get() == NULL)
    fullName_ = mPropSchema.InitStepDbValue();
  else
    fullName_ = schema()->name();

  return fullName_ + "." + name();
}
// ----------------------------------------------------------------------------

std::string
IsTable::displayName() const
{
  std::string name_ = name();

  if (inherits().get() != NULL)
  {
    boost::format inherit("(%s:%s)");
    inherit % inherits()->fullName();
    inherit % inheritType();
    name_ += inherit.str();
  }

  return name_;
}
// ----------------------------------------------------------------------------


IsTable::PtrT
IsTable::inherits() const
{
  InheritsListListT inheritsLists;
  inheritsList(inheritsLists);

  switch (inheritsLists.size())
  {
    case 1:
      return inheritsLists.front()->dstTable();

    case 0:
      return PtrT();

    default:
      for (InheritsListListT::iterator itr = inheritsLists.begin(); //
      itr != inheritsLists.end(); ++itr)
      {
        IsInherit::PtrT isInherit = *itr;
        qDebug("src=%s, dst=%s, type=%s",
            isInherit->srcTable()->fullName().c_str(),
            isInherit->dstTable()->fullName().c_str(),
            isInherit->type().c_str());
      }
      assert(false);
  }

  return PtrT();
}
// ----------------------------------------------------------------------------


std::string
IsTable::inheritType() const
{
  InheritsListListT inheritsLists;
  inheritsList(inheritsLists);

  switch (inheritsLists.size())
  {
    case 1:
      return inheritsLists.front()->type();

    case 0:
      return "";

    default:
      assert(false);
  }

  return "";
}
// ----------------------------------------------------------------------------
