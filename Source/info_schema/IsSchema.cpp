/*
 * IsSchemas.cpp
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#include "IsSchema.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

ORM_V1_CALL_STORABLE_SETUP( IsSchema)
ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(IsSchema, std::string, name)
template class orm::v1::StorageWrapper<IsModel, IsSchema, std::string>;
template class orm::v1::StorableWrapper< IsModel, IsSchema >;
// ----------------------------------------------------------------------------
/// --------------
// Declare the properties for this storable

ORM_V1_DEFINE_SIMPLE_PROPERTY(IsSchema, name, Name, std::string)
// ----------------------------------------------------------------------------

IsSchema::IsSchema()
{
  mPropName.setup(this);
}
// ----------------------------------------------------------------------------

IsSchema::~IsSchema()
{
}
// ----------------------------------------------------------------------------

bool
IsSchema::setupStatic()
{
  using namespace orm::v1;
  typedef StorableConfig< IsSchema > CfgT;

  AttrI::PtrT attrName = NamePropT::setupStatic("table_schema");

  // Use the table name as our "isSetup" flag
  if (CfgT::setSqlOpts()->tableName().empty())
  {
    CfgT::addFieldMeta(attrName);
  }

  CfgT::setSqlOpts()->setName("IsSchema");
  CfgT::setSqlOpts()->setTableName("information_schema.tables");
  CfgT::setSqlOpts()->setSelectDistinct(true);
  CfgT::setSqlOpts()->clearConditions();
  CfgT::setSqlOpts()->addCondition("table_schema != 'information_schema'");
  CfgT::setSqlOpts()->addCondition("table_schema != 'pg_catalog'");

  return true;
}
// ----------------------------------------------------------------------------

bool
IsSchema::setupStaticOracle()
{
  using namespace orm::v1;
  typedef StorableConfig< IsSchema > CfgT;

  CfgT::setSqlOpts()->setTableName("ALL_TAB_COLUMNS");
  CfgT::setSqlOpts()->setSelectDistinct(true);
  CfgT::setSqlOpts()->clearConditions();
  CfgT::setSqlOpts()->addCondition("OWNER != 'SYS'");
  CfgT::setSqlOpts()->addCondition("OWNER != 'WMSYS'");

  NamePropT::setupStatic("OWNER");

  return true;
}
// ----------------------------------------------------------------------------
