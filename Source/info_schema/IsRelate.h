/*
 * IsRelates.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _IS_RELATE_H_
#define _IS_RELATE_H_

#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

/*! simSCADA Sim Objects
 * Storage class to load simSCADA objects
 *
 */
#include "orm/v1/Storable.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"
//#include "RelatedProperty.h"
//#include "RelatedToManyProperty.h"

#include "IsColumn.h"

class IsModel;
class IsRelate;

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(IsRelate, std::string)

class IsRelate : public orm::v1::Storable< IsModel, IsRelate >
{
  public:
    IsRelate();

    virtual
    ~IsRelate();

  public:
    /// --------------
    // Setup methods

    static bool
    setupStatic();

  public:
    /// --------------
    // Declare the properties for this storable

  ORM_V1_DECLARE_RELATED_PROPERTY(srcColumn, SrcColumn, std::string, IsColumn)
  ORM_V1_DECLARE_RELATED_PROPERTY(dstColumn, DstColumn, std::string, IsColumn)

  public:
    std::string
    srcColumnFullName() const;

};
// ----------------------------------------------------------------------------

#endif /* _IS_RELATE_H_ */
