/*
 * IsColumns.cpp
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#include "IsColumn.h"

#include <boost/format.hpp>

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "IsOrmModel.h"
#include "IsRelate.h"

ORM_V1_CALL_STORABLE_SETUP(IsColumn)
ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(IsColumn, std::string, fullName)
template class orm::v1::StorageWrapper<IsModel, IsColumn, std::string>;
template class orm::v1::StorableWrapper< IsModel, IsColumn >;
// ----------------------------------------------------------------------------

/// --------------
// Declare the properties for this storable

ORM_V1_DEFINE_RELATED_PROPERTY(IsModel,
    IsColumn,
    table,
    Table,
    std::string,
    IsTable)
ORM_V1_DEFINE_SIMPLE_PROPERTY(IsColumn, name, Name, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(IsColumn, dataType, DataType, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(IsColumn, dataLength, DataLength, int16_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(IsColumn, isNullable, Nullable, bool)
ORM_V1_DEFINE_SIMPLE_PROPERTY(IsColumn, defaultValue, DefaultValue, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(IsColumn, isPrimaryKey, PrimaryKey, bool)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(IsColumn,
    relatesToList,
    RelatesToList,
    IsRelate)
// ----------------------------------------------------------------------------

IsColumn::IsColumn()
{
  mPropTable.setup(this);
  mPropName.setup(this);
  mPropDataType.setup(this);
  mPropDataLength.setup(this);
  mPropNullable.setup(this);
  mPropDefaultValue.setup(this);
  mPropPrimaryKey.setup(this);

  // Relates to many properties
  mPropRelatesToList.setup(this);
}
// ----------------------------------------------------------------------------

IsColumn::~IsColumn()
{
}
// ----------------------------------------------------------------------------

bool
IsColumn::setupStatic()
{
  using namespace orm::v1;
  typedef StorableConfig< IsColumn > CfgT;

  AttrI::PtrT attrTable = TablePropT::setupStatic("full_table_name",
      &IsTable::fullName,
      &IsModel::Table);
  AttrI::PtrT attrName = NamePropT::setupStatic("column_name");
  AttrI::PtrT attrType = DataTypePropT::setupStatic("udt_name");
  AttrI::PtrT attrLen = DataLengthPropT::setupStatic("character_octet_length");
  AttrI::PtrT attrNullable = NullablePropT::setupStatic("is_nullable");
  AttrI::PtrT attrDefault = DefaultValuePropT::setupStatic("default_value");
  AttrI::PtrT attrPrimaryKey = PrimaryKeyPropT::setupStatic("is_primary_key");

  if (CfgT::setSqlOpts()->name().empty())
  {
    CfgT::addFieldMeta(attrTable);
    CfgT::addFieldMeta(attrName);
    CfgT::addFieldMeta(attrType);
    CfgT::addFieldMeta(attrLen);
    CfgT::addFieldMeta(attrNullable);
    CfgT::addFieldMeta(attrDefault);
    CfgT::addFieldMeta(attrPrimaryKey);
  }

  CfgT::setSqlOpts()->setName("IsColumn");

  std::string sql //
  = "SELECT DISTINCT  \n"
      "  c.table_schema || '.' || c.table_name as full_table_name \n"
      "  , c.column_name \n"
      "  , c.udt_name \n"
      "  , c.character_octet_length \n"
      "  , (c.is_nullable = 'YES')::bool as is_nullable  \n"
      "  , CASE  \n"
      "      WHEN c.column_default like 'nextval%' THEN  \n"
      "        NULL  \n"
      "      ELSE  \n"
      "        replace(split_part(c.column_default, '::', 1), '''', '') \n"
      "    END AS default_value  \n"
      "  , (c.column_name = 'ID' or c.column_name = 'oid') AS is_primary_key \n"
      "FROM information_schema.columns c \n"
      "  JOIN information_schema.tables t  \n"
      "    ON c.table_name = t.table_name  \n"
      "       AND c.table_schema = t.table_schema  \n"
      "WHERE  \n"
      "  t.table_schema != 'information_schema' \n"
      "  AND t.table_schema != 'pg_catalog' \n"
      "  AND t.table_type = 'BASE TABLE' \n";

  CfgT::setSqlOpts()->setQuerySql(sql);
  return true;
}
// ----------------------------------------------------------------------------

bool
IsColumn::setupStaticOracle()
{
  using namespace orm::v1;
  typedef StorableConfig< IsColumn > CfgT;

  std::string sql //
  = "SELECT  \n"
      "  c.OWNER || '.' || c.TABLE_NAME as FULL_TABLE_NAME \n"
      "  , c.COLUMN_NAME \n"
      "  , c.DATA_TYPE \n"
      "  , c.DATA_LENGTH \n"
      "  , DECODE( c.NULLABLE, 'Y', 1, 0) as IS_NULLABLE  \n"
      "  , DATA_DEFAULT  \n"
      "  , DECODE(con.constraint_type, 'P', 1, 0) IS_PRIMARY_KEY   \n"
      "FROM ALL_TAB_COLUMNS c \n"
      "  , (SELECT * FROM all_constraints WHERE constraint_type = 'P') con \n"
      "  , all_cons_columns ccol \n"
      "WHERE c.OWNER != \'SYS\' \n"
      "  AND c.OWNER != \'WMSYS\' \n"
      "  AND c.owner = ccol.owner(+) \n"
      "  AND c.table_name = ccol.table_name(+) \n"
      "  AND c.column_name = ccol.column_name(+) \n"
      "  AND ccol.owner = con.owner(+) \n"
      "  AND ccol.table_name = con.table_name(+) \n"
      "  AND ccol.constraint_name = con.constraint_name(+) \n";

  CfgT::setSqlOpts()->setQuerySql(sql);

  TablePropT::setupStatic("FULL_TABLE_NAME",
      &IsTable::fullName,
      &IsModel::Table);
  NamePropT::setupStatic("COLUMN_NAME");
  DataTypePropT::setupStatic("DATA_TYPE");
  DataLengthPropT::setupStatic("DATA_LENGTH");
  NullablePropT::setupStatic("IS_NULLABLE");
  DefaultValuePropT::setupStatic("DATA_DEFAULT");
  PrimaryKeyPropT::setupStatic("IS_PRIMARY_KEY");
  return true;
}
// ----------------------------------------------------------------------------

std::string
IsColumn::fullName() const
{
  std::string fullName;

  if (table().get() == NULL)
    fullName = mPropTable.InitStepDbValue();
  else
    fullName = table()->fullName();

  return fullName + "." + name();
}
// ----------------------------------------------------------------------------

std::string
IsColumn::ConvertedDataType() const
{
  if (name() == "SIM_VALUE" //
  || name() == "SYS_VALUE" //
  || name() == "SYS_RAW_VALUE" //
  || name() == "AO_ENG_MIN" //
  || name() == "AO_ENG_MAX")
  {
    qWarning("Applying QSql Metadata SQLite REAL datatype workaround"
        " for column %s", name().c_str());
    return "float8";
  }

  const std::string type = dataType();

  if (type == "CHAR")
    return "string";

  if (type == "NCLOB")
    return "string";

  if (type == "CLOB")
    return "string";

  if (type == "VARCHAR2")
    return "string";

  if (type == "varchar")
    return "string";

  if (type == "int2")
    return type;

  if (type == "int4")
    return type;

  if (type == "RAW")
    return "int4";

  if (type == "NUMBER")
    return "int4";

  if (type == "RAW")
    return "int4";

  if (type == "int8")
    return type;

  if (type == "ROWID")
    return "int8";

  if (type == "LONG")
    return "int8";

  if (type == "LONG RAW")
    return "int8";

  if (type == "FLOAT")
    return "float4";

  if (type == "REAL")
    return "float4";

  if (type == "float4")
    return type;

  if (type == "float8")
    return type;

  if (type == "bool")
    return type;

  if (type == "varchar")
    return "string";

  if (type == "text")
    return "string";

  if (type == "string")
    return type;

  if (type == "BLOB") // FIXME
    return "string";

  if (type == "bytea") // FIXME
    return "string";

  if (type == "timestamp") // FIXME
    return "string";

  if (type == "DATE") // FIXME
    return "string";

  if (type == "TIMESTAMP(6) WITH TIME ZONE") // FIXME
    return "string";

  if (type == "UNDEFINED") // FIXME
    return "string";

  if (type == "unknown") // FIXME
    return "string";

  qDebug("Unknown data type, |%s|", type.c_str());
  // assert(false);
  return "string";
}
// ----------------------------------------------------------------------------

std::string
IsColumn::DisplayName() const
{
  boost::format fmt("%s (%s,%d%s)%s%s%s");
  fmt % name() % ConvertedDataType() % dataLength();
  fmt % (isNullable() ? ",Nullable" : "");

  if (relatesTo().get() != NULL)
    fmt % ("->" + relatesTo()->fullName());
  else
    fmt % "";

  fmt % (isDefaultValueNull() ? "" : ("=" + defaultValue()));

  fmt % (isPrimaryKey() ? ",PK" : "");

  return fmt.str();
}
// ----------------------------------------------------------------------------

std::string
IsColumn::userModifiedComapareString() const
{
  boost::format fmt("%s (%s,%d)%s");
  fmt % name() % ConvertedDataType() % dataLength();

  fmt % (isDefaultValueNull() ? "" : ("=" + defaultValue()));

  return fmt.str();
}
// ----------------------------------------------------------------------------
IsColumn::PtrT
IsColumn::relatesTo() const
{
  RelatesToListListT relatesTos;
  relatesToList(relatesTos);

  switch (relatesTos.size())
  {
    case 1:
      return relatesTos.front()->dstColumn();

    case 0:
      return PtrT();

    default:
    {
      boost::format info("Column %s relates to more than one other column");
      info % fullName();
      throw std::runtime_error(info.str());
    }
  }

  return PtrT();
}
// ----------------------------------------------------------------------------
