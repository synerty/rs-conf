/*
 * IsRelates.cpp
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#include "IsRelate.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "IsOrmModel.h"

ORM_V1_CALL_STORABLE_SETUP( IsRelate)
ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(IsRelate, std::string, srcColumnFullName)
template class orm::v1::StorageWrapper<IsModel, IsRelate, std::string>;
template class orm::v1::StorableWrapper< IsModel, IsRelate >;
// ----------------------------------------------------------------------------

/// --------------
// Declare the properties for this storable

ORM_V1_DEFINE_RELATED_PROPERTY(IsModel, IsRelate, srcColumn, SrcColumn, std::string, IsColumn)
ORM_V1_DEFINE_RELATED_PROPERTY(IsModel, IsRelate, dstColumn, DstColumn, std::string, IsColumn)
// ----------------------------------------------------------------------------


IsRelate::IsRelate()
{
  mPropSrcColumn.setup(this);
  mPropDstColumn.setup(this);
}
// ----------------------------------------------------------------------------

IsRelate::~IsRelate()
{
}
// ----------------------------------------------------------------------------

bool
IsRelate::setupStatic()
{
  using namespace orm::v1;
  typedef StorableConfig< IsRelate > CfgT;
  CfgT::setSqlOpts()->setName("Is Relate");

  std::string
      sql = "SELECT \n"
        "  dst.table_schema "
        "    || '.' || dst.table_name "
        "    || '.' || dst.column_name as dst_column,\n"
        "  src.table_schema  "
        "    || '.' || src.table_name "
        "    || '.' || src.column_name as src_column\n"
        "FROM \n"
        "  information_schema.constraint_column_usage dst, \n"
        "  information_schema.referential_constraints, \n"
        "  information_schema.key_column_usage src\n"
        "WHERE \n"
        "  referential_constraints.unique_constraint_catalog = dst.constraint_catalog AND\n"
        "  referential_constraints.unique_constraint_schema = dst.constraint_schema AND\n"
        "  referential_constraints.unique_constraint_name = dst.constraint_name AND\n"
        "  referential_constraints.constraint_catalog = src.constraint_catalog AND\n"
        "  referential_constraints.constraint_schema = src.constraint_schema AND\n"
        "  referential_constraints.constraint_name = src.constraint_name AND\n"
        "  NOT (src.column_name = \'ID\' AND dst.column_name = \'ID\')";

  CfgT::setSqlOpts()->setQuerySql(sql);

  CfgT::addFieldMeta(SrcColumnPropT::setupStatic("src_column",
      &IsColumn::fullName,
      &IsModel::Column,
      &IsColumn::RelatesToList));
  CfgT::addFieldMeta(DstColumnPropT::setupStatic("dst_column",
      &IsColumn::fullName,
      &IsModel::Column));
  return true;
}
// ----------------------------------------------------------------------------

std::string
IsRelate::srcColumnFullName() const
{
  if (srcColumn().get() == NULL)
    return mPropSrcColumn.InitStepDbValue();

  return srcColumn()->fullName();
}
// ----------------------------------------------------------------------------
