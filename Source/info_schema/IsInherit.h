/*
 * IsInherits.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _IS_INHERIT_H_
#define _IS_INHERIT_H_

#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

/*! simSCADA Sim Objects
 * Storage class to load simSCADA objects
 *
 */
#include "orm/v1/Storable.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"
//#include "RelatedProperty.h"
//#include "RelatedToManyProperty.h"

#include "IsTable.h"

class IsModel;
class IsInherit;

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(IsInherit, std::string)

class IsInherit : public orm::v1::Storable< IsModel, IsInherit >
{
  public:
  IsInherit();

  virtual
  ~IsInherit();

  public:
  /// --------------
  // Setup methods

  static bool
  setupStatic();

  public:
  /// --------------
  // Declare the properties for this storable

  ORM_V1_DECLARE_RELATED_PROPERTY(srcTable, SrcTable, std::string, IsTable)
  ORM_V1_DECLARE_RELATED_PROPERTY(dstTable, DstTable, std::string, IsTable)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(type, Type, std::string)

  public:
  std::string
  srcTableFullName() const;

};
// ----------------------------------------------------------------------------

#endif /* _IS_INHERIT_H_ */
