/*
 * IsModel.h
 *
 *  Created on: Dec 18, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _INFO_SCHEMA_MODEL_H_
#define _INFO_SCHEMA_MODEL_H_

#include "orm/v1/Model.h"

class IsSchema;
class IsTable;
class IsColumn;
class IsInherit;
class IsRelate;

class IsModel : public orm::v1::Model< IsModel >
{
  public:
    IsModel(const orm::v1::Conn& ormConn = orm::v1::Conn());
    virtual
    ~IsModel();

  public:

  ORM_V1_MODEL_DECLARE_STORABLE(IsSchema, Schema, std::string)
  ORM_V1_MODEL_DECLARE_STORABLE(IsTable, Table, std::string)
  ORM_V1_MODEL_DECLARE_STORABLE(IsColumn, Column, std::string)
  ORM_V1_MODEL_DECLARE_STORABLE(IsInherit, Inherit, std::string)
  ORM_V1_MODEL_DECLARE_STORABLE(IsRelate, Relate, std::string)

};

#endif /* _INFO_SCHEMA_MODEL_H_ */
