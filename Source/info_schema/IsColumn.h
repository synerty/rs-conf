/*
 * IsColumns.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _IS_COLUMN_H_
#define _IS_COLUMN_H_

#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

/*! simSCADA Sim Objects
 * Storage class to load simSCADA objects
 *
 */
#include "orm/v1/Storable.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"
#include "orm/v1/RelatedToManyProperty.h"

#include "IsTable.h"

class IsRelate;

class IsModel;
class IsColumn;

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(IsColumn, std::string)

class IsColumn : public orm::v1::Storable< IsModel, IsColumn >
{
  public:
  IsColumn();

  virtual
  ~IsColumn();

  public:
  /// --------------
  // Setup methods

  static bool
  setupStatic();

  static bool
  setupStaticOracle();

  public:
  /// --------------
  // Declare the properties for this storable

  ORM_V1_DECLARE_RELATED_PROPERTY(table, Table, std::string, IsTable)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(name, Name, std::string)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(dataType, DataType, std::string)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(dataLength, DataLength, int16_t)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(isNullable, Nullable, bool)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(defaultValue, DefaultValue, std::string)
  ORM_V1_DECLARE_SIMPLE_PROPERTY(isPrimaryKey, PrimaryKey, bool)
  ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(relatesToList, RelatesToList, IsRelate)

  public:
  std::string
  fullName() const;

  std::string
  ConvertedDataType() const;

  std::string
  DisplayName() const;

  std::string
  userModifiedComapareString() const;

  PtrT
  relatesTo() const;
};
// ----------------------------------------------------------------------------

#endif /* _IS_COLUMN_H_ */
