/*
 * IsInherits.cpp
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#include "IsInherit.h"

#include "IsOrmModel.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>


ORM_V1_CALL_STORABLE_SETUP(IsInherit)
ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(IsInherit, std::string, srcTableFullName)
template class orm::v1::StorageWrapper<IsModel, IsInherit, std::string>;
template class orm::v1::StorableWrapper<IsModel, IsInherit>;
// ----------------------------------------------------------------------------

/// --------------
// Declare the properties for this storable

ORM_V1_DEFINE_RELATED_PROPERTY(IsModel, IsInherit, srcTable, SrcTable, std::string, IsTable)
ORM_V1_DEFINE_RELATED_PROPERTY(IsModel, IsInherit, dstTable, DstTable, std::string, IsTable)
ORM_V1_DEFINE_SIMPLE_PROPERTY(IsInherit, type, Type, std::string)
// ----------------------------------------------------------------------------

IsInherit::IsInherit()
{
  mPropSrcTable.setup(this);
  mPropDstTable.setup(this);
  mPropType.setup(this);
}
// ----------------------------------------------------------------------------

IsInherit::~IsInherit()
{
}
// ----------------------------------------------------------------------------

bool
IsInherit::setupStatic()
{
  using namespace orm::v1;
  typedef StorableConfig< IsInherit > CfgT;
  CfgT::setSqlOpts()->setName("IsInherit");

  std::string
      sql = "SELECT \n"
        "  dst.table_schema || '.' || dst.table_name as dst_table,\n"
        "  src.table_schema  || '.' || src.table_name as src_table,\n"
        "  'JOINED' as type \n"
        "FROM \n"
        "  information_schema.constraint_column_usage dst, \n"
        "  information_schema.referential_constraints, \n"
        "  information_schema.key_column_usage src\n"
        "WHERE \n"
        "  referential_constraints.unique_constraint_catalog = dst.constraint_catalog AND\n"
        "  referential_constraints.unique_constraint_schema = dst.constraint_schema AND\n"
        "  referential_constraints.unique_constraint_name = dst.constraint_name AND\n"
        "  referential_constraints.constraint_catalog = src.constraint_catalog AND\n"
        "  referential_constraints.constraint_schema = src.constraint_schema AND\n"
        "  referential_constraints.constraint_name = src.constraint_name AND\n"
        "  src.column_name = \'ID\' AND dst.column_name = \'ID\'\n";

  sql += "UNION \n"
    "SELECT \n"
    "  src_ns.nspname || \'.\' || src_tbl.relname as src_table, \n"
    "  dst_ns.nspname || \'.\' || dst_tbl.relname as dst_table,\n"
    "  \'CONCRETE\' AS type\n"
    "FROM \n"
    "  pg_catalog.pg_class src_tbl, \n"
    "  pg_catalog.pg_namespace src_ns, \n"
    "  pg_catalog.pg_inherits, \n"
    "  pg_catalog.pg_class dst_tbl, \n"
    "  pg_catalog.pg_namespace dst_ns\n"
    "WHERE \n"
    "  src_tbl.relnamespace = src_ns.oid AND\n"
    "  pg_inherits.inhrelid = dst_tbl.oid AND\n"
    "  pg_inherits.inhparent = src_tbl.oid AND\n"
    "  dst_tbl.relnamespace = dst_ns.oid\n";

  CfgT::setSqlOpts()->setQuerySql(sql);

  CfgT::addFieldMeta(SrcTablePropT::setupStatic("src_table",
      &IsTable::fullName,
      &IsModel::Table,
      &IsTable::InheritsList));
  CfgT::addFieldMeta(DstTablePropT::setupStatic("dst_table",
      &IsTable::fullName,
      &IsModel::Table));
  CfgT::addFieldMeta(TypePropT::setupStatic("type"));
  return true;
}
// ----------------------------------------------------------------------------

std::string
IsInherit::srcTableFullName() const
{
  if (srcTable().get() == NULL)
    return mPropSrcTable.InitStepDbValue();

  return srcTable()->fullName();
}
// ----------------------------------------------------------------------------
