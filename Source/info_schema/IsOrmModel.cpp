/*
 * IsModel.cpp
 *
 *  Created on: Dec 18, 2010
 *      Author: Jarrod Chesney
 */

#include "IsOrmModel.h"

#include "orm/v1/ModelWrapper.ini"

#include "IsSchema.h"
#include "IsTable.h"
#include "IsColumn.h"
#include "IsInherit.h"
#include "IsRelate.h"

template class orm::v1::ModelWrapper< IsModel, IsSchema >;
template class orm::v1::ModelWrapper< IsModel, IsTable >;
template class orm::v1::ModelWrapper< IsModel, IsColumn >;
template class orm::v1::ModelWrapper< IsModel, IsInherit >;
template class orm::v1::ModelWrapper< IsModel, IsRelate >;

IsModel::IsModel(const orm::v1::Conn& ormConn) :
  BaseModel(ormConn)
{
  if (ormConn.Type == orm::v1::Conn::Oracle)
  {
    IsSchema::setupStaticOracle();
    IsTable::setupStaticOracle();
    IsColumn::setupStaticOracle();

    addStorageWrapper(mSchemaStorageWrapper);
    addStorageWrapper(mTableStorageWrapper);
    addStorageWrapper(mColumnStorageWrapper);
    //  addStorageWrapper( mInheritStorageWrapper);
    //  addStorageWrapper( mRelateStorageWrapper);
  }
  else
  {
    IsSchema::setupStatic();
    IsTable::setupStatic();
    IsColumn::setupStatic();

    addStorageWrapper(mSchemaStorageWrapper);
    addStorageWrapper(mTableStorageWrapper);
    addStorageWrapper(mColumnStorageWrapper);
    addStorageWrapper(mInheritStorageWrapper);
    addStorageWrapper(mRelateStorageWrapper);
  }
}
// ----------------------------------------------------------------------------

IsModel::~IsModel()
{
}
// ----------------------------------------------------------------------------


ORM_V1_MODEL_DEFINE_STORABLE(IsModel, IsSchema, Schema, std::string)
ORM_V1_MODEL_DEFINE_STORABLE(IsModel, IsTable, Table, std::string)
ORM_V1_MODEL_DEFINE_STORABLE(IsModel, IsColumn, Column, std::string)
ORM_V1_MODEL_DEFINE_STORABLE(IsModel, IsInherit, Inherit, std::string)
ORM_V1_MODEL_DEFINE_STORABLE(IsModel, IsRelate, Relate, std::string)
