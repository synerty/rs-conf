/*
 * IsSchemas.h
 *
 *  Created on: Dec 17, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _IS_SCHEMA_H_
#define _IS_SCHEMA_H_

#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

/*! simSCADA Sim Objects
 * Storage class to load simSCADA objects
 *
 */
#include "orm/v1/Storable.h"
#include "orm/v1/SimpleProperty.h"

class IsModel;
class IsSchema;

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(IsSchema, std::string)

class IsSchema : public orm::v1::Storable< IsModel, IsSchema >
{
  public:
  /// --------------
  // Types


  public:
  IsSchema();
  virtual
  ~IsSchema();

  public:
  /// --------------
  // Setup methods

  static bool
  setupStatic();

  static bool
  setupStaticOracle();

  public:
  /// --------------
  // Declare the properties for this storable

  ORM_V1_DECLARE_SIMPLE_PROPERTY(name, Name, std::string)
};
// ----------------------------------------------------------------------------

#endif /* _IS_SCHEMA_H_ */
