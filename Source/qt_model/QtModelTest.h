/*
 * QtModelTest.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef QTMODELTEST_H_
#define QTMODELTEST_H_

#include <QtCore/qabstractitemmodel.h>

/*! QtModelTest Brief Description
 * Long comment about the class
 *
 */
class QtModelTest
{
  public:
    QtModelTest(QAbstractItemModel* model);
    virtual
    ~QtModelTest();

  public:
    void
    run();

  private:

    void
    testModel(QModelIndex parent = QModelIndex());

    QAbstractItemModel* mModel;
};

#endif /* QTMODELTEST_H_ */
