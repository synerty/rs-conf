/*
 * uComboItemDelegate.cpp
 *
 *  Created on: Jul 27, 2010
 *      Author: Jarrod Chesney
 */

// Include header for this unit
#include "ComboItemDelegate.h"

// Include Qt libs
#include <Qt/qcombobox.h>
#include <Qt/qapplication.h>
#include <Qt/qstyleditemdelegate.h>

// Project includes

#include "QtModelUserRoles.h"

ComboItemDelegate::ComboItemDelegate(QObject *parent) :
  QItemDelegate(parent)
{
}

ComboItemDelegate::~ComboItemDelegate()
{
}

QWidget*
ComboItemDelegate::createEditor(QWidget *parent,
    const QStyleOptionViewItem &option,
    const QModelIndex &index) const
{
  QStringList options = index.model()->data(index,
      Qt::ComboOptionsRole).toStringList();
  QComboBox* comboBox = new QComboBox(parent);

  comboBox->addItems(options);
  comboBox->setMaxVisibleItems(40);
  return comboBox;
}

void
ComboItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QString value = index.model()->data(index, Qt::EditRole).toString();
  QComboBox *comboBox = dynamic_cast< QComboBox* > (editor);
  int itemIndex = comboBox->findText(value);
  comboBox->setCurrentIndex(itemIndex);
  comboBox->showPopup();
}

void
ComboItemDelegate::setModelData(QWidget *editor,
    QAbstractItemModel *model,
    const QModelIndex &index) const
{
  QComboBox *comboBox = dynamic_cast< QComboBox* > (editor);
  QString value = comboBox->currentText();
  model->setData(index, value, Qt::EditRole);
}

bool
ComboItemDelegate::itemSelected(QEvent* event,
    QAbstractItemModel* model,
    const QStyleOptionViewItem& option,
    const QModelIndex& index)
{
  return false;
}

void
ComboItemDelegate::paint(QPainter *painter,
    const QStyleOptionViewItem &option,
    const QModelIndex &index) const
{
  QItemDelegate::paint(painter, option, index);
  /* It looks just like a text field anyway.
   QStyleOptionComboBox comboOption;
   comboOption.rect = option.rect;
   comboOption.currentText
   = index.model()->data(index, Qt::DisplayRole).toString();

   QApplication::style()->drawControl(
   QStyle::CE_ComboBoxLabel,
   &comboOption,
   painter);

   QStyleOptionFocusRect rectOption;
   rectOption.rect = option.rect;
   rectOption.backgroundColor = index.data(Qt::BackgroundColorRole);

   QApplication::style()->drawControl(
   QStyle::CE_ComboBoxLabel,
   &rectOption,
   painter);
   */
}

QSize
ComboItemDelegate::sizeHint(const QStyleOptionViewItem & option,
    const QModelIndex & index) const
{
  QSize size = QItemDelegate::sizeHint(option, index);
  size.setHeight(1.3 * size.height());
  return size;
}
