/*
 * uComboItemDelegate.h
 *
 *  Created on: Jul 27, 2010
 *      Author: Jarrod Chesney
 */

#ifndef _COMBO_ITEM_DELEGATE_H_
#define _COMBO_ITEM_DELEGATE_H_

/*! Combo Item Delegate
 * Long comment about the class
 *
 */
#include <Qt/qitemdelegate.h>

class ComboItemDelegate : public QItemDelegate
{
  public:
    explicit
    ComboItemDelegate(QObject *parent = 0);

    virtual
    ~ComboItemDelegate();

    QWidget*
    createEditor(QWidget *parent,
        const QStyleOptionViewItem &option,
        const QModelIndex &index) const;

    void
    setEditorData(QWidget *editor, const QModelIndex &index) const;

    void
    setModelData(QWidget *editor,
        QAbstractItemModel *model,
        const QModelIndex &index) const;

    bool
    itemSelected(QEvent* event,
        QAbstractItemModel* model,
        const QStyleOptionViewItem& option,
        const QModelIndex& index);

    void
    paint(QPainter *painter,
        const QStyleOptionViewItem &option,
        const QModelIndex &index) const;

    QSize
        sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index) const;

};

#endif /* _COMBO_ITEM_DELEGATE_H_ */
