/*
 * QtModelUserRoles.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef QTMODELUSERROLES_H_
#define QTMODELUSERROLES_H_

namespace Qt
{
  enum QtUserRoles
  {
    //! Value is QMap<QString, QVariant>
    ComboOptionsRole = 256,

    //! Value is Bool
    NeedsAttentionRole
  };
}

#endif /* QTMODELUSERROLES_H_ */
