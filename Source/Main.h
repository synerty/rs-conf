/*
 * Main.h
 *
 *  Created on: Dec 19, 2010
 *      Author: Jarrod Chesney
 */
#ifndef MAIN_H_
#define MAIN_H_

#include <auto_ptr.h>

#include <boost/noncopyable.hpp>

#include <QtCore/qobject.h>

#include "orm/v1/Model.h"

class IsSchema;
class MainUi;

class Main : public QObject, boost::noncopyable
{
  Q_OBJECT

  public:

    enum SelectedDbE
    {
      selectedDbStart = 0,
      samsDb = 0,
      sysCfgDb,
      enmacDb,
      simScadaDb,
      ttaSimScadaDb,
      ttaSynTestDb,
      ttaEnmacDb,
      selectedDbCount
    };

    struct DbocSettings
    {
        std::string path;
        std::string modelName;
        std::string modelClass;
    };

    struct DbMetaSettings
    {
        bool isManuallyConfigured;
        bool useQsqlMetaData;
        std::string defaultSchema;
    };

  public:
    static Main&
    Inst();

    virtual
    ~Main();

  public:
    void
    Run();

  public slots:
    /// --------------
    // Interface used by Gui

    void
    exitApplication();

  public:
    /// --------------
    // Glaboal interface

    QWidget*
    MsgBoxParent();

    orm::v1::Conn
    databaseConnectionSettings();

    orm::v1::Conn
    metaDataDatabaseConnectionSettings();

    DbocSettings
    dbocSettings();

    DbMetaSettings
    dbmetaSettings();

  private:
    //    Settings mSettings;
    std::auto_ptr< MainUi > mGui;

  private:
    Main();
    static Main sInst;

};
#endif /* MAIN_H_ */
