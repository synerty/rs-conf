/*
 * Globals.h
 *
 *  Created on: Dec 19, 2010
 *      Author: Jarrod Chesney
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

#include <string>

class QWidget;

QWidget*
MsgBoxParent();

#endif /* GLOBALS_H_ */
