#include "MainUi.h"

#include <boost/enable_shared_from_this.hpp>

#include <QtGui/qfiledialog.h>
#include <QtGui/qlabel.h>
#include <QtGui/qmessagebox.h>

#include "Globals.h"
#include "Main.h"
#include "DbMetaConfig.h"
#include "AppMetaConfig.h"
#include "ValueEditor.h"
#include "DbObjectCompiler.h"
#include "Log.h"

MainUi::MainUi(Main& uc) :
  mUc(uc)
{
  ui.setupUi(this);

  for (int i = Main::selectedDbStart; i < Main::selectedDbCount; i++)
  {
    switch (i)
    {
      case Main::samsDb:
        ui.cboDatabase->addItem("Sams Database");
        break;

      case Main::sysCfgDb:
        ui.cboDatabase->addItem("SysCfg Database");
        break;

      case Main::enmacDb:
        ui.cboDatabase->addItem("Enamc Database");
        break;

      case Main::simScadaDb:
        ui.cboDatabase->addItem("simSCADA Database");
        break;

      case Main::ttaSimScadaDb:
        ui.cboDatabase->addItem("TTA simSCADA Database");
        break;

      case Main::ttaSynTestDb:
        ui.cboDatabase->addItem("TTA SynTEST Database");
        break;

      case Main::ttaEnmacDb:
        ui.cboDatabase->addItem("TTA Enmac Database");
        break;

      default:
        ui.cboDatabase->addItem("ERROR");
    }
  }

  connectSlots();

  mDbMetaConfig.reset(new DbMetaConfig(ui.tabDbMeta));
  mAppMetaConfig.reset(new AppMetaConfig(ui.tabAppMeta));
  mValueEditor.reset(new ValueEditor(ui.tabValueEditor));
  mDbObjectCompiler.reset(new DbObjectCompiler(ui.tabDbObjectCompiler));
  mLog.reset(new Log(ui.tabLog));

}
// ----------------------------------------------------------------------------

MainUi::~MainUi()
{

}
// ----------------------------------------------------------------------------

void
MainUi::connectSlots()
{
  connect(ui.cboDatabase,
      SIGNAL(activated(int)),
      this,
      SLOT(databaseChanged(int)));
}
// ----------------------------------------------------------------------------

void
MainUi::databaseChanged(int index)
{
  mDbMetaConfig->Reset();
  mAppMetaConfig->Reset();
  mValueEditor->Reset();
  mDbObjectCompiler->Reset();
}
// ----------------------------------------------------------------------------

int
MainUi::selectedDatabase()
{
  return ui.cboDatabase->currentIndex();
}
// ----------------------------------------------------------------------------
