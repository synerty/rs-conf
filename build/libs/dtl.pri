# QMake project file for DTL lib
# This project file will add the configuration to the project for the DTL
# database template library.
#
# =============================================================================
# Version History 
#
# <version>	<yymmdd_hhmm>	<Name>
#	<comment ....>
# =============================================================================
#
# 0001		100504_1209		Jarrod Chesney
#	Initial Writing
#
# =============================================================================

# Verify our environment
DEVENV_DTL_VERSION = $$(DEVENV_DTL_VERSION)

isEmpty(DEVENV_DTL_VERSION) {
	error(DEVENV_DTL_VERSION has not been defined in the build environment)
}

# Add in DTL, Its only built as static, so add it as an object
LIBS += $$(MAKE_SH_DEVENV_DIR)/dtl/$$(DEVENV_DTL_VERSION)/$$(DEVENV_BUILD_TYPE)/static/lib/libDTL.a

# Add in the ODBC libs
LIBS += -lodbc32 -lodbccp32

# Add in the directory where the source resides
INCLUDEPATH += $$(DEVENV_ROOT)/src/dtl/$$(DEVENV_DTL_VERSION)/lib

# This define is used to tell DTL that we're using mingw-w64 headers, Its a 
# customised change 
DEFINES += DTL_MINGW64
